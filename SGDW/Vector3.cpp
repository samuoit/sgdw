/*
	This is the implementation for Vector3 class.
	Samir Suljkanovic 2016
*/
#include "Engine.h"

Vector3::Vector3()
{
	x = 0;
	y = 0;
	z = 0;
}

Vector3::Vector3(float X, float Y, float Z)
{
	x = X;
	y = Y;
	z = Z;
}

Vector3::Vector3(float XYZ)
{
	Vector3(XYZ, XYZ, XYZ);
}

Vector3::Vector3(const Vector3 &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
}

Vector3::Vector3(glm::vec3 &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
}

Vector3::~Vector3(){ /* DESTRUCTOR */ }

bool Vector3::operator==(const Vector3 &vec)
{
	return (x == vec.x && y == vec.y && z == vec.z);
}

bool Vector3::operator!=(const Vector3 &vec)
{
	return (vec.x != x || vec.y != y || vec.z != z);
}

void Vector3::operator=(const Vector3 &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
}

void Vector3::operator+=(const Vector3 &vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
}

void Vector3::operator-=(const Vector3 &vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
}

void Vector3::operator/=(float f)
{
	x /= f;
	y /= f;
	z /= f;
}

void Vector3::operator*=(float f)
{
	x *= f;
	y *= f;
	z *= f;
}

Vector3 Vector3::operator+(const Vector3 &vec)
{
	return Vector3(x + vec.x, y + vec.y, z + vec.z);
}

Vector3 Vector3::operator-(const Vector3 &vec)
{
	return Vector3(x - vec.x, y - vec.y, z - vec.z);
}

Vector3 Vector3::operator*(float scalar) const
{
	return Vector3(x * scalar, y * scalar, z * scalar);
}

Vector3 Vector3::operator/(float scalar) const
{
	return Vector3(x / scalar, y / scalar, z / scalar);
}

void Vector3::Negated()
{
	x = x * -1;
	y = y * -1;
	z = z * -1;
}

float Vector3::Dot(const Vector3 &vec)
{
	return x * vec.x + y * vec.y + z * vec.z;
}

float Vector3::Magnitude()
{
	return (float)sqrt(x * x + y * y + z * z);
}

float Vector3::MagnitudeSquared()
{
	return (float)(x * x + y * y + z * z);
}

Vector3 Vector3::Zero()
{
	return Vector3();
}

Vector3 Vector3::One()
{
	return Vector3(1.0f, 1.0f, 1.0f);
}

Vector3 Vector3::Normalize()
{
	float magnitude = Magnitude();

	if (magnitude <= 0.00001f) 
	{ 
		return Vector3(0.0f, 1.0f, 0.0f); 
	}

	return operator/(magnitude);
}

Vector3 Vector3::Cross(const Vector3 &vec)
{
	return Vector3(
		y * vec.z - z * vec.y,
		z * vec.x - x * vec.z,
		x * vec.y - y * vec.x
		);
}

void Vector3::Rotate(COORDINATE_AXIS vecAxis, float angle)
{
	float sinAngle = sin(DEGREES_TO_RADIANS * angle);
	float cosAngle = cos(DEGREES_TO_RADIANS * angle);

	switch (vecAxis)
	{
	case X_AXIS:
		y = y * cosAngle - z * sinAngle;
		z = y * sinAngle + z * cosAngle;
		break;
	case Y_AXIS:
		x = x * cosAngle + z * sinAngle;
		z = -x * sinAngle + z * cosAngle;
		break;
	case Z_AXIS:
		x = x * cosAngle - y * sinAngle;
		y = x * sinAngle + y * cosAngle;
		break;
	default:
		break;
	}
}

Vector3 Vector3::ScaleVector(Vector3 scaleVector, Vector3 vectorToScale)
{
	return Vector3(scaleVector.x * vectorToScale.x, scaleVector.y * vectorToScale.y, scaleVector.z * vectorToScale.z);
}

glm::vec3 Vector3::ToGlmVec3()
{
	return glm::vec3(x, y, z);
}

glm::vec3 Vector3::ToGlmVec3(Vector3 &vec)
{
	return glm::vec3(vec.x, vec.y, vec.z);
}


//void Vector3::PolarToVector(float azemuth, float elevation, float distance)
//{
//	sinAngle = azemuth * DEGREES_TO_RADIANS;
//	cosAngle = elevation * DEGREES_TO_RADIANS;
//
//	tempV.x = sin(sinAngle);
//	tempV.z = cos(sinAngle)*(-1.0f);
//	tempV.y = cos(cosAngle);
//
//	x = tempV.x * tempV.y * distance;
//	z = tempV.z * tempV.y * distance;
//	y = sin(cosAngle)*(-1.0f) * distance;
//}
//
//Vector3 Vector3::VectorToPolar(Vector3 target)
//{
//	Vector3 output;
//
//	output.y = 180.0f - (atan2(target.x, target.z) * RADIANS_TO_DEGREES);
//
//	if (output.y<0.0f){ output.y += 360.0f; }
//
//	tempV = target;
//	tempV.y = 0.0f;
//	output.x = (atan2(tempV.Magnitude(), target.y) * RADIANS_TO_DEGREES) - 90.0f;
//
//	if (output.x<0.0f){ output.x += 360.0f; }
//
//	output.z = (float)sqrt(target.x * target.x + target.y * target.y + target.z * target.z);
//
//	return output;
//}