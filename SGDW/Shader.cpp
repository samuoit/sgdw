/*
	This is the implementation for Shader class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

Shader::Shader()
{
	handle = 0;				// Initialize handle
}

Shader::~Shader()
{
	Destroy();				// Call Destroy function to release memory occupied by this shader
}

unsigned int Shader::LoadShader(SHADER_SOURCE shaderSource, const char* shaderData, GLenum shaderType)
{
	const char* shaderCode;								// Memory store for shader code

	// Check if the source is file or code and load it properly
	if (SHADER_FILE == shaderSource)
	{
		shaderCode = Util::LoadFile(shaderData, "rb");	// Get shader code from file (rb = read binary)
	}
	else
	{
		shaderCode = shaderData;						// Get shader code from provided code
	}
	
	// If shader code was not loaded or is corrupted return with 0 i.e. loading was not successful
	if (shaderCode == nullptr) return 0;

	handle = glCreateShader(shaderType);				// Generate handle for the shader
	glShaderSource(handle, 1, &shaderCode, 0);			// Assign shader code to shader handle
	glCompileShader(handle);							// Compile shader

	// Check if shader compilation was successful and return handle to shader if it was
	int compileStatus;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus)
	{
		std::cout << "Shader compiled successfully! " << (SHADER_FILE == shaderSource ? shaderData : " from code") << std::endl;
		return handle;
	}

	// If shader handle was not returned due to the error return message and detailed log information
	std::cout << "Shader NOT compiled! " << (SHADER_FILE == shaderSource ? shaderData : " from code") << std::endl;

	int logLength;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLength);

	std::string log(logLength, ' ');
	glGetShaderInfoLog(handle, logLength, &logLength, &log[0]);

	std::cout << log.c_str() << std::endl;

	return 0;	// Return 0 as shader was not compiled
}

unsigned int Shader::GetHandle()
{
	return handle;				// Return handle
}

void Shader::Destroy()
{
	if (handle)	// If shader exists
	{
		glDeleteShader(handle);	// Delete shader
		handle = 0;				// Reset handle
	}
}