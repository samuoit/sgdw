///*
//	This is the implementation for Matrix3x3 class.
//	Samir Suljkanovic 2015
//*/
//#include "Engine.h"
//
//Matrix3x3::Matrix3x3()
//{
//	m11 = 0; m12 = 0; m13 = 0;
//	m21 = 0; m22 = 0; m23 = 0;
//	m31 = 0; m32 = 0; m33 = 0;
//}
//
//Matrix3x3::Matrix3x3(bool isIdentity)
//{
//	if (!isIdentity) Matrix3x3();
//	else Matrix3x3(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
//}
//
//Matrix3x3::Matrix3x3(
//	float M11, float M12, float M13,
//	float M21, float M22, float M23,
//	float M31, float M32, float M33)
//{
//	m11 = M11; m12 = M12; m13 = M13;
//	m21 = M21; m22 = M22; m23 = M23;
//	m31 = M31; m32 = M32; m33 = M33;
//}
//
//Matrix3x3::Matrix3x3(const Matrix3x3 &mat)
//{
//	m11 = mat.m11; m12 = mat.m12; m13 = mat.m13;
//	m21 = mat.m21; m22 = mat.m22; m23 = mat.m23;
//	m31 = mat.m31; m32 = mat.m32; m33 = mat.m33;
//}
//
//Matrix3x3::~Matrix3x3() { /* DESTRUCTOR */ }
//
//bool Matrix3x3::operator==(const Matrix3x3 &mat)
//{
//	if (m11 == mat.m11 && m12 == mat.m12 && m13 == mat.m13 && 
//		m21 == mat.m21 && m22 == mat.m22 && m23 == mat.m23 && 
//		m31 == mat.m31 && m32 == mat.m32 && m33 == mat.m33)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}
//
//void Matrix3x3::operator=(const Matrix3x3 &mat)
//{
//	m11 = mat.m11; m12 = mat.m12; m13 = mat.m13;
//	m21 = mat.m21; m22 = mat.m22; m23 = mat.m23;
//	m31 = mat.m31; m32 = mat.m32; m33 = mat.m33;
//}
//
//void Matrix3x3::operator+=(const Matrix3x3 &mat)
//{
//	m11 += mat.m11; m12 += mat.m12; m13 += mat.m13;
//	m21 += mat.m21; m22 += mat.m22; m23 += mat.m23;
//	m31 += mat.m31; m32 += mat.m32; m33 += mat.m33;
//}
//
//void Matrix3x3::operator-=(const Matrix3x3 &mat)
//{
//	m11 -= mat.m11; m12 -= mat.m12; m13 -= mat.m13;
//	m21 -= mat.m21; m22 -= mat.m22; m23 -= mat.m23;
//	m31 -= mat.m31; m32 -= mat.m32; m33 -= mat.m33;
//}
//
//void Matrix3x3::operator*=(const Matrix3x3 &mat)
//{
//	Matrix3x3 temp = this;
//
//	m11 = temp.m11 * mat.m11 + temp.m12 * mat.m21 + temp.m13 * mat.m31;
//	m12 = temp.m11 * mat.m12 + temp.m12 * mat.m22 + temp.m13 * mat.m32;
//	m13 = temp.m11 * mat.m13 + temp.m12 * mat.m23 + temp.m13 * mat.m33;
//	m21 = temp.m21 * mat.m11 + temp.m22 * mat.m21 + temp.m23 * mat.m31;
//	m22 = temp.m21 * mat.m12 + temp.m22 * mat.m22 + temp.m23 * mat.m32;
//	m23 = temp.m21 * mat.m13 + temp.m22 * mat.m23 + temp.m23 * mat.m33;
//	m31 = temp.m31 * mat.m11 + temp.m32 * mat.m21 + temp.m33 * mat.m31;
//	m32 = temp.m31 * mat.m12 + temp.m32 * mat.m22 + temp.m33 * mat.m32;
//	m33 = temp.m31 * mat.m13 + temp.m32 * mat.m23 + temp.m33 * mat.m33;
//}
//
//Matrix3x3 Matrix3x3::operator+(const Matrix3x3 &mat)
//{
//	return Matrix3x3(
//		m11 + mat.m11, m12 + mat.m12, m13 + mat.m13,
//		m21 + mat.m21, m22 + mat.m22, m23 + mat.m23,
//		m31 + mat.m31, m32 + mat.m32, m33 + mat.m33
//		);
//}
//
//Matrix3x3 Matrix3x3::operator-(const Matrix3x3 &mat)
//{
//	return Matrix3x3(
//		m11 - mat.m11, m12 - mat.m12, m13 - mat.m13,
//		m21 - mat.m21, m22 - mat.m22, m23 - mat.m23,
//		m31 - mat.m31, m32 - mat.m32, m33 - mat.m33
//		);
//}
//
//Matrix3x3 Matrix3x3::operator*(const Matrix3x3 &mat)
//{
//	Matrix3x3 temp;
//	
//	temp.m11 = m11 * mat.m11 + m12 * mat.m21 + m13 * mat.m31;
//	temp.m12 = m11 * mat.m12 + m12 * mat.m22 + m13 * mat.m32;
//	temp.m13 = m11 * mat.m13 + m12 * mat.m23 + m13 * mat.m33;
//	temp.m21 = m21 * mat.m11 + m22 * mat.m21 + m23 * mat.m31;
//	temp.m22 = m21 * mat.m12 + m22 * mat.m22 + m23 * mat.m32;
//	temp.m23 = m21 * mat.m13 + m22 * mat.m23 + m23 * mat.m33;
//	temp.m31 = m31 * mat.m11 + m32 * mat.m21 + m33 * mat.m31;
//	temp.m32 = m31 * mat.m12 + m32 * mat.m22 + m33 * mat.m32;
//	temp.m33 = m31 * mat.m13 + m32 * mat.m23 + m33 * mat.m33;
//
//	return temp;
//}
//
//Matrix3x3 Matrix3x3::operator*(float f)
//{
//	return Matrix3x3(
//		m11 * f, m12 * f, m13 * f,
//		m21 * f, m22 * f, m23 * f,
//		m31 * f, m32 * f, m33 * f
//		);
//}
//
//Matrix3x3 Matrix3x3::operator/(float f)
//{
//	return Matrix3x3(
//		m11 / f, m12 / f, m13 / f,
//		m21 / f, m22 / f, m23 / f,
//		m31 / f, m32 / f, m33 / f
//		);
//}
//
//float Matrix3x3::Determinant()
//{
//	return m11 * (m22 * m33 - m23 * m32) - m12 * (m21 * m33 - m23 * m31) + m13 * (m21 * m32 - m22 * m31);
//}
//
//void Matrix3x3::Transpose()
//{
//	float temp;
//
//	temp = m12; m12 = m21; m21 = temp;
//	temp = m13; m13 = m31; m31 = temp;
//	temp = m23; m23 = m32; m32 = temp;
//}
//
//void Matrix3x3::Inverse()
//{
//	Matrix3x3 mInverse;	// Inverse matrix
//
//	// Check if matrix has inverse (determinant not equal to 0)
//	if (Determinant() != 0)
//	{
//		Matrix3x3 mAdjoint;
//
//		// Adjugated / Adjoint matrix (after minor's cofactoring and transpose)
//		mAdjoint.m11 = (m22 * m33 - m23 * m32);
//		mAdjoint.m21 = (m21 * m33 - m23 * m31) * (-1);
//		mAdjoint.m31 = (m21 * m32 - m22 * m31);
//		mAdjoint.m12 = (m12 * m33 - m13 * m32) * (-1);
//		mAdjoint.m22 = (m11 * m33 - m13 * m31);
//		mAdjoint.m32 = (m11 * m32 - m12 * m31) * (-1);
//		mAdjoint.m13 = (m12 * m23 - m13 * m22);
//		mAdjoint.m23 = (m11 * m23 - m13 * m21) * (-1);
//		mAdjoint.m33 = (m11 * m22 - m12 * m21);
//
//		mInverse = mAdjoint / Determinant();
//
//		m11 = mInverse.m11;
//		m12 = mInverse.m12;
//		m13 = mInverse.m13;
//		m21 = mInverse.m21;
//		m22 = mInverse.m22;
//		m23 = mInverse.m23;
//		m31 = mInverse.m31;
//		m32 = mInverse.m32;
//		m33 = mInverse.m33;
//	}
//}
//
//void Matrix3x3::OrthoNormalize()
//{
//	// Get columns of the matrix
//	Vector3 col1 = GetColumn(1);	// First column
//	Vector3 col2 = GetColumn(2);	// Second column
//	Vector3 col3 = GetColumn(3);	// Third column
//
//	// Ortho-normalize
//	col1.Normalize();
//	col3 = col1.Cross(col2);
//	col3.Normalize();
//	col2 = col3.Cross(col1);
//	col2.Normalize();
//
//	// Take ortho-normalized values and assign them to  This is the implementation for  matrix
//	m11 = col1.x;
//	m21 = col1.y;
//	m31 = col1.z;
//	m12 = col2.x;
//	m22 = col2.y;
//	m32 = col2.z;
//	m13 = col3.x;
//	m23 = col3.y;
//	m33 = col3.z;
//}
//
//Vector3 Matrix3x3::GetRow(int row)
//{
//	if (row == 1) return Vector3(m11, m12, m13);
//	else if (row == 2) return Vector3(m21, m22, m23);
//	else if (row == 3) return Vector3(m31, m32, m33);
//	else return Vector3();
//}
//
//Vector3 Matrix3x3::GetColumn(int column)
//{
//	if (column == 1) return Vector3(m11, m21, m31);
//	else if (column == 2) return Vector3(m12, m22, m32);
//	else if (column == 3) return Vector3(m13, m23, m33);
//	else return Vector3();
//}
//
//void Matrix3x3::SetRow(int row, Vector3 rowVector)
//{
//	if (row == 1)
//	{
//		m11 = rowVector.x;
//		m12 = rowVector.y;
//		m13 = rowVector.z;
//	}
//	else if (row == 2)
//	{
//		m21 = rowVector.x;
//		m22 = rowVector.y;
//		m23 = rowVector.z;
//	}
//	else if (row == 3)
//	{
//		m31 = rowVector.x;
//		m32 = rowVector.y;
//		m33 = rowVector.z;
//	}
//}
//
//void Matrix3x3::SetColumn(int column, Vector3 colVector)
//{
//	if (column == 1)
//	{
//		m11 = colVector.x;
//		m21 = colVector.y;
//		m31 = colVector.z;
//	}
//	else if (column == 2)
//	{
//		m12 = colVector.x;
//		m22 = colVector.y;
//		m32 = colVector.z;
//	}
//	else if (column == 3)
//	{
//		m13 = colVector.x;
//		m23 = colVector.y;
//		m33 = colVector.z;
//	}
//}
//
//Matrix3x3 Matrix3x3::RotationMatrixX(float angle)
//{
//	return Matrix3x3(1.0f, 0.0f, 0.0f, 0.0f, cosf(angle), sinf(angle), 0.0f, -sinf(angle), cosf(angle));
//}
//
//Matrix3x3 Matrix3x3::RotationMatrixY(float angle)
//{
//	return Matrix3x3(cosf(angle), 0.0f, -sinf(angle), 0.0f, 1.0f, 0.0f, sinf(angle), 0.0f, cosf(angle));
//}
//
//Matrix3x3 Matrix3x3::RotationMatrixZ(float angle)
//{
//	return Matrix3x3(cosf(angle), sinf(angle), 0.0f, -sinf(angle), cosf(angle), 0.0f, 0.0f, 0.0f, 1.0f);
//}
//
//Matrix3x3 Matrix3x3::Zero()
//{
//	return Matrix3x3();
//}
//
//Matrix3x3 Matrix3x3::Identity()
//{
//	return Matrix3x3(true);
//}
//
//Vector3 Matrix3x3::ScaleVector(Matrix3x3 scaleMat, Vector3 vecToScale)
//{
//	return Vector3(scaleMat.m11 * vecToScale.x, scaleMat.m22 * vecToScale.y, scaleMat.m33 * vecToScale.z);
//}