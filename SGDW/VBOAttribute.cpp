/*
	This is the implementation for VBOAttribute class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

VBOAttribute::VBOAttribute()
{
	attributeType = VBO_POSITION;
	elementType = GL_FLOAT;
	elementSize = 0;
	elementsPerAttribute = 0;
	elementsTotalCount = 0;
	data = nullptr;
}

VBOAttribute::~VBOAttribute(){}

VBO_ATTRIBUTE_TYPE VBOAttribute::GetAttributeType()
{
	return attributeType;
}

GLenum VBOAttribute::GetElementType()
{
	return elementType;
}

unsigned VBOAttribute::GetElementsSize()
{
	return elementSize;
}

unsigned VBOAttribute::GetElementsPerAttribute()
{
	return elementsPerAttribute;
}

unsigned VBOAttribute::GetElementsTotalCount()
{
	return elementsTotalCount;
}

void* VBOAttribute::GetDataPointer()
{
	return data;
}

unsigned* VBOAttribute::GetInterleavedElementsPerAttribute()
{
	return interleavedElementsPerAttribute;
}

void VBOAttribute::SetAttributeType(VBO_ATTRIBUTE_TYPE type)
{
	attributeType = type;
}

void VBOAttribute::SetElementType(GLenum elemType)
{
	elementType = elemType;
}

void VBOAttribute::SetElementsSize(unsigned elemSize)
{
	elementSize = elemSize;
}

void VBOAttribute::SetElementsPerAttribute(unsigned elemPerAttribute)
{
	elementsPerAttribute = elemPerAttribute;
}

void VBOAttribute::SetElementsTotalCount(unsigned elemTotalCount)
{
	elementsTotalCount = elemTotalCount;
}

void VBOAttribute::SetDataPointer(void* dataPtr)
{
	data = dataPtr;
}

void VBOAttribute::SetInterleavedElementsPerAttribute(unsigned elementsPerAttribute[4])
{
	interleavedElementsPerAttribute[0] = elementsPerAttribute[0];
	interleavedElementsPerAttribute[1] = elementsPerAttribute[1];
	interleavedElementsPerAttribute[2] = elementsPerAttribute[2];
	interleavedElementsPerAttribute[3] = elementsPerAttribute[3];
}