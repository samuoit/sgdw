/*
	This is the implementation for Water class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

// Random number generator (generates number between specified min and max arguments)
float Water::RandomFloat(float min, float max, unsigned seed)
{
	return min + ((max - min) * rand()) / (RAND_MAX + 1.0f);		// Return random number between minimum and maximum specified arguments
}

// Sets seed for random number generator
void Water::SetRandomSeed(unsigned seed)
{
	if (seed > 0) srand(seed);										// Randomize based on seed if greater than 0
	else srand(static_cast<unsigned int>(std::time(NULL)));			// Randomize based on time as seed if seed is 0
}

Water::Water()
{ 
	isInitialized = false;
	waterHeight = 0.0f;
}

Water::~Water() {}

bool Water::Initialize() 
{
	// Return true and exit if already initialized
	if (isInitialized) return true;

	// Load water object (plane) and create VBO
	if (!loader.LoadFromFile("../assets/models/Plane2.obj"))
		std::cout << "Water plane failed to load.";
	else
	{
		vboWater.AddAttributesData(loader.GetInterleavedAttributeData());
		vboWater.CreateVBOs(true);
	}

	// Load texture
	if (!texWater.Load("../assets/textures/water3.png")) std::cout << "Water texture failed to load.";
	
	// ** Initialize shaders, attach them to shaders program and link program
	// Static Reflection
	vertWaterRR.LoadShader(SHADER_FILE, "../assets/shaders/ReflectionRefraction.vert", GL_VERTEX_SHADER);
	fragWaterRR.LoadShader(SHADER_FILE, "../assets/shaders/ReflectionRefraction.frag", GL_FRAGMENT_SHADER);

	// Attach and link water shaders to water shader program
	sProgWaterRR.AttachShader(vertWaterRR);
	sProgWaterRR.AttachShader(fragWaterRR);
	sProgWaterRR.LinkProgram();

	// Water movement
	vertWaterMV.LoadShader(SHADER_FILE, "../assets/shaders/Water.vert", GL_VERTEX_SHADER);
	fragWaterMV.LoadShader(SHADER_FILE, "../assets/shaders/Water.frag", GL_FRAGMENT_SHADER);

	// Attach and link water shaders to shader program used to move water surface
	sProgWaterMV.AttachShader(vertWaterMV);
	sProgWaterMV.AttachShader(fragWaterMV);
	sProgWaterMV.LinkProgram();

	isInitialized = true;

	return isInitialized;
}

void Water::Update(float secondsElapsed)
{
	if (!isInitialized) return;
}

void Water::Draw()
{
	if (!isInitialized) return;

	// Bind Reflection/Refraction shader program and send uniforms
	sProgWaterRR.Bind();
	sProgWaterRR.SendUniformMatrix("u_Projection", waterProjectionMatrix);	// Send projection matrix
	sProgWaterRR.SendUniformMatrix("u_View", waterViewMatrix);				// Send view matrix
	sProgWaterRR.SendUniformMatrix("u_Model", waterModelMatrix);			// Send model matrix
	sProgWaterRR.SendUniformInt("u_EnvTexture", 0);							// Send environmental cube texture
	sProgWaterRR.SendUniformBool("u_IsRefractive", false);					// Send info about refractivness

	// Bind and send water texture, and send alpha setting for it
	glActiveTexture(GL_TEXTURE1);
	texWater.Bind();
	
	// Draw water object
	vboWater.Draw();

	// Unbind texture and shader programs
	texWater.Unbind();
	sProgWaterRR.Unbind();
}

void Water::Destroy()
{
	// Destroy shader program and shaders
	sProgWaterRR.Destroy();
	if (vertWaterRR.GetHandle()) vertWaterRR.Destroy();
	if (fragWaterRR.GetHandle()) fragWaterRR.Destroy();
	if (vertWaterMV.GetHandle()) vertWaterMV.Destroy();
	if (fragWaterMV.GetHandle()) fragWaterMV.Destroy();
	
	// Cleanup memory taken by VBO (no need for this as VertexBufferObject destructor calls Destroy function)
	// vboWater.Destroy();
}

float Water::GetWaterHeight()
{
	waterHeight = waterModelMatrix.elements[13];	// y position is water height
	return waterHeight;
}

void Water::SetMatrices(Matrix projection, Matrix view, Matrix model)
{
	waterProjectionMatrix = projection;
	waterViewMatrix = view;
	waterModelMatrix = model;
}





//// ** Set uniform values before sending them to shader
//Matrix normalMatrix = IDENTITY_MATRIX;
//Matrix modelView = MultiplyMatrices(&waterModelMatrix, &waterViewMatrix);	
//normalMatrix = InverseMatrix(&modelView);
//normalMatrix = TransposeMatrix(&normalMatrix);

//numberOfWaves = 4;
//eyePosition = Vector3(waterViewMatrix.elements[12], waterViewMatrix.elements[13], waterViewMatrix.elements[14]);
//waterHeight = waterModelMatrix.elements[13];
//time = 1.0f;

//SetRandomSeed();
//randomAngle = RandomFloat(-PI / 3, PI / 3);

//// Loop for each wave and set values of wave parameters
//for (int i = 0; i < numberOfWaves; i++)
//{
//	amplitude[i] = 0.5f / (i + 1);										// Calculate amplitude for each wave
//	direction[i] = Vector3(i, cos(randomAngle), sin(randomAngle));		// Calculate directions for each wave
//	speed[i] = 1.0f + 2 * i;											// Calculate speeds for each wave
//	waveLength[i] = 8 * PI / (i + 1);									// Calculate wave lengths for each wave
//}

//// Bind Water motion shader program and send uniforms
//sProgWaterMV.Bind();
//sProgWaterMV.SendUniformMatrix("u_Projection", waterProjectionMatrix);	// Send projection matrix
//sProgWaterMV.SendUniformMatrix("u_View", waterViewMatrix);				// Send view matrix
//sProgWaterMV.SendUniformMatrix("u_Model", waterModelMatrix);			// Send model matrix
//sProgWaterMV.SendUniformMatrix("u_NormalMatrix", normalMatrix);			// Send normal matrix
//sProgWaterMV.SendUniformVec3("u_EyePosition", eyePosition);				// Send viewer (camera/eye) position
//sProgWaterMV.SendUniformFloat("u_WaterHeight", waterHeight);			// Send water height
//sProgWaterMV.SendUniformFloat("u_Time", time);							// Send time for wave cycle
//sProgWaterMV.SendUniformVec3Array("u_Direction", direction, 8);			// Send wave direction vector
//sProgWaterMV.SendUniformFloatArray("u_Amplitude", amplitude, 8);		// Send max. wave heights (amplitude)
//sProgWaterMV.SendUniformFloatArray("u_WaveLength", waveLength, 8);		// Send max. wave lengths
//sProgWaterMV.SendUniformFloatArray("u_Speed", speed, 8);				// Send wave speeds
//sProgWaterMV.SendUniformInt("u_NumberOfWaves", numberOfWaves);			// Send number of waves setting

//// Draw water object
//vboWater.Draw();