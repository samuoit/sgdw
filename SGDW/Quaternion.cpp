/*
	This is the implementation for Quaternion class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

Quaternion::Quaternion()
{
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}

Quaternion::Quaternion(float X, float Y, float Z, float W)
{
	x = X;
	y = Y;
	z = Z;
	w = W;
}

Quaternion::Quaternion(float XYZW)
{
	Quaternion(XYZW, XYZW, XYZW, XYZW);
}

Quaternion::Quaternion(const Quaternion &quat)
{
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
}

Quaternion::~Quaternion(){ /* DESTRUCTOR */ }

bool Quaternion::operator==(const Quaternion &quat)
{
	return (quat.x == x && quat.y == y && quat.z == z && quat.w == w);
}

bool Quaternion::operator!=(const Quaternion &quat)
{
	return (quat.x != x || quat.y != y || quat.z != z || quat.w != w);
}

void Quaternion::operator=(const Quaternion &quat)
{
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
}

void Quaternion::operator+=(const Quaternion &quat)
{
	x += quat.x;
	y += quat.y;
	z += quat.z;
	w += quat.w;
}

void Quaternion::operator-=(const Quaternion &quat)
{
	x -= quat.x;
	y -= quat.y;
	z -= quat.z;
	w -= quat.w;
}

void Quaternion::operator/=(const Quaternion &quat)
{
	x /= quat.x;
	y /= quat.y;
	z /= quat.z;
	z /= quat.z;
}

void Quaternion::operator*=(const Quaternion &quat)
{
	x *= quat.x;
	y *= quat.y;
	z *= quat.z;
	z *= quat.z;
}

Quaternion Quaternion::operator+(const Quaternion &quat)
{
	return Quaternion(x + quat.x, y + quat.y, z + quat.z, w + quat.w);
}

Quaternion Quaternion::operator-(const Quaternion &quat)
{
	return Quaternion(x - quat.x, y - quat.y, z - quat.z, w - quat.w);
}

Quaternion Quaternion::operator/(const Quaternion &quat)
{
	return Quaternion(x / quat.x, y / quat.y, z / quat.z, w / quat.w);
}

Quaternion Quaternion::operator*(const Quaternion &quat)
{
	return Quaternion(x * quat.x, y * quat.y, z * quat.z, w * quat.w);
}

Quaternion Quaternion::operator*(float scalar)
{
	return Quaternion(x * scalar, y * scalar, z * scalar, w * scalar);
}

Quaternion Quaternion::operator/(float scalar)
{
	return Quaternion(x / scalar, y / scalar, z / scalar, w / scalar);
}

void Quaternion::Negated()
{
	x = x * -1;
	y = y * -1;
	z = z * -1;
	w = w * -1;
}

Quaternion Quaternion::Conjugate() const
{
	return Quaternion(-x, -y, -z, w);
}

float Quaternion::Dot(const Quaternion &quat)
{
	return x * quat.x + y * quat.y + z * quat.z + w * quat.w;
}

Quaternion Quaternion::Cross(const Quaternion &quat)
{
	return Quaternion(
		w * quat.w - x * quat.x - y * quat.y - z * quat.z,
		w * quat.x + x * quat.w + y * quat.z - z * quat.y,
		w * quat.y + y * quat.w + z * quat.x - x * quat.z,
		w * quat.z + z * quat.w + x * quat.y - y * quat.x);
}

float Quaternion::Magnitude()
{
	return (float)sqrt(x * x + y * y + z * z);
}

float Quaternion::MagnitudeSquared()
{
	return (float)(x * x + y * y + z * z);
}

Quaternion Quaternion::Zero()
{
	return Quaternion();
}

Quaternion Quaternion::Normalize()
{
	float magnitude = Magnitude();
	return operator/(magnitude);
}

Vector3 Quaternion::RotateVector(Quaternion quat, const Vector3 vec3) const
{
	Vector3 result;

	float x1 = quat.y * vec3.z - quat.z * vec3.y;
	float y1 = quat.z * vec3.x - quat.x * vec3.z;
	float z1 = quat.x * vec3.y - quat.y * vec3.x;

	float x2 = quat.w * x1 + quat.y * z1 - quat.z * y1;
	float y2 = quat.w * y1 + quat.z * x1 - quat.x * z1;
	float z2 = quat.w * z1 + quat.x * y1 - quat.y * x1;

	result.x = vec3.x + 2.0f * x2;
	result.y = vec3.y + 2.0f * y2;
	result.z = vec3.z + 2.0f * z2;

	return result;
}

Quaternion Quaternion::GetRotationQuaternion(const Vector3 &fromVec, const Vector3 &toVec)
{
	Quaternion result;

	Vector3 tempVec = fromVec;
	tempVec += toVec;					// Add 'from' and 'to' vectors
	tempVec = tempVec.Normalize();		// Normalize result vector

	result.w = tempVec.Dot(fromVec);
	result.x = fromVec.y * tempVec.z - fromVec.z * tempVec.y;
	result.y = fromVec.z * tempVec.x - fromVec.x * tempVec.z;
	result.z = fromVec.x * tempVec.y - fromVec.y * tempVec.x;
	
	return result;
}