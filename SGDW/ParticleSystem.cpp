#include "Engine.h"

// Random number generator (generates number between specified min and max arguments)
float ParticleSystem::RandomFloat(float min, float max, unsigned seed)
{
	return min + ((max - min) * rand()) / (RAND_MAX + 1.0f);		// Return random number between minimum and maximum specified arguments
}

// Sets seed for random number generator
void ParticleSystem::SetRandomSeed(unsigned seed)
{
	if (seed > 0) srand(seed);										// Randomize based on seed if greater than 0
	else srand(static_cast<unsigned int>(time(NULL)));				// Randomize based on time as seed if seed is 0
}

ParticleSystem::ParticleSystem()
{
	isInitialized = false;											// Particle system is created but not initialized
}

ParticleSystem::~ParticleSystem(){}

// TODO: Create set of values (size, position, color, etc. for different particle types)
void ParticleSystem::SetParticleType(PARTICLE_TYPE typeOfParticle, const char* textureFilePath)
{
	// Check if particle type is custom to apply custom texture, otherwise use texture for defined particle type
	if (PARTICLE_DEFAULT == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_sparkle.png";
	}
	else if (PARTICLE_CUSTOM == typeOfParticle)
	{
		particleTextureName = textureFilePath;
	}
	else if (PARTICLE_RED == typeOfParticle)
	{		
		particleTextureName = "../assets/textures/particle_red.png";
	}
	else if (PARTICLE_GREEN == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_green.png";
	}
	else if (PARTICLE_BLUE == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_blue.png";
	}
	else if (PARTICLE_YELLOW == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_yellow.png";
	}
	else if (PARTICLE_ASH == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_ash.png";
	}
	else if (PARTICLE_SNOW == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_snow.png";
	}
	else if (PARTICLE_RAIN == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_rain.png";
	}
	else if (PARTICLE_SMOKE == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_smoke.png";
	}
	else if (PARTICLE_DUST == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_dust.png";
	}
	else if (PARTICLE_FIRE == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_fire.png";
	}
	else if (PARTICLE_CLOUD == typeOfParticle)
	{
		particleTextureName = "../assets/textures/particle_cloud.png";
	}
	
	if (!particleTexture.Load(particleTextureName)) std::cout << "Particle texture has failed to load.\n";

	//printf("\n\nParticle type initialized %d, texture %s\n\n", typeOfParticle, particleTextureName);
}

bool ParticleSystem::Init(Vector3 positionMinimum, Vector3 positionMaximum, 
	Vector3 velocityMinimum, Vector3 velocityMaximum, 
	Vector2 lifeTimeRangeMinMax, Vector2 alphaRangeMinMax, Vector2 sizeRangeMinMax,
	unsigned int maxParticles, unsigned int rate, 
	PARTICLE_TYPE useParticleType, const char* fileParticlePath)
{
	// If particle system is already initialized just return true
	if (isInitialized) return true;

	// Set default values for particle properties
	positionRangeMinimum = positionMinimum;
	positionRangeMaximum = positionMaximum;
	velocityRangeMinimum = velocityMinimum;
	velocityRangeMaximum = velocityMaximum;
	lifetimeRange = lifeTimeRangeMinMax;
	alphaRange = alphaRangeMinMax;
	sizeRange = sizeRangeMinMax;
	countOfParticlesToCreate = maxParticles;
	emitRate = rate;

	// Set random number generator seed
	SetRandomSeed();

	// Set particle type
	SetParticleType(useParticleType, fileParticlePath);

	// Initialize and load shaders
	vertParticles.LoadShader(SHADER_FILE, "../assets/shaders/Particles.vert", GL_VERTEX_SHADER);
	geomParticles.LoadShader(SHADER_FILE, "../assets/shaders/Particles.geom", GL_GEOMETRY_SHADER);
	fragParticles.LoadShader(SHADER_FILE, "../assets/shaders/Particles.frag", GL_FRAGMENT_SHADER);

	// Attach shaders to shader program and link program
	progParticleSystem.AttachShader(vertParticles);
	progParticleSystem.AttachShader(geomParticles);
	progParticleSystem.AttachShader(fragParticles);
	progParticleSystem.LinkProgram();

	// Initialize collection for each particle property
	particlesList.Positions = new Vector3[countOfParticlesToCreate];
	particlesList.Velocities = new Vector3[countOfParticlesToCreate];
	particlesList.Alpha = new float[countOfParticlesToCreate];
	particlesList.Ages = new float[countOfParticlesToCreate];
	particlesList.Lifetimes = new float[countOfParticlesToCreate];
	particlesList.Size = new float[countOfParticlesToCreate];

	// Generate and bind VAO
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vboPositions);
	glGenBuffers(1, &vboSizes);
	glGenBuffers(1, &vboAlphas);

	// Enable vertex attribute arrays
	glEnableVertexAttribArray(0);		// positions
	glEnableVertexAttribArray(1);		// sizes
	glEnableVertexAttribArray(2);		// color alphas

	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3)* countOfParticlesToCreate, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, vboSizes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* countOfParticlesToCreate, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)1, 1, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, vboAlphas);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* countOfParticlesToCreate, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)2, 1, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);

	isInitialized = true;

	return isInitialized;
}

void ParticleSystem::Update(float elapsed)
{
	if (!isInitialized) return;

	int NumToSpawn = emitRate;

	// Create new particlesList
	while (countOfParticlesCreated < countOfParticlesToCreate && NumToSpawn > 0)
	{
		particlesList.Ages[countOfParticlesCreated] = 0.0f;
		particlesList.Lifetimes[countOfParticlesCreated] = RandomFloat(lifetimeRange.x, lifetimeRange.y);
		particlesList.Size[countOfParticlesCreated] = RandomFloat(sizeRange.x, sizeRange.y);
		particlesList.Alpha[countOfParticlesCreated] = RandomFloat(alphaRange.x, alphaRange.y);

		// Set the particle positions and send them in a random direction
		particlesList.Positions[countOfParticlesCreated] = 
			Vector3
			(
				RandomFloat(positionRangeMinimum.x, positionRangeMaximum.x),	// Randomize position on X axis between min and max values
				RandomFloat(positionRangeMinimum.y, positionRangeMaximum.y),	// Randomize position on Y axis between min and max values 
				RandomFloat(positionRangeMinimum.z, positionRangeMaximum.z)		// Randomize position on Z axis between min and max values
			);

		particlesList.Velocities[countOfParticlesCreated] = 
			Vector3
			(
				RandomFloat(velocityRangeMinimum.x, velocityRangeMaximum.x),	// Randomize velocity on X axis between min and max values
				RandomFloat(velocityRangeMinimum.y, velocityRangeMaximum.y),	// Randomize velocity on Y axis between min and max values
				RandomFloat(velocityRangeMinimum.z, velocityRangeMaximum.z)		// Randomize velocity on Z axis between min and max values
			);

		// Increment particle counters...
		countOfParticlesCreated++;
		NumToSpawn--;
	}

	/// Update existing particlesList ///
	for (unsigned i = 0; i < countOfParticlesCreated; i++)
	{
		particlesList.Ages[i] += elapsed;

		if (particlesList.Ages[i] > particlesList.Lifetimes[i])
		{
			//remove the particle by replacing it with the one at the top of the stack
			particlesList.Alpha[i] = particlesList.Alpha[countOfParticlesCreated - 1];
			particlesList.Ages[i] = particlesList.Ages[countOfParticlesCreated - 1];
			particlesList.Lifetimes[i] = particlesList.Lifetimes[countOfParticlesCreated - 1];
			particlesList.Positions[i] = particlesList.Positions[countOfParticlesCreated - 1];
			particlesList.Size[i] = particlesList.Size[countOfParticlesCreated - 1];
			particlesList.Velocities[i] = particlesList.Velocities[countOfParticlesCreated - 1];

			countOfParticlesCreated--;
			continue;
		}

		particlesList.Positions[i] += particlesList.Velocities[i] * elapsed;

		float interp = particlesList.Ages[i] / particlesList.Lifetimes[i];

		particlesList.Alpha[i] = LERP(alphaRange.x, alphaRange.y, interp);
		particlesList.Size[i] = LERP(sizeRange.x, sizeRange.y, interp);
	}

	// update OpenGL on the changes
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vector3)* countOfParticlesCreated, &particlesList.Positions[0]);

	glBindBuffer(GL_ARRAY_BUFFER, vboSizes);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)* countOfParticlesCreated, &particlesList.Size[0]);

	glBindBuffer(GL_ARRAY_BUFFER, vboAlphas);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)* countOfParticlesCreated, &particlesList.Alpha[0]);

	glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
}

void ParticleSystem::Render()
{
	if (!isInitialized) return;

	// Loop through all particles created and draw them
	if (countOfParticlesCreated != 0)
	{
		glEnable(GL_BLEND);													// Enable alpha blending
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);					// Define blending function
		glDepthMask(GL_FALSE);												// Disable depth writing

		glActiveTexture(GL_TEXTURE0);										// Activate texture slot 0
		particleTexture.Bind();												// Bind texture to slot 0

		// Bind shader program and send uniforms to shaders
		progParticleSystem.Bind();											// Bind shader program
		progParticleSystem.SendUniformMatrix("uModel", IDENTITY_MATRIX);	// Send model matrix
		progParticleSystem.SendUniformMatrix("uView", viewMatrix);			// Send view matrix
		progParticleSystem.SendUniformMatrix("uProj", projectionMatrix);	// Send projection matrix
		progParticleSystem.SendUniformInt("uTex", 0);						// Send bound texture

		glBindVertexArray(vao);												// Bind VAO
		glDrawArrays(GL_POINTS, 0, countOfParticlesCreated);				// Draw created particles
		glBindVertexArray(GL_NONE);											// Unbind VAO

		glDepthMask(GL_TRUE);												// Enable depth writing
		glDisable(GL_BLEND);												// Disable blending

		particleTexture.Unbind();											// Unbind texture
	}
}

void ParticleSystem::SetMatrices(Matrix mProjection, Matrix mView, Matrix mModel)
{
	projectionMatrix = mProjection;
	viewMatrix = mView;	
	modelMatrix = mModel;
}

void ParticleSystem::Destroy()
{
	// Destroy shader program and shaders
	progParticleSystem.Destroy();
	vertParticles.Destroy();
	geomParticles.Destroy();
	fragParticles.Destroy();

	// Cleanup memory taken by VAO and VBOs
	if (vao != GL_NONE) glDeleteVertexArrays(1, &vao);
	if (vboParticles != GL_NONE) glDeleteBuffers(1, &vboParticles);
	if (vboPositions != GL_NONE) glDeleteBuffers(1, &vboPositions);
	if (vboSizes != GL_NONE) glDeleteBuffers(1, &vboSizes);
	if (vboAlphas != GL_NONE) glDeleteBuffers(1, &vboAlphas);
	if (particlesList.Positions != nullptr)
	{
		delete[] particlesList.Positions;
		delete[] particlesList.Velocities;
		delete[] particlesList.Alpha;
		delete[] particlesList.Ages;
		delete[] particlesList.Lifetimes;
		delete[] particlesList.Size;
	}
}