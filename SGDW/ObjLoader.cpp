/*
	This is the implementation for ObjLoader class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

ObjLoader::ObjLoader() {}

ObjLoader::~ObjLoader(){}

// Load a ObjLoader and send it to OpenGL
bool ObjLoader::LoadFromFile(const char* file)
{
	std::ifstream input;
	input.open(file);

	if (!input)
	{
		std::cout << "Could not open the file." << std::endl;
		return false;
	}

	char inputString[CHAR_BUFFER_SIZE];

	// Data from file
	std::vector<Vector3> vertexData, textureData, normalData;
	std::vector<ObjectFace> facesData;;

	while (!input.eof())
	{
		input.getline(inputString, CHAR_BUFFER_SIZE);

		// Check for specific data lines (v , vn, vt, f) in .obj file and load it to proper vector
		if (std::strstr(inputString, "#") != nullptr)						// Comments - Do nothing
		{
			continue;
		}
		else if (std::strstr(inputString, "v ") != nullptr)					// Vertex data
		{
			Vector3 temp;
			std::sscanf(inputString, "v %f %f %f", &temp.x, &temp.y, &temp.z);
			vertexData.push_back(temp);
		}
		else if (std::strstr(inputString, "vt") != nullptr)					// Texture data
		{
			Vector3 temp;
			std::sscanf(inputString, "vt %f %f", &temp.x, &temp.y);
			textureData.push_back(temp);
		}
		else if (std::strstr(inputString, "vn") != nullptr)					// Normals data
		{
			Vector3 temp;
			std::sscanf(inputString, "vn %f %f %f", &temp.x, &temp.y, &temp.z);
			normalData.push_back(temp);
		}
		else if (std::strstr(inputString, "f") != nullptr)					// Faces data
		{
			ObjectFace temp;
			std::sscanf(inputString, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textures[0], &temp.normals[0],		// 1st vertex's position, texture and normal data
				&temp.vertices[1], &temp.textures[1], &temp.normals[1],		// 2nd vertex...
				&temp.vertices[2], &temp.textures[2], &temp.normals[2]);	// 3rd vertex...
			facesData.push_back(temp);
		}
	}

	input.close();															// Close file

	// Clear vectors from previous data
	readyVertexData.clear();
	readyTextureData.clear();
	readyNormalData.clear();
	readyInterleavedData.clear();

	// Based on faces information make data ready for use by VertexBufferObject
	for (unsigned i = 0; i < facesData.size(); i++)
	{
		for (unsigned j = 0; j < 3; j++)
		{
			if (vertexData.size() > 0 && ((vertexData.size() >= facesData[i].vertices[j] - 1)))
			{
				// Add to non-interleaved 'vertex' data collection
				readyVertexData.push_back(vertexData[facesData[i].vertices[j] - 1].x);
				readyVertexData.push_back(vertexData[facesData[i].vertices[j] - 1].y);
				readyVertexData.push_back(vertexData[facesData[i].vertices[j] - 1].z);
				
				// Also add data for interleaved collection
				readyInterleavedData.push_back(vertexData[facesData[i].vertices[j] - 1].x);
				readyInterleavedData.push_back(vertexData[facesData[i].vertices[j] - 1].y);
				readyInterleavedData.push_back(vertexData[facesData[i].vertices[j] - 1].z);
			}

			if (normalData.size() > 0 && ((normalData.size() >= facesData[i].vertices[j] - 1)))
			{
				// Add to non-interleaved 'normal' data collection
				readyNormalData.push_back(normalData[facesData[i].normals[j] - 1].x);
				readyNormalData.push_back(normalData[facesData[i].normals[j] - 1].y);
				readyNormalData.push_back(normalData[facesData[i].normals[j] - 1].z);

				// Also add data for interleaved collection
				readyInterleavedData.push_back(normalData[facesData[i].normals[j] - 1].x);
				readyInterleavedData.push_back(normalData[facesData[i].normals[j] - 1].y);
				readyInterleavedData.push_back(normalData[facesData[i].normals[j] - 1].z);
			}

			if (textureData.size() > 0 && ((textureData.size() >= facesData[i].vertices[j] - 1)))
			{
				// Add to non-interleaved 'texture' data collection
				readyTextureData.push_back(textureData[facesData[i].textures[j] - 1].x);
				readyTextureData.push_back(textureData[facesData[i].textures[j] - 1].y);

				// Also add data for interleaved collection
				readyInterleavedData.push_back(textureData[facesData[i].textures[j] - 1].x);
				readyInterleavedData.push_back(textureData[facesData[i].textures[j] - 1].y);
				//readyInterleavedData.push_back(0);				// Padding
			}
		}
	}

	// Copy data from vectors to arrays
	outVertexData = new float[readyVertexData.size()];
	std::copy(readyVertexData.begin(), readyVertexData.end(), outVertexData);
	
	outTextureData = new float[readyTextureData.size()];
	std::copy(readyTextureData.begin(), readyTextureData.end(), outTextureData);
	
	outNormalData = new float[readyNormalData.size()];
	std::copy(readyNormalData.begin(), readyNormalData.end(), outNormalData);

	outInterleavedData = new float[readyInterleavedData.size()];
	std::copy(readyInterleavedData.begin(), readyInterleavedData.end(), outInterleavedData);

	//printf("\nFaces %d", facesData.size());
	//printf("\nVert data (vector) %d last element (vector) %f last element (array) %f", readyVertexData.size(), readyVertexData.at(readyVertexData.size() - 1), outVertexData[readyVertexData.size() - 1]);
	//printf("\nText data (vector) %d last element (vector) %f last element (array) %f", readyTextureData.size(), readyTextureData.at(readyTextureData.size() - 1), outTextureData[readyTextureData.size() - 1]);
	//printf("\nNorm data (vector) %d last element (vector) %f last element (array) %f", readyNormalData.size(), readyNormalData.at(readyNormalData.size() - 1), outNormalData[readyNormalData.size() - 1]);

	// Return true to confirm successful load of data if everything was fine until this point
	return true;
}

VBOAttribute ObjLoader::GetAttributeData(VBO_ATTRIBUTE_TYPE attributeDataType)
{
	switch (attributeDataType)
	{
	case VBO_POSITION:
		vboAttribute.SetAttributeType(VBO_POSITION);
		vboAttribute.SetDataPointer((void*)outVertexData);
		vboAttribute.SetElementsSize(sizeof(float));
		vboAttribute.SetElementType(GL_FLOAT);
		vboAttribute.SetElementsTotalCount(readyVertexData.size());
		vboAttribute.SetElementsPerAttribute(3);
		//printf("\nVert data (vector) %d last element (vector) %f last element (array) %f", readyVertexData.size(), readyVertexData.at(readyVertexData.size() - 1), outVertexData[readyVertexData.size() - 1]);
		break;
	case VBO_NORMAL:
		vboAttribute.SetAttributeType(VBO_NORMAL);
		vboAttribute.SetDataPointer((void*)outNormalData);
		vboAttribute.SetElementsSize(sizeof(float));
		vboAttribute.SetElementType(GL_FLOAT);
		vboAttribute.SetElementsTotalCount(readyNormalData.size());
		vboAttribute.SetElementsPerAttribute(3);
		//printf("\nText data (vector) %d last element (vector) %f last element (array) %f", readyTextureData.size(), readyTextureData.at(readyTextureData.size() - 1), outTextureData[readyTextureData.size() - 1]);
		break;
	case VBO_TEXTURE_UV:
		vboAttribute.SetAttributeType(VBO_TEXTURE_UV);
		vboAttribute.SetDataPointer((void*)outTextureData);
		vboAttribute.SetElementsSize(sizeof(float));
		vboAttribute.SetElementType(GL_FLOAT);
		vboAttribute.SetElementsTotalCount(readyTextureData.size());
		vboAttribute.SetElementsPerAttribute(2);
		//printf("\nNorm data (vector) %d last element (vector) %f last element (array) %f", readyNormalData.size(), readyNormalData.at(readyNormalData.size() - 1), outNormalData[readyNormalData.size() - 1]);
		break;
	case VBO_COLOR:
		break;
	}

	return vboAttribute;
}

VBOAttribute ObjLoader::GetInterleavedAttributeData()
{
	vboAttribute.SetAttributeType(VBO_INTERLEAVED);
	vboAttribute.SetDataPointer((void*)outInterleavedData);
	vboAttribute.SetElementsSize(sizeof(float));
	vboAttribute.SetElementType(GL_FLOAT);
	vboAttribute.SetElementsTotalCount(readyInterleavedData.size());
	unsigned elementsPerAttr[4] = { 3, 3, 2, 0 };	// 3 for position elements (X, Y, Z), 3 for normal elements (X, Y, Z), and 3 for texture elements (U, V, padding) - 0 for colors
	vboAttribute.SetInterleavedElementsPerAttribute(elementsPerAttr);
	return vboAttribute;
}