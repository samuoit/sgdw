#include "Engine.h"

#pragma once

enum PRIMITIVE_TYPE
{
	PRIMITIVE_QUAD,
	PRIMITIVE_TRIANGLE
};

class PrimitiveLoader
{

private:

	VBOAttribute vboAttribute;		// Temporary collections of prepared vertex data attributes
	float* quadInterleavedData;		// Array of quad data
	float* triangleInterleavedData;	// Array of triangle data

public:

	PrimitiveLoader()
	{
		float quadData[] =
		{
			-1.0f, -1.0f, 0.0f,		// v1 position
			0.00, 0.00, 1.00, 		// v1 normal
			0.0f, 0.0f,				// v1 UV coordinates
			
			1.0f, -1.0f, 0.0f,		// v2 position
			0.00, 0.00, 1.00, 		// v2 normal
			1.0f, 0.0f,				// v2 UV coordinates

			-1.0f, 1.0f, 0.0f,		// v3 position
			0.00, 0.00, 1.00,		// v3 normal
			0.0f, 1.0f,				// v3 UV coordinates

			1.0f, 1.0f, 0.0f,		// v4 position
			0.00, 0.00, 1.00,		// v4 normal
			1.0f, 1.0f,				// v4 UV coordinates
			
			-1.0f, 1.0f, 0.0f,		// v5 position
			0.00, 0.00, 1.00,		// v5 normal
			0.0f, 1.0f,				// v5 UV coordinates

			1.0f, -1.0f, 0.0f,		// v6 position	
			0.00, 0.00, 1.00,		// v6 normal
			1.0f, 0.0f				// v6 UV coordinates
		};

		quadInterleavedData = quadData;

		float triangleData[] =
		{
			-0.4f, 0.1f, 0.0f,		// v1 position
			0.0, 0.0, 1.0,			// v1 normal
			0.0f, 0.0f,				// v1 UV
			0.4f, 0.1f, 0.0f,		// v2 position
			0.0f, 0.0f, 1.0f,		// v2 normal
			1.0f, 0.0f,				// v2 UV
			0.0f, 0.7f, 0.0f,		// v3 position
			0.0f, 0.0f, 1.0f,		// v3 normal
			0.0f, 1.0f				// v3 UV
		};

		triangleInterleavedData = triangleData;
	}

	VBOAttribute GetObjectInterleavedAttributeData(PRIMITIVE_TYPE typeOfPrimitive)
	{
		if (PRIMITIVE_QUAD == typeOfPrimitive)
		{
			vboAttribute.SetAttributeType(VBO_INTERLEAVED);
			vboAttribute.SetDataPointer((void*)quadInterleavedData);
			vboAttribute.SetElementsSize(sizeof(float));
			vboAttribute.SetElementType(GL_FLOAT);
			vboAttribute.SetElementsTotalCount(48);
			unsigned elementsPerAttr[4] = { 3, 3, 2, 0 };	// 3 position, 3 normal, 2 texture
			vboAttribute.SetInterleavedElementsPerAttribute(elementsPerAttr);
			return vboAttribute;
		}
		else if (PRIMITIVE_TRIANGLE == typeOfPrimitive)
		{
			vboAttribute.SetAttributeType(VBO_INTERLEAVED);
			vboAttribute.SetDataPointer((void*)triangleInterleavedData);
			vboAttribute.SetElementsSize(sizeof(float));
			vboAttribute.SetElementType(GL_FLOAT);
			vboAttribute.SetElementsTotalCount(24);
			unsigned elementsPerAttr[4] = { 3, 3, 2, 0 };	// 3 position, 3 normal, 2 texture
			vboAttribute.SetInterleavedElementsPerAttribute(elementsPerAttr);
			return vboAttribute;
		}
	}
};