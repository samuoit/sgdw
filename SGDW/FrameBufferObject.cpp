/*
	This is the implementation for FrameBufferObject class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

#include "FrameBufferObject.h"


FrameBufferObject::FrameBufferObject(unsigned colorAttachmentsNumber)
{
	this->colorAttachmentsNumber = colorAttachmentsNumber;

	colorAttachments = new GLuint[this->colorAttachmentsNumber];

	// buffers is required as a parameter for glDrawBuffers
	buffers = new GLenum[this->colorAttachmentsNumber];
	for (int i = 0; i < this->colorAttachmentsNumber; i++)
	{
		buffers[i] = GL_COLOR_ATTACHMENT0 + i;
	}

	glGenFramebuffers(1, &frameBuffer);

}

FrameBufferObject::~FrameBufferObject()
{
	Destroy();
}

void FrameBufferObject::InitDepthTexture(unsigned width, unsigned height)
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	// create depth texture
	glGenTextures(1, &depthAttachment);
	glBindTexture(GL_TEXTURE_2D, depthAttachment);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT24, width, height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// bind texture to fbo
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthAttachment, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
}

void FrameBufferObject::InitColorTexture(unsigned index, unsigned width, unsigned height, GLint internalFormat, GLint filter, GLint wrap)
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	// create color texture
	glGenTextures(1, &colorAttachments[index]);
	glBindTexture(GL_TEXTURE_2D, colorAttachments[index]);
	glTexStorage2D(GL_TEXTURE_2D, 1, internalFormat, width, height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

	// bind texture to fbo
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, GL_TEXTURE_2D, colorAttachments[index], 0);
	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
}

bool FrameBufferObject::Check()
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		Destroy();
		return false;
	}

	return true;
}

// Clears all OpenGL memory
void FrameBufferObject::Destroy()
{
	if (buffers != nullptr)
	{
		delete[] buffers;
		buffers = nullptr;
	}

	if (colorAttachments != nullptr)
	{
		for (int i = 0; i < this->colorAttachmentsNumber; i++)
		{
			glDeleteTextures(1, &colorAttachments[i]);
		}

		delete[] colorAttachments;
		colorAttachments = nullptr;
	}

	if (depthAttachment != GL_NONE)
	{
		glDeleteTextures(1, &depthAttachment);
		depthAttachment = GL_NONE;
	}

	this->colorAttachmentsNumber = 0;
}

// Clears all attached textures
void FrameBufferObject::Clear()
{
	GLbitfield temp = 0;

	if (depthAttachment != GL_NONE)
	{
		temp = temp | GL_DEPTH_BUFFER_BIT;
	}

	if (colorAttachments != nullptr)
	{
		temp = temp | GL_COLOR_BUFFER_BIT;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glClear(temp);
	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
}

void FrameBufferObject::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glDrawBuffers(this->colorAttachmentsNumber, buffers);
}

void FrameBufferObject::UnBind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
}

void FrameBufferObject::MoveToBackBuffer(int windowWidth, int windowHeight)
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GL_NONE);
	glBlitFramebuffer(0, 0, windowWidth, windowHeight, 0, 0, windowWidth, windowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
}

GLuint FrameBufferObject::GetColorHandle(unsigned index) const
{
	return colorAttachments[index];
}

GLuint FrameBufferObject::GetDepthHandle() const
{
	return depthAttachment;
}

void FrameBufferObject::RenderToCubeMap(GLuint* texture, float width, float height)
{
	// Create the cubemap
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Set initial textures
	for (int i = 0; i < 6; ++i)
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

	// Attach FBO
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, frameBuffer);

	// Attach cubemap textures
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X, *texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, *texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, *texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, *texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, *texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, *texture, 0);

	// Check for framebuffer completeness
	Check();

	// Disable FBO and cube map texture
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

}