/*
	This is the implementation for Texture2D class. 
	Samir Suljkanovic 2016
*/

#include "Engine.h"

Texture::Texture(){}

Texture::~Texture()
{
	Destroy();
}

bool Texture::Load(const char* filePath)
{
	txHandle = SOIL_load_OGL_texture(filePath, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);

	// Check if file with texture was not loaded
	if (txHandle == 0)
	{
		// Return false and display message if texture loading failed
		std::cout << "Texture failed to load.\n" << SOIL_last_result() << "\n";
		return false;
	}

	// - The texture was loaded -
	// printf("\nTexture loaded and handle generated: %u", txHandle);

	// Bind texture, and set texture parameters, U axis, and V axis for texture maping
	glBindTexture(GL_TEXTURE_2D, txHandle);											// Bind texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);				// Magnification filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// Minification filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);			// U axis - clamping texture to edge
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);			// V axis - ...

	return true;	// Return true if texture loading was successful
}

GLuint Texture::GetHandle()
{
	return txHandle;																// Return SOIL assigned texture handle
}

void Texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, txHandle);											// Bind texture
}

void Texture::Unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);												// Unbind texture i.e. set handle to 0
}

void Texture::Destroy()
{
	// If txHandle exists delete it from VRAM
	if (txHandle != 0)
	{
		glDeleteTextures(1, &txHandle);												// Delete texture from GPU
		txHandle = 0;																// Reset texture's handle
	}
}