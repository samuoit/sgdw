/*
	This is the implementation for VertexBufferObject class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

VertexBufferObject::VertexBufferObject()
{
	vboHandles.clear();		// Clear content of vboHandles
	vaoHandle = 0;			// Reset vaoHandle
}

VertexBufferObject::~VertexBufferObject()
{
	Destroy();				// Cleanup
}

void VertexBufferObject::AddAttributesData(VBOAttribute &attribute)
{
	vboAttributes.push_back(attribute);					// Add new VBO attribute to collection of attributes for this VBO
}

void VertexBufferObject::SetInterleaved(bool isIntrleaved)
{
	isInterleaved = isIntrleaved;
}

void VertexBufferObject::CreateVBOs(bool isInterleaved)
{
	// Setup VAO - A VAO keeps track of all of the state changes necessary for rendering
	glGenVertexArrays(1, &vaoHandle);					// Allocate memory for a new VAO (Vertex Array Object).
	glBindVertexArray(vaoHandle);						// Bind the vertex array (glBindVertexArray)\

	if (!isInterleaved)	// not interleaving
	{
		this->isInterleaved = false;					// Non-interleaved VBO

		unsigned bufferCount = vboAttributes.size();	// Allocate memory for all VBOs (Vertex Buffer Objects)
		vboHandles.resize(bufferCount);					// Resize vector for VBO handles to store all VBOs
		glGenBuffers(bufferCount, &vboHandles[0]);		// Generate buffers to contain vboHandles vector elements
		
		// Loop for each VBO to add it
		for (int i = 0; i < vboHandles.size(); i++)
		{
			// Enable vertex array of particular type. Layout locations in Vertex shader must match order of this enabled array data.
			glEnableVertexAttribArray(vboAttributes[i].GetAttributeType());	
			glBindBuffer(GL_ARRAY_BUFFER, vboHandles[i]);						// Bind the current VBO to GPU buffer

			// Send data to bound GPU buffer
			glBufferData(GL_ARRAY_BUFFER, vboAttributes[i].GetElementsSize() * vboAttributes[i].GetElementsTotalCount(), vboAttributes[i].GetDataPointer(), GL_STATIC_DRAW);
			
			// Set Vertex attribute pointer to specify how data in VBO is ordered
			glVertexAttribPointer(vboAttributes[i].GetAttributeType(), vboAttributes[i].GetElementsPerAttribute(), vboAttributes[i].GetElementType(), GL_FALSE, 0, 0);
			
			glBindBuffer(GL_ARRAY_BUFFER, 0);									// Unbind VBO from GPU buffer
		}

		// Unbind the VAO - When we want to draw this VBO, we just need to bind the VAO once. Binding the VAO will cause OpenGL to set the necessary states for us.
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		this->isInterleaved = true;							// This is interleaved VBO

		unsigned bufferCount = vboAttributes.size();
		vboHandles.resize(bufferCount);
		glGenBuffers(bufferCount, &vboHandles[0]);

		glEnableVertexAttribArray(VBO_POSITION);			// Enable vertex attribute array for 'vertex positions'
		glEnableVertexAttribArray(VBO_NORMAL);				// Enable vertex attribute array for 'vertex normals'
		glEnableVertexAttribArray(VBO_TEXTURE_UV);			// Enable vertex attribute array for 'vertex texture UVs'

		glBindBuffer(GL_ARRAY_BUFFER, vboHandles[0]);
		glBufferData(GL_ARRAY_BUFFER, vboAttributes[0].GetElementsSize() * vboAttributes[0].GetElementsTotalCount(), vboAttributes[0].GetDataPointer(), GL_STATIC_DRAW);
		
		// --- Needed for stride and offset ---
		unsigned vertexSize =								// Total Size of Vertex in Bytes
			(
				vboAttributes[0].GetInterleavedElementsPerAttribute()[0] + 
				vboAttributes[0].GetInterleavedElementsPerAttribute()[1] + 
				vboAttributes[0].GetInterleavedElementsPerAttribute()[2]
			) * sizeof(float);
		
		const void* offsetToNormals = 
			(void*)(vboAttributes[0].GetInterleavedElementsPerAttribute()[0] * sizeof(float));			// Offset to normals data
		
		const void* offsetToTextureUVs = 
			(void*)(vboAttributes[0].GetInterleavedElementsPerAttribute()[0] * 
			sizeof(float)+vboAttributes[0].GetInterleavedElementsPerAttribute()[1] * sizeof(float));	// Offset to texture data

		// printf("\nVertex Size: %u", vertexSize);
		// printf("\nOffset to normals %u , offset to texture uvs %u", offsetToNormals, offsetToTextureUVs);

		// Pointer to arrays of data (vertex positions, normals, texture UVs)
		// --- Positions ---
		glVertexAttribPointer(VBO_POSITION, vboAttributes[0].GetInterleavedElementsPerAttribute()[0], vboAttributes[0].GetElementType(), GL_FALSE, vertexSize, 0);

		// --- Normals ---
		glVertexAttribPointer(VBO_NORMAL, vboAttributes[0].GetInterleavedElementsPerAttribute()[1], vboAttributes[0].GetElementType(), GL_FALSE, vertexSize, offsetToNormals);

		// --- Texture UVs ---
		glVertexAttribPointer(VBO_TEXTURE_UV, vboAttributes[0].GetInterleavedElementsPerAttribute()[2], vboAttributes[0].GetElementType(), GL_FALSE, vertexSize, offsetToTextureUVs);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
}

void VertexBufferObject::Draw()
{
	unsigned elementsCount = 0;

	if (isInterleaved)
	{
		elementsCount =
			vboAttributes[0].GetElementsTotalCount() /
			vboAttributes[0].GetInterleavedElementsPerAttribute()[0] +
			vboAttributes[0].GetInterleavedElementsPerAttribute()[1] +
			vboAttributes[0].GetInterleavedElementsPerAttribute()[2];

		//printf("\nVBO Attributes positions: %u, normals: %u, texture UVs: %u", 
		//	vboAttributes[0].GetInterleavedElementsPerAttribute()[0], 
		//	vboAttributes[0].GetInterleavedElementsPerAttribute()[1],
		//	vboAttributes[0].GetInterleavedElementsPerAttribute()[2]);
	}
	else
	{
		elementsCount =
			vboAttributes[0].GetElementsTotalCount() /
			vboAttributes[0].GetElementsPerAttribute();
	}

	// printf("\nElements Count %u", elementsCount);

	glBindVertexArray(vaoHandle);							// Bind the VAO (glBindVertexArray)
	glDrawArrays(GL_TRIANGLES, 0, elementsCount);			// Draw VBO (glDrawArrays)
}

void VertexBufferObject::Destroy()
{
	if (vaoHandle)
	{
		glDeleteVertexArrays(1, &vaoHandle);				// Delete vertex array object(s)
		glDeleteBuffers(vboHandles.size(), &vboHandles[0]);	// Delete VBO buffers
	}

	vboHandles.clear();										// Clear vector (collection) of VBO handles
	vboAttributes.clear();									// Clear vector (collection) of VBO attributes
}

int VertexBufferObject::GetVBOSize()
{
	return vboHandles.size();
}