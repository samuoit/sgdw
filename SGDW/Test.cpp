///*
//This is the implementation for  is the main file for GDW Project.
//Samir Suljkanovic 2016
//Parts of the code in  This is the implementation for  file adapted from http://openglbook.com/chapter-1-getting-started.html
//**/
//#include "Engine.h"
//
//int
//windowWidth = 800,
//windowHeight = 600,
//WindowHandle = 0;
//
//unsigned frameCount = 0;
//
//GLuint
//VertexShaderId,
//FragmentShaderId,
//ProgramId,
//VaoId,
//VboId,
//ColorBufferId;
//
//const GLchar* VertexShader =
//{
//	"#version 400\n"\
//
//	"layout(location=0) in vec4 in_Position;\n"\
//	"layout(location=1) in vec4 in_Color;\n"\
//	"out vec4 ex_Color;\n"\
//
//	"void main(void)\n"\
//	"{\n"\
//	"  gl_Position = in_Position;\n"\
//	"  ex_Color = in_Color;\n"\
//	"}\n"
//};
//
//const GLchar* FragmentShader =
//{
//	"#version 400\n"\
//
//	"in vec4 ex_Color;\n"\
//	"out vec4 out_Color;\n"\
//
//	"void main(void)\n"\
//	"{\n"\
//	"  out_Color = ex_Color;\n"\
//	"}\n"
//};
//
//void Initialize(int, char*[]);
//void InitWindow(int, char*[]);
//void ResizeFunction(int, int);
//void RenderFunction(void);
//void TimerFunction(int);
//void IdleFunction(void);
//void Cleanup(void);
//void CreateVBO(void);
//void DestroyVBO(void);
//void CreateShaders(void);
//void DestroyShaders(void);
//
//int main(int argc, char* argv[])
//{
//	Initialize(argc, argv);
//
//	glutMainLoop();
//
//	exit(EXIT_SUCCESS);
//}
//
//void Initialize(int argc, char* argv[])
//{
//	GLenum GlewInitResult;
//
//	InitWindow(argc, argv);
//
//	GlewInitResult = glewInit();
//
//	if (GLEW_OK != GlewInitResult) {
//		fprintf(stderr, "ERROR: %s\n", glewGetErrorString(GlewInitResult));
//		exit(EXIT_FAILURE);
//	}
//
//	fprintf(stdout, "INFO: OpenGL Version: %s\n", glGetString(GL_VERSION));
//
//	CreateShaders();
//	CreateVBO();
//
//	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
//}
//
//void InitWindow(int argc, char* argv[])
//{
//	glutInit(&argc, argv);
//
//	glutInitContextVersion(4, 0);
//	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
//	glutInitContextProfile(GLUT_CORE_PROFILE);
//
//	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
//
//	glutInitWindowSize(windowWidth, windowHeight);
//
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
//
//	WindowHandle = glutCreateWindow(WINDOW_TITLE_PREFIX);
//
//	if (WindowHandle < 1) {
//		fprintf(stderr, "ERROR: Could not create a new window.\n");
//		exit(EXIT_FAILURE);
//	}
//
//	glutReshapeFunc(ResizeFunction);
//	glutDisplayFunc(RenderFunction);
//	glutIdleFunc(IdleFunction);
//	glutTimerFunc(0, TimerFunction, 0);
//	glutCloseFunc(Cleanup);
//}
//
//void ResizeFunction(int Width, int Height)
//{
//	windowWidth = Width;
//	windowHeight = Height;
//	glViewport(0, 0, windowWidth, windowHeight);
//}
//
//void RenderFunction(void)
//{
//	++frameCount;
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	glDrawArrays(GL_TRIANGLES, 0, 3);
//
//	glutSwapBuffers();
//	glutPostRedisplay();
//}
//
//void IdleFunction(void)
//{
//	glutPostRedisplay();
//}
//
//void TimerFunction(int Value)
//{
//	if (0 != Value) {
//		char* TempString = (char*)malloc(512 + strlen(WINDOW_TITLE_PREFIX));
//
//		sprintf(TempString, "%s: %d Frames Per Second @ %d x %d", WINDOW_TITLE_PREFIX, frameCount * 4, windowWidth, windowHeight);
//
//		glutSetWindowTitle(TempString);
//		free(TempString);
//	}
//
//	frameCount = 0;
//	glutTimerFunc(250, TimerFunction, 1);
//}
//
//void Cleanup(void)
//{
//	DestroyShaders();
//	DestroyVBO();
//}
//
//void CreateVBO(void)
//{
//	GLfloat Vertices[] = {
//		-0.8f, -0.8f, 0.0f, 1.0f,
//		0.0f, 0.8f, 0.0f, 1.0f,
//		0.8f, -0.8f, 0.0f, 1.0f
//	};
//
//	GLfloat Colors[] = {
//		1.0f, 0.0f, 0.0f, 1.0f,
//		0.0f, 1.0f, 0.0f, 1.0f,
//		0.0f, 0.0f, 1.0f, 1.0f
//	};
//
//	GLenum ErrorCheckValue = glGetError();
//
//	glGenVertexArrays(1, &VaoId);
//	glBindVertexArray(VaoId);
//
//	glGenBuffers(1, &VboId);
//	glBindBuffer(GL_ARRAY_BUFFER, VboId);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
//	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
//	glEnableVertexAttribArray(0);
//
//	glGenBuffers(1, &ColorBufferId);
//	glBindBuffer(GL_ARRAY_BUFFER, ColorBufferId);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(Colors), Colors, GL_STATIC_DRAW);
//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
//	glEnableVertexAttribArray(1);
//
//	ErrorCheckValue = glGetError();
//	if (ErrorCheckValue != GL_NO_ERROR)
//	{
//		fprintf(
//			stderr,
//			"ERROR: Could not create a VBO: %s \n",
//			gluErrorString(ErrorCheckValue)
//			);
//
//		exit(-1);
//	}
//}
//
//void DestroyVBO(void)
//{
//	GLenum ErrorCheckValue = glGetError();
//
//	glDisableVertexAttribArray(1);
//	glDisableVertexAttribArray(0);
//
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//	glDeleteBuffers(1, &ColorBufferId);
//	glDeleteBuffers(1, &VboId);
//
//	glBindVertexArray(0);
//	glDeleteVertexArrays(1, &VaoId);
//
//	ErrorCheckValue = glGetError();
//	if (ErrorCheckValue != GL_NO_ERROR)
//	{
//		fprintf(
//			stderr,
//			"ERROR: Could not destroy the VBO: %s \n",
//			gluErrorString(ErrorCheckValue)
//			);
//
//		exit(-1);
//	}
//}
//
//void CreateShaders(void)
//{
//	GLenum ErrorCheckValue = glGetError();
//
//	VertexShaderId = glCreateShader(GL_VERTEX_SHADER);
//	glShaderSource(VertexShaderId, 1, &VertexShader, NULL);
//	glCompileShader(VertexShaderId);
//
//	FragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
//	glShaderSource(FragmentShaderId, 1, &FragmentShader, NULL);
//	glCompileShader(FragmentShaderId);
//
//	ProgramId = glCreateProgram();
//	glAttachShader(ProgramId, VertexShaderId);
//	glAttachShader(ProgramId, FragmentShaderId);
//	glLinkProgram(ProgramId);
//	glUseProgram(ProgramId);
//
//	ErrorCheckValue = glGetError();
//	if (ErrorCheckValue != GL_NO_ERROR)
//	{
//		fprintf(
//			stderr,
//			"ERROR: Could not create the shaders: %s \n",
//			gluErrorString(ErrorCheckValue)
//			);
//
//		exit(-1);
//	}
//}
//
//void DestroyShaders(void)
//{
//	GLenum ErrorCheckValue = glGetError();
//
//	glUseProgram(0);
//
//	glDetachShader(ProgramId, VertexShaderId);
//	glDetachShader(ProgramId, FragmentShaderId);
//
//	glDeleteShader(FragmentShaderId);
//	glDeleteShader(VertexShaderId);
//
//	glDeleteProgram(ProgramId);
//
//	ErrorCheckValue = glGetError();
//	if (ErrorCheckValue != GL_NO_ERROR)
//	{
//		fprintf(
//			stderr,
//			"ERROR: Could not destroy the shaders: %s \n",
//			gluErrorString(ErrorCheckValue)
//			);
//
//		exit(-1);
//	}
//}












///*
//	This class contains static functions for testing of various components of GDW project e.g. Matrix or Vector classes.
//*/
//
//#include "Engine.h"
//
//// Templated function for equality testing of two same type values
//template <typename T_Num>
//static void CHECK_EQUALS(T_Num a, T_Num b)
//{
//	if (a == b)
//	{
//		std::cout << a << " == " << b << " --> PASSED TEST!" << std::endl; 
//	}
//	else
//	{
//		std::cout << a << " != " << b << " --> FAILED TEST!" << std::endl;
//	}
//}
//
//static void testList()
//{
//	// Some floats usable as coordinates
//	float 
//		X = 3.0f, 
//		Y = 4.0f,
//		Z = 5.0f,
//		W = 2.0f; 
//
//	// ================================ Vector2 tests ================================ 
//	// Test various constructors:
//	Vector2 vec2_A;						// Default constructor
//	Vector2 vec2_B(X, Y);				// 2 parameter constructor
//	Vector2 vec2_C(W);					// Both Vector2 coordinates should equal W
//	Vector2 vec2_D(vec2_A);				// Copy values of vec2_A to another vector
//
//	// Testing overloaded operators
//	CHECK_EQUALS(vec2_C, vec2_D);		// Equality operator on two vectors (should be the same as one was created from another)
//	vec2_D = vec2_A;					// Use assignment operator to assign one vector to another and check if they are equal
//	CHECK_EQUALS(vec2_D, vec2_A);		// Equality operator on two vectors after assigning value of one to another
//	vec2_D += vec2_A;					// This should add value of vec2_A to vec2D which should be identical at this point
//	CHECK_EQUALS(vec2_D, vec2_A * 2);	// This also tests Vector2 * scalar operation
//	CHECK_EQUALS(vec2_D, vec2_A * 2);	// This tests Vector2 / scalar operation
//	vec2_D -= vec2_A;					// Subtract vec2_A from vec2_D, now both should be the same again
//	CHECK_EQUALS(vec2_D, vec2_A);
//
//
//
//	// ================================ Vector3 Tests ================================
//
//	// ================================ Vector4 Tests ================================ 
//
//	// ================================ Quaternion Tests ================================ 
//
//	// ================================ Matrix3x3 Tests ================================ 
//
//	// ================================ Matrix4x4 Tests ================================ 
//
//
//	// Use the CHECK_EQUALS function to test your vector class. ALL methods MUST be
//	// tested.
//	//
//	// Example:
//	// Vector3f v1(1, 2, 3);
//	// Vector3f v2(2, 3, 4);
//	//
//	// Vector3f v3 = v1 + v2;
//	// CHECK_EQUALS(v3[0], 3);
//	// CHECK_EQUALS(v3[1], 5);
//	// CHECK_EQUALS(v3[2], 7);
//
//	CHECK_EQUALS(5, 6); // 5 is obviously not equal to 6, the console reflects this
//	CHECK_EQUALS(6, 6); // 6 is equal to 6, the console reflects this
//}
//
//
//void Vector2::Negated()
//{
//	x = x * -1;
//	y = y * -1;
//}
//
//float Vector2::Dot(const Vector2 &vec)
//{
//	return x * vec.x + y * vec.y;
//}
//
//float Vector2::Magnitude()
//{
//	return (float)sqrt(x * x + y * y);
//}
//
//float Vector2::MagnitudeSquared()
//{
//	return (float)(x * x + y * y);
//}
//
//Vector2 Vector2::Zero()
//{
//	return Vector2();
//}
//
//Vector2 Vector2::Normalize()
//{
//	float magnitude = Magnitude();
//	return operator/(magnitude);
//}
//
//Vector2 Vector2::Perpendicular()
//{
//	return Vector2(-y, x);	// Just turn vector by 90 degrees and return it
//}
//
//void Vector2::Rotate(float angle)
//{
//	float radians = angle * DEGREES_TO_RADIANS;
//	float tempX = cos(radians) * x - sin(radians) * y;
//
//	y = sin(radians) * x + cos(radians) * y;
//	x = tempX;
//}
//
//Vector2 Vector2::AngleToVector(float angle)
//{
//	return Vector2(cos(angle * DEGREES_TO_RADIANS), sin(angle * DEGREES_TO_RADIANS));
//}
//
//float Vector2::VectorToAngle(const Vector2 &vec)
//{
//	return atan2(vec.y, vec.x) * RADIANS_TO_DEGREES;
//}
//
//void Vector2::Reflect(Vector2& normalizedVec)
//{
//	Vector2 temp;
//	temp = (normalizedVec * -1.0f) * (Dot(normalizedVec) * 2.0f);
//	*this += temp;
//}