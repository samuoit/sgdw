/*
*/

#include "Engine.h"

Particle::Particle() {}

Particle::~Particle() {}

unsigned Particle::GetType()
{
	return type;
}

Vector3 Particle::GetPosition()
{
	return position;
}

Vector3 Particle::GetVelocity()
{
	return velocity;
}

Vector3 Particle::GetColor()
{
	return color;
}

float Particle::GetLifeTimeInMs()
{
	return lifetimeInMs;
}

float Particle::GetSize()
{
	return size;
}

void Particle::SetType(unsigned newType)
{
	type = newType;
}

void Particle::SetPosition(Vector3 newPosition)
{
	position = newPosition;
}

void Particle::SetVelocity(Vector3 newVelocity)
{
	velocity = newVelocity;
}

void Particle::SetColor(Vector3 newColor)
{
	color = newColor;
}

void Particle::SetLifeTimeInMs(float lifetimeInMilisecs)
{
	lifetimeInMs = lifetimeInMilisecs;
}

void Particle::SetSize(float newSize)
{
	size = newSize;
}