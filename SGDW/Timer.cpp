/*
	This is the implementation for Timer class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

Timer::Timer()
{
	previousTime = glutGet(GLUT_ELAPSED_TIME);		// glut.h method to get elapsed time and set it as previous time
	this->TickTock();								// Start timer update method
}

Timer::~Timer(){}

float Timer::TickTock()
{
	currentTime = glutGet(GLUT_ELAPSED_TIME);		// Get current time
	elapsedTime = currentTime - previousTime;		// Calculate elapsed time by subtracting current elapsed time with previous elapsed time
	previousTime = currentTime;						// Set current time as previous time
	return elapsedTime;								// Return calculated elapsed time
}

float Timer::GetElapsedTimeMS()
{
	return elapsedTime;								// Current elapsed time is in MS (milliseconds)
}

float Timer::GetElapsedTimeSeconds()
{
	return elapsedTime / 1000.f;					// Since elapsed time is in MS to convert to seconds need to devide by 1000 as 1 sec = 1000 MS
}

float Timer::GetCurrentTime()
{
	return currentTime;								// Current i.e. the last elapsed time recorded
}