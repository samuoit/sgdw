/*
	This is the implementation for ShaderProgram class.
	Samir Suljkanovic 2016
*/
#include "Engine.h"

ShaderProgram::ShaderProgram()
{
	handle = 0;		// Initialize handle for shader program
}

ShaderProgram::~ShaderProgram()
{
	Destroy();		// Call cleanup for shader program
}

void ShaderProgram::AttachShader(Shader shader)
{
	// If no shader program was created then create it
	if (handle == 0) handle = glCreateProgram();

	// If shader provided has handle, attach provided shader to shader program handle
	if (shader.GetHandle())	glAttachShader(handle, shader.GetHandle());	// Attach shader program
}

int ShaderProgram::GetUniformLocation(char* uniformName)
{
	int tempHandle = 0;		// Store for temporary handle

	// Use iterator to lookup for uniformName value in map values
	std::map<std::string, int>::iterator itr = uniformLocations.find(uniformName);
	if (itr != uniformLocations.cend())	// If itr has not reached the end of the map then value was found
	{
		tempHandle = itr->second;
		return tempHandle;
	}

	// Another way: Iterate over the map to look for uniformName value in first set of map values
	// for (auto itr = uniformLocations.cbegin(); itr != uniformLocations.cend(); ++itr)
	// {
	//	// Get value of handle (second mapped value) if first matches the parameter specified as uniformName
	//	if (itr->first == uniformName)
	//	{
	//		tempHandle = itr->second;
	//		return tempHandle;				// If uniform name was found return associated handle, this also terminates function here
	//	}
	// }

	// If function did not terminate at this point then uniform was not mapped, so get it from GL
	tempHandle = glGetUniformLocation(handle, uniformName);	// Get GL's uniform location

	// Insert pair (uniform name / handle) into map to make it available for the use on next request to this function
	uniformLocations.insert(std::pair<std::string, int>(uniformName, tempHandle));
	
	// Return handle
	return tempHandle;
}

void ShaderProgram::SendUniformInt(char* uniformName, int intValue)
{
	glUniform1i(GetUniformLocation(uniformName), intValue);
}

void ShaderProgram::SendUniformFloat(char* uniformName, float floatValue)
{
	glUniform1f(GetUniformLocation(uniformName), floatValue);
}

void ShaderProgram::SendUniformVec3(char* uniformName, Vector3 vec3Value)
{
	glUniform3fv(GetUniformLocation(uniformName), 1, vec3Value.coords);
}

void ShaderProgram::SendUniformVec4(char* uniformName, Vector4 vec4Value)
{
	glUniform4fv(GetUniformLocation(uniformName), 1, vec4Value.coords);
}

void ShaderProgram::SendUniformMatrix(char* uniformName, Matrix matrix)
{
	glUniformMatrix4fv(GetUniformLocation(uniformName), 1, false, matrix.elements);
}

void ShaderProgram::SendUniformBool(char* uniformName, bool boolValue)
{
	glUniform1i(GetUniformLocation(uniformName), boolValue);
}

void ShaderProgram::SendUniformFloatArray(char* uniformName, float* arr, int arrSize)
{
	glUniform1fv(GetUniformLocation(uniformName), arrSize, arr);
}

void ShaderProgram::SendUniformVec3Array(char* uniformName, Vector3* arr, int arrSize)
{
	glUniform3fv(GetUniformLocation(uniformName), arrSize, arr->coords);
}

unsigned ShaderProgram::GetHandle()
{
	return handle;
}

int ShaderProgram::LinkProgram()
{
	if (handle)	// If shader program exists
	{
		glLinkProgram(handle);	// Link shader program

		// Based on the status of link display message if it was successful and return handle, or display error log and return 0
		int linkStatus;
		glGetProgramiv(handle, GL_LINK_STATUS, &linkStatus);

		if (linkStatus)
		{
			std::cout << "Shader program linked successfully." << std::endl;
			isLinked = true;
			return handle;
		}
		
		std::cout << "Shader program NOT linked" << std::endl;
		isLinked = false;
		
		int logLength;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &logLength);

		std::string log(logLength, ' ');

		//char * writable = new char[log.size() + 1];
		//std::copy(log.begin(), log.end(), writable);
		//writable[log.size()] = '\0'; // terminating 0

		glGetShaderInfoLog(handle, logLength, &logLength, &log[0]);

		printf("\nLog length : %d , Log: %s\n", logLength, log.c_str());
	}

	return 0;
}

void ShaderProgram::Bind()
{
	glUseProgram(handle);
	// if (isLinked) glUseProgram(handle);
}

void ShaderProgram::Unbind()
{
	glUseProgram(0);
	// if (isLinked) glUseProgram(0);
}

void ShaderProgram::Destroy()
{
	if (handle)
	{
		if (isLinked) glDeleteProgram(handle);
		handle = 0;
	}
}