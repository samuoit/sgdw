/*
	This is the header for ParticleSystem class.
	Samir Suljkanovic 2016
*/
#ifndef SGDW_PARTICLE_SYSTEM_H
#define SGDW_PARTICLE_SYSTEM_H

struct ParticlesData
{
	Vector3 *Positions = nullptr;
	Vector3 *Velocities = nullptr;
	float *Size = nullptr;
	float *Alpha = nullptr;
	float *Ages = nullptr;
	float *Lifetimes = nullptr;
};

// Linear interpolation
template<class T>
T LERP(const T &data1, const T &data2, float u)
{
	return data1 * (1.0f - u) + data2 * u;
}

class ParticleSystem
{
public:
	ParticleSystem();
	~ParticleSystem();

	bool Init														// Initialize particle system
		(
		Vector3 positionMinimum,										// Minimum position where particle can be generated
		Vector3 positionMaximum,										// Maximum position where particle can be generated
		Vector3 velocityMinimum,										// Minimum velocity of particle in X, Y, and Z directions
		Vector3 velocityMaximum,										// Maximum velocity of particle in X, Y, and Z directions
		Vector2 lifeTimeRangeMinMax,									// Minimum and maximum life time of a particle (x - min, y - max)
		Vector2 alphaRangeMinMax,										// Minimum and maximum alpha range (x - min, y - max)
		Vector2 sizeRangeMinMax,										// Minimum and maximum size range (x - min, y - max)
		unsigned int maxParticles,										// Maximum number of particles to spawn at a time
		unsigned int rate,												// Rate of spawning
		PARTICLE_TYPE useParticleType,									// Type of particle (used for some default alpha/size settings and texture settings)
		const char* fileParticlePath									// If type of particle used is PARTICLE_CUSTOM then this argument provides path to custom texture
		);
	void Update(float elapsed);										// Run update on particles
	void Render();													// Render particles
	void Destroy();													// Destroy particles system (cleanup memory resources taken by particle system components)

	void SetParticleType											// Sets particle attributes, if type is PARTICLE_CUSTOM then path of custom file can be given
		(
		PARTICLE_TYPE particleType,									// Type of particle used
		const char* textureFilePath = ""							// File path for custom texture (if type is PARTICLE_CUSTOM)
		);

	void SetMatrices												// Set matrices used by shaders
		(
		Matrix projectionSpace = IDENTITY_MATRIX,						// Projection matrix - 2D space affected by clipping
		Matrix viewSpace = IDENTITY_MATRIX,								// View matrix - world seen from point of view
		Matrix worldSpace = IDENTITY_MATRIX								// Model matrix - engine position in the world
		);

	float RandomFloat(float min, float max, unsigned seed = 0);
	void SetRandomSeed(unsigned seed = 0);

private:

	bool isInitialized;												// Flag indicating if particle system is initialized or not

	GLuint vao = 0;													// VAO handle
	GLuint vboParticles = 0;										// VBO (interleaved) with all particle data
	GLuint vboPositions = 0;										// VBO with particles position data
	GLuint vboSizes = 0;											// VBO with particles size data
	GLuint vboAlphas = 0;											// VBO with particles alpha data
	
	ShaderProgram progParticleSystem;								// Shader program for particle system
	Shader vertParticles, geomParticles, fragParticles;				// Vertex, geometry, and fragment shaders for particle system

	PARTICLE_TYPE particleType;										// Type of each particle in the system
	ParticlesData particlesList;										// List of all particles
	Texture particleTexture;										// Texture for particles
	const char* particleTextureName;								// If type of particle is PARTICLE_CUSTOM, particleTextureName contains path of the file to be used for this particle

	float emitRate = 0.0f;											// Emission rate (particles are spawned every 'emitRate' time range)
	unsigned int countOfParticlesToCreate = 0;						// Count of particles to create
	unsigned int countOfParticlesCreated = 0;						// Count of particles currently created

	// Particles initialization data
	Vector3 positionRangeMinimum, positionRangeMaximum;				// Position ranges minimum and maximum on all axis
	Vector3 velocityRangeMinimum, velocityRangeMaximum;				// Velocity ranges minimum and maximum on all axis
	Vector2 lifetimeRange;											// Life time range (x - minimum, y - maximum)
	Vector2	alphaRange;												// Lerp for vertex color alpha (x - minimum, y - maximum)
	Vector2	sizeRange;												// Lerp size (x - minimum, y - maximum)

	// Matrices used by shaders
	Matrix projectionMatrix;										// Projection matrix
	Matrix viewMatrix;												// View matrix
	Matrix modelMatrix;												// Model matrix
};

#endif