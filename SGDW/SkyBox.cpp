/*
	This is the implementation for SkyBox class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

SkyBox::SkyBox()
{
	isInitialized = false;
}

SkyBox::~SkyBox(){}

bool SkyBox::Initialize()
{
	// Return true and exit if already initialized
	if (isInitialized) return true;

	// Set SkyBox cube points data (for a big skybox cube)
	float points[] = {
		-25.0f, 25.0f, -25.0f, -25.0f, -25.0f, -25.0f, 25.0f, -25.0f, -25.0f, 25.0f, -25.0f, -25.0f, 25.0f, 25.0f, -25.0f, -25.0f, 25.0f, -25.0f,
		-25.0f, -25.0f, 25.0f, -25.0f, -25.0f, -25.0f, -25.0f, 25.0f, -25.0f, -25.0f, 25.0f, -25.0f, -25.0f, 25.0f, 25.0f, -25.0f, -25.0f, 25.0f,
		25.0f, -25.0f, -25.0f, 25.0f, -25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, -25.0f, 25.0f, -25.0f, -25.0f,
		-25.0f, -25.0f, 25.0f, -25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, -25.0f, 25.0f, -25.0f, -25.0f, 25.0f,
		-25.0f, 25.0f, -25.0f, 25.0f, 25.0f, -25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, 25.0f, -25.0f, 25.0f, 25.0f, -25.0f, 25.0f, -25.0f,
		-25.0f, -25.0f, -25.0f, -25.0f, -25.0f, 25.0f, 25.0f, -25.0f, -25.0f, 25.0f, -25.0f, -25.0f, -25.0f, -25.0f, 25.0f, 25.0f, -25.0f, 25.0f
	};

	faceSize = 3;
	pointsFaces = 36;
	pointsSize = pointsFaces * faceSize * sizeof(float);

	// ---- ** Generate VBO and bind cube points data
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, pointsSize, &points, GL_STATIC_DRAW);

	// Generate VAO and bind VBO to it
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// ---- ** Initialize SkyBox shaders and shader program
	vertSkyBox.LoadShader(SHADER_FILE, "../assets/shaders/SkyBox.vert", GL_VERTEX_SHADER);
	fragSkyBox.LoadShader(SHADER_FILE, "../assets/shaders/SkyBox.frag", GL_FRAGMENT_SHADER);

	// Attach and link water shaders to water shader program
	sProgSkyBox.AttachShader(vertSkyBox);
	sProgSkyBox.AttachShader(fragSkyBox);
	sProgSkyBox.LinkProgram();

	// ---- ** Create cube texture
	glActiveTexture(GL_TEXTURE0);	// Activate texture
	glGenTextures(1, &texCube);		// Generate texture

	// Generate a cube-map texture to hold all the sides
	// load each image and copy into a side of the cube-map texture
	InitCubeSide(texCube, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "../assets/textures/SkyBox/negz.jpg");
	InitCubeSide(texCube, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "../assets/textures/SkyBox/posz.jpg");
	InitCubeSide(texCube, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "../assets/textures/SkyBox/posy.jpg");
	InitCubeSide(texCube, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "../assets/textures/SkyBox/negy.jpg");
	InitCubeSide(texCube, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "../assets/textures/SkyBox/negx.jpg");
	InitCubeSide(texCube, GL_TEXTURE_CUBE_MAP_POSITIVE_X, "../assets/textures/SkyBox/posx.jpg");
	
	// Format cube map texture
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	isInitialized = true;

	return isInitialized;
}

void SkyBox::Draw()
{
	// Disable depth checking
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);

	// Bind cube texture
	glActiveTexture(GL_TEXTURE0);						// Activate texture 0 slot
	glBindTexture(GL_TEXTURE_CUBE_MAP, texCube);		// Bind texture cube map

	// Bind shader program and send uniforms
	sProgSkyBox.Bind();
	sProgSkyBox.SendUniformMatrix("u_Projection", mProj);
	sProgSkyBox.SendUniformMatrix("u_View", mView);
	sProgSkyBox.SendUniformInt("u_TexCube", 0);
	
	// Bind VAO and SkyBox
	glBindVertexArray(vao);								// Bind VAO
	glDrawArrays(GL_TRIANGLES, 0, pointsFaces);			// Draw triangles with VAO
	
	// Enable depth checking
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);				// Unbind cube map texture
}

void SkyBox::Destroy()
{
	sProgSkyBox.Destroy();
	vertSkyBox.Destroy();
	fragSkyBox.Destroy();
}

bool SkyBox::InitCubeSide(GLuint texture, GLenum targetSide, const char* textureFile) 
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	int w, h;
	unsigned char* image_data = SOIL_load_image(textureFile, &w, &h, 0, SOIL_LOAD_RGBA);
	if (!image_data) {
		printf("ERROR: could not load %s\n", textureFile);
		return false;
	}
	// check if image dimensions are non-power-of-2
	if ((w & (w - 1)) != 0 || (h & (h - 1)) != 0) {
		fprintf(stderr, "WARNING: image %s is not power-of-2 dimensions\n", textureFile);
	}

	// copy pixel image data into side of cube map
	glTexImage2D(targetSide, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
	free(image_data);
	return true;
}

void SkyBox::SetMatrices(Matrix projection, Matrix view, Matrix model)
{
	mProj = projection;
	mView = view;
	mModel = model;
}