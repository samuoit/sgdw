/*
	This is the implementation for Vector4 class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

Vector4::Vector4()
{
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}

Vector4::Vector4(float X, float Y, float Z, float W)
{
	x = X;
	y = Y;
	z = Z;
	w = W;
}

Vector4::Vector4(float XYZW)
{
	Vector4(XYZW, XYZW, XYZW, XYZW);
}

Vector4::Vector4(const Vector4 &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	w = vec.w;
}

Vector4::~Vector4(){ /* DESTRUCTOR */ }

bool Vector4::operator==(const Vector4 &vec)
{
	return (vec.x == x && vec.y == y && vec.z == z && vec.w == w);
}

bool Vector4::operator!=(const Vector4 &vec)
{
	return (vec.x != x || vec.y != y || vec.z != z || vec.w != w);
}

void Vector4::operator=(const Vector4 &vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;
	w = vec.w;
}

void Vector4::operator+=(const Vector4 &vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
	w += vec.w;
}

void Vector4::operator-=(const Vector4 &vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
	w -= vec.w;
}

void Vector4::operator/=(float f)
{
	x /= f;
	y /= f;
	z /= f;
	w /= f;
}

void Vector4::operator*=(float f)
{
	x *= f;
	y *= f;
	z *= f;
	w *= f;
}

Vector4 Vector4::operator+(const Vector4 &vec)
{
	return Vector4(x + vec.x, y + vec.y, z + vec.z, w + vec.w);
}

Vector4 Vector4::operator-(const Vector4 &vec)
{
	return Vector4(x - vec.x, y - vec.y, z - vec.z, w - vec.w);
}

Vector4 Vector4::operator*(float scalar)
{
	return Vector4(x * scalar, y * scalar, z * scalar, w * scalar);
}

Vector4 Vector4::operator/(float scalar)
{
	return Vector4(x / scalar, y / scalar, z / scalar, w / scalar);
}

void Vector4::Negated()
{
	x = x * -1;
	y = y * -1;
	z = z * -1;
	w = w * -1;
}

float Vector4::Dot(const Vector4 &vec)
{
	return x * vec.x + y * vec.y + z * vec.z + w * vec.w;
}

float Vector4::Magnitude()
{
	return (float)sqrt(x * x + y * y + z * z + w * w);
}

float Vector4::MagnitudeSquared()
{
	return (float)(x * x + y * y + z * z + w * w);
}

Vector4 Vector4::Zero()
{
	return Vector4();
}

Vector4 Vector4::One()
{
	return Vector4(1.0f, 1.0f, 1.0f, 1.0f);
}

Vector4 Vector4::Normalize()
{
	float magnitude = Magnitude();
	return operator/(magnitude);
}

Vector4 Vector4::Cross(const Vector4 &vec)
{
	return Vector4(
		y * vec.z - z * vec.y,
		z * vec.x - x * vec.z,
		x * vec.y - y * vec.x,
		w * vec.w - w * vec.w
		);
}

void Vector4::Rotate(COORDINATE_AXIS vecAxis, float angle)
{
	float sinAngle = sin(DEGREES_TO_RADIANS * angle);
	float cosAngle = cos(DEGREES_TO_RADIANS * angle);

	switch (vecAxis)
	{
	case X_AXIS:
		y = y * cosAngle - z * sinAngle;
		z = y * sinAngle + z * cosAngle;
		break;
	case Y_AXIS:
		x = x * cosAngle + z * sinAngle;
		z = -x * sinAngle + z * cosAngle;
		break;
	case Z_AXIS:
		x = x * cosAngle - y * sinAngle;
		y = x * sinAngle + y * cosAngle;
		break;
	case W_AXIS:

		break;
	default:
		break;
	}
}