/*
	This is the implementation for Vector2 class.
	Samir Suljkanovic 2016
*/
#include "Engine.h"

Vector2::Vector2()
{
	x = 0;
	y = 0;
}

Vector2::Vector2(float X, float Y)
{
	x = X;
	y = Y;
}

Vector2::Vector2(float XY)
{
	Vector2(XY, XY);
}

Vector2::Vector2(const Vector2 &vec)
{
	x = vec.x;
	y = vec.y;
}

Vector2::~Vector2(){ /* DESTRUCTOR */ }

bool Vector2::operator==(const Vector2 &vec)
{
	if (x == vec.x && y == vec.y) 
		return true; 
	else 
		return false;
}

bool Vector2::operator!=(const Vector2 &vec)
{
	return (vec.x != x || vec.y != y);
}

void Vector2::operator=(const Vector2 &vec)
{
	x = vec.x;
	y = vec.y;
}

void Vector2::operator+=(const Vector2 &vec)
{
	x += vec.x;
	y += vec.y;
}

void Vector2::operator-=(const Vector2 &vec)
{
	x -= vec.x;
	y -= vec.y;
}

void Vector2::operator/=(float f)
{
	x /= f;
	y /= f;
}

void Vector2::operator*=(float f)
{
	x *= f;
	y *= f;
}

Vector2 Vector2::operator+(const Vector2 &vec)
{
	return Vector2(x + vec.x, y + vec.y);
}

Vector2 Vector2::operator-(const Vector2 &vec)
{
	return Vector2(x - vec.x, y - vec.y);
}

Vector2 Vector2::operator*(float scalar)
{
	return Vector2(x * scalar, y * scalar);
}

Vector2 Vector2::operator/(float scalar)
{
	return Vector2(x / scalar, y / scalar);
}

void Vector2::Negated()
{
	x = x * -1;
	y = y * -1;
}

float Vector2::Dot(const Vector2 &vec)
{
	return x * vec.x + y * vec.y;
}

float Vector2::Magnitude()
{
	return (float)sqrt(x * x + y * y);
}

float Vector2::MagnitudeSquared()
{
	return (float)(x * x + y * y);
}

Vector2 Vector2::Zero()
{
	return Vector2();
}

static Vector2 One()
{
	return Vector2(1.0f, 1.0f);
}

Vector2 Vector2::Normalize()
{
	float magnitude = Magnitude();
	return operator/(magnitude);
}

Vector2 Vector2::Perpendicular()
{
	return Vector2(-y, x);	// Just turn vector by 90 degrees and return it
}

void Vector2::Rotate(float angle)
{
	float radians = angle * DEGREES_TO_RADIANS;
	float tempX = cos(radians) * x - sin(radians) * y;

	y = sin(radians) * x + cos(radians) * y;
	x = tempX;
}

Vector2 Vector2::AngleToVector(float angle)
{
	return Vector2(cos(angle * DEGREES_TO_RADIANS), sin(angle * DEGREES_TO_RADIANS));
}

float Vector2::VectorToAngle(const Vector2 &vec)
{
	return atan2(vec.y, vec.x) * RADIANS_TO_DEGREES;
}

void Vector2::Reflect(Vector2& normalizedVec)
{
	Vector2 temp;
	temp = (normalizedVec * -1.0f) * (Dot(normalizedVec) * 2.0f);
	*this += temp;
}