/*
	This is the implementation for Game class.
	Samir Suljkanovic 2016
*/

#include "Engine.h"

// ====== SkyBox and Water ======
SkyBox skyBox;											// SkyBox
Water water;											// Water

// ====== PARTICLE SYSTEMS ======
ParticleSystem particleSparkles;						// Particle system (sparkles)
ParticleSystem particleSnow;							// Particle system (snow)
ParticleSystem particleCustom;							// Particle system (custom)

// ===== Reflection/Refraction Related =====
bool hasDynamicReflectionsPlane = false;				// Should this object reflect environment only (false), or the other non-reflective objects as well (true)
bool hasDynamicReflectionsSphere = false;				// Should this object reflect environment only (false), or the other non-reflective objects as well (true)

// ***** Constructor and destructor *****
Game::Game() : fboNegZ(1), fboPosZ(1), fboNegY(1), fboPosY(1), fboNegX(1), fboPosX(1) {}	// FrameBuffer initialization through Game constructor
Game::~Game(){}

// Display Keyboard Shortcuts used to navigate the scene
void DisplayUserControls(const std::string before_message = "", const std::string after_message = "")
{
	system("cls");
	if (before_message.size() > 0) printf("\n %s \n", before_message.c_str());
	// Display mapped keys
	printf("\n-------------------------------------------\n");
	printf("\nCamera (POV) Movement - Keyboard:\n");
	printf("\n-------------------------------------------\n");
	printf("\nMove:   W - Forward,  A - Left,  S - Back,  D - Right");
	printf("\nMove:   R - Up,       F - Down");
	printf("\nRotate: Z - Counter Clock Wise,  X - Clock Wise (Around viewer's Y Axis)");
	printf("\n\n-------------------------------------------\n");
	printf("\nMouse:\n");
	printf("\nLeft Click: Move Mouse Arround to Change Viewing Direction.");
	printf("\nScroll Wheel: Move-In and Move-Out.\n");
	printf("\n\n-------------------------------------------\n");
	if (after_message.size() > 0) printf("\n %s \n", after_message.c_str());
}

void Game::SetCameraProperties(float fovAngle, float zNear, float zFar)
{
	fieldOfView = fovAngle;
	nearPlane = zNear;
	farPlane = zFar;
}

void Game::Initialize()
{
	// ---------------------------------------------------------------------------
	// Initial Setup of Reflections (false = Environmental - Static / true = Reflecting non-reflective objects - Dynamic)
	// ---------------------------------------------------------------------------
	hasDynamicReflectionsPlane = true;
	hasDynamicReflectionsSphere = true;
	
	// ---------------------------------------------------------------------------
	// Initialize FrameBuffer Objects - initialize color and depth textures (used for Reflection/Refraction)
	// ---------------------------------------------------------------------------
	fboWidth = 2048;
	fboHeight = 2048;

	// FBO - to capture negative Z side
	fboNegZ.InitDepthTexture(fboWidth, fboHeight);													// Initialize Depth Texture
	fboNegZ.InitColorTexture(0, windowWidth, windowHeight, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE);	// Initialize Color Texture

	// FBO - to capture positive Z side
	fboPosZ.InitDepthTexture(fboWidth, fboHeight);													// Initialize Depth Texture
	fboPosZ.InitColorTexture(0, windowWidth, windowHeight, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE);	// Initialize Color Texture

	// FBO - to capture negative Y side
	fboNegY.InitDepthTexture(fboWidth, fboHeight);													// Initialize Depth Texture
	fboNegY.InitColorTexture(0, windowWidth, windowHeight, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE);	// Initialize Color Texture

	// FBO - to capture positive Y side
	fboPosY.InitDepthTexture(fboWidth, fboHeight);													// Initialize Depth Texture
	fboPosY.InitColorTexture(0, windowWidth, windowHeight, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE);	// Initialize Color Texture

	// FBO - to capture negative X side
	fboNegX.InitDepthTexture(fboWidth, fboHeight);													// Initialize Depth Texture
	fboNegX.InitColorTexture(0, windowWidth, windowHeight, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE);	// Initialize Color Texture

	// FBO - to capture positive X side
	fboPosX.InitDepthTexture(fboWidth, fboHeight);													// Initialize Depth Texture
	fboPosX.InitColorTexture(0, windowWidth, windowHeight, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE);	// Initialize Color Texture

	// ---------------------------------------------------------------------------
	// Initial Setup of Camera
	// ---------------------------------------------------------------------------
	// Initialize projection matrix components
	SetCameraProperties(90.0f, 0.01f, 100.0f);			// field of view, near, and far planes

	// Initialize (eye/camera) model, view and projection matrices with identity matrix
	projectionMatrix = IDENTITY_MATRIX;					// Affects how objects are projected on screen
	renderFBOProjectionMatrix = IDENTITY_MATRIX;		// Projection matrix when rendering from reflective object point of view
	viewMatrix = IDENTITY_MATRIX;						// Affects camera position
	skyViewMatrix = IDENTITY_MATRIX;					// Excludes translation from view matrix (so the skybox does not move with camera)
	mTransformWater = IDENTITY_MATRIX;					// Water model matrix
	mTransformMonkey = IDENTITY_MATRIX;					// Monkey model matrix
	mTransformEarth = IDENTITY_MATRIX;					// Earth model matrix
	mTransformTorus = IDENTITY_MATRIX;					// Torus model matrix
	mTransformMirrorSphere = IDENTITY_MATRIX;

	// ---------------------------------------------------------------------------
	// Initialize Shaders
	// ---------------------------------------------------------------------------
	// Simple shaders
	vertPassThrough.LoadShader(SHADER_FILE, "../assets/shaders/PassThrough.vert", GL_VERTEX_SHADER);
	fragSimpleColor.LoadShader(SHADER_FILE, "../assets/shaders/Colorify.frag", GL_FRAGMENT_SHADER);

	// Attach and link shaders to shader program
	sProgPassthrough.AttachShader(vertPassThrough);
	sProgPassthrough.AttachShader(fragSimpleColor);
	sProgPassthrough.LinkProgram();

	// Reflection/Refraction shaders
	vertReflectRefract.LoadShader(SHADER_FILE, "../assets/shaders/ReflectionRefraction.vert", GL_VERTEX_SHADER);
	fragReflectRefract.LoadShader(SHADER_FILE, "../assets/shaders/ReflectionRefraction.frag", GL_FRAGMENT_SHADER);

	// Attach and link reflection and refraction shaders to shader program
	sProgReflectRefract.AttachShader(vertReflectRefract);
	sProgReflectRefract.AttachShader(fragReflectRefract);
	sProgReflectRefract.LinkProgram();

	// ---------------------------------------------------------------------------
	// Initialize SkyBox
	// ---------------------------------------------------------------------------
	skyBox.Initialize();

	// ---------------------------------------------------------------------------
	// Initialize Water
	// ---------------------------------------------------------------------------
	water.Initialize();

	// ---------------------------------------------------------------------------
	// Initialize Particle Systems
	// ---------------------------------------------------------------------------
	particleSparkles.Init(
		Vector3(-15.0f, -1.0, -15.0f),	// Minimum position where particles are generated
		Vector3(15.0f, 1.0f, -2.0f),	// Maximum position where particles are generated 
		Vector3(-5.0f, -5.0f, -5.0f),	// Miminum velocity of particles in all directions
		Vector3(5.0f, 5.0f, 5.0f),		// Maximum velocity of particles in all directions
		Vector2(2.0f, 10.0f),			// Life time range from x to y
		Vector2(0.1f, 0.4f),			// Alpha blending range
		Vector2(0.1f, 1.0f),			// Size range
		1500,							// Maximum number of particles spawned at a time
		10,								// Rate of particles spawned
		PARTICLE_DEFAULT,				// Type of particles used
		""								// Custom texture file path if particles used are PARTICLE_CUSTOM
		);

	particleSnow.Init(
		Vector3(-15.0f, -1.0, -15.0f), Vector3(15.0f, 1.0f, -2.0f), Vector3(-5.0f, -5.0f, -5.0f), Vector3(5.0f, 5.0f, 5.0f), 
		Vector2(4.0f, 15.0f), Vector2(0.1f, 0.4f), Vector2(0.1f, 1.0f), 1000, 20, PARTICLE_SNOW, "");

	particleCustom.Init(
		Vector3(-15.0f, -1.0, -15.0f), Vector3(15.0f, 1.0f, -2.0f),	Vector3(-5.0f, -5.0f, -5.0f), Vector3(5.0f, 5.0f, 5.0f),
		Vector2(4.0f, 15.0f), Vector2(0.1f, 0.4f), Vector2(0.1f, 1.0f), 400, 10, PARTICLE_CUSTOM, "../assets/textures/particle_grass.png");

	// ---------------------------------------------------------------------------
	// Initialize Models and Textures
	// ---------------------------------------------------------------------------
	// Initialize full screen quad
	InitFullScreenQuad();

	// Monkey Head
	if (!objLoader.LoadFromFile("../assets/models/Monkey.obj")) std::cout << "Monkey head model has failed to load.\n";
	else
	{
		vboMonkey.AddAttributesData(objLoader.GetInterleavedAttributeData());
		vboMonkey.CreateVBOs(true);
	}

	// Torus
	if (!objLoader.LoadFromFile("../assets/models/Torus.obj")) std::cout << "Torus model has failed to load.\n";
	else
	{	
		vboTorus.AddAttributesData(objLoader.GetInterleavedAttributeData());
		vboTorus.CreateVBOs(true);
	}

	// Earth Sphere
	if (!objLoader.LoadFromFile("../assets/models/Sphere.obj")) std::cout << "Sphere model (Earth) has failed to load.\n";
	else
	{
		vboEarth.AddAttributesData(objLoader.GetInterleavedAttributeData());
		vboEarth.CreateVBOs(true);
	}

	// Mirror Sphere
	if (!objLoader.LoadFromFile("../assets/models/Sphere2.obj")) std::cout << "Sphere model (Mirror Sphere) has failed to load.\n";
	else
	{
		vboMirrorSphere.AddAttributesData(objLoader.GetInterleavedAttributeData());
		vboMirrorSphere.CreateVBOs(true);
	}

	// ---------------------------------------------------------------------------
	// Initialize Textures
	// ---------------------------------------------------------------------------
	if (!texEarth.Load("../assets/textures/Earth.jpg")) std::cout << "Eearth texture has failed to load.";
	if (!texUVMap.Load("../assets/textures/uvmap.png")) std::cout << "UVmap texture has failed to load.\n";
	if (!texFur.Load("../assets/textures/fur.png")) std::cout << "Fur texture has failed to load.\n";
	if (!texBlue.Load("../assets/textures/plain-blue.jpg")) std::cout << "Blue texture has failed to load.\n";
	if (!texMess.Load("../assets/textures/color-mess.jpg")) std::cout << "Color mess texture has failed to load.\n";
	
	// ** Cube texture for reflection/refraction (water)
	glActiveTexture(GL_TEXTURE0);		// Activate texture
	glGenTextures(1, &texRRCube);		// Generate Reflection/Refraction texture

	 //Initially load skybox textures to a cube-map texture sides
	InitCubeSide(texRRCube, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "../assets/textures/SkyBox/negz.jpg");
	InitCubeSide(texRRCube, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "../assets/textures/SkyBox/posz.jpg");
	InitCubeSide(texRRCube, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "../assets/textures/SkyBox/posy.jpg");
	InitCubeSide(texRRCube, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "../assets/textures/SkyBox/negy.jpg");
	InitCubeSide(texRRCube, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "../assets/textures/SkyBox/negx.jpg");
	InitCubeSide(texRRCube, GL_TEXTURE_CUBE_MAP_POSITIVE_X, "../assets/textures/SkyBox/posx.jpg");

	// Format cube map texture
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// ** Cube texture for reflection/refraction (mirror sphere)
	glActiveTexture(GL_TEXTURE1);		// Activate texture
	glGenTextures(1, &texRRCube2);		// Generate Reflection/Refraction texture

	//Initially load skybox textures to a cube-map texture sides
	InitCubeSide(texRRCube2, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "../assets/textures/SkyBox/negz.jpg");
	InitCubeSide(texRRCube2, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "../assets/textures/SkyBox/posz.jpg");
	InitCubeSide(texRRCube2, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "../assets/textures/SkyBox/posy.jpg");
	InitCubeSide(texRRCube2, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "../assets/textures/SkyBox/negy.jpg");
	InitCubeSide(texRRCube2, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "../assets/textures/SkyBox/negx.jpg");
	InitCubeSide(texRRCube2, GL_TEXTURE_CUBE_MAP_POSITIVE_X, "../assets/textures/SkyBox/posx.jpg");

	// Format cube map texture
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Show Keyboard Shortcuts
	DisplayUserControls();

}

// Update Method
void Game::Update(float timeElapsed)
{
	// Get seconds from timeElapsed in miliseconds
	secondsElapsed = timeElapsed / 1000;

	// To rotate objects in world, calculate angle based on time elapsed
	degreesRotated += secondsElapsed * degreesPerSecond;
	while (degreesRotated > 360.0f) degreesRotated -= 360.0f;
	radiansAngle = degreesRotated * DEGREES_TO_RADIANS;

	// ======================= Update model matrices (transforms) for all objects in the scene =======================
	// ** SkyBox
	// Set skyViewMatrix to viewMatrix, but reset position, so the SkyBox is not moving with the viewer (i.e. camera)
	skyViewMatrix = viewMatrix;											// Copy viewmatrix
	skyViewMatrix.elements[12] = 0.0f;									// reset X position
	skyViewMatrix.elements[13] = 0.0f;									// reset Y position
	skyViewMatrix.elements[14] = 0.0f;									// reset Z position
	skyBox.SetMatrices(projectionMatrix, skyViewMatrix, IDENTITY_MATRIX);// Update skybox matrices

	// ** Water updates are done in Draw

	// ** Monkey
	mTransformMonkey = IDENTITY_MATRIX;
	RotateAboutY(&mTransformMonkey, radiansAngle);						// Rotate object in Y direction
	TranslateMatrix(&mTransformMonkey, 0.0f, -2.0f, -2.0f);				// Translate object

	// ** Torus
	mTransformTorus = IDENTITY_MATRIX;
	RotateAboutX(&mTransformTorus, radiansAngle);						// Rotate object in X direction
	RotateAboutY(&mTransformTorus, radiansAngle);						// Rotate object in Y direction
	TranslateMatrix(&mTransformTorus, 3.0f, 2.0f, -4.0f);				// Translate object

	// ** Earth (sphere)
	mTransformEarth = IDENTITY_MATRIX;
	RotateAboutY(&mTransformEarth, radiansAngle * 1);					// Rotate object around Y axis
	TranslateMatrix(&mTransformEarth, -3.0f, -2.0f, -4.0f);				// Translate object

	// ** Mirror sphere
	mTransformMirrorSphere = IDENTITY_MATRIX;
	TranslateMatrix(&mTransformMirrorSphere, -4.0f, -4.0f, -4.0f);
}

// Draw Method (function DrawToFBO is for FBO rendering)
void Game::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);						// Call clear of color and depth buffer bits

	// =================== Draw SKYBOX ===========================
	skyBox.Draw();

	// ============ Water model tranforms ============
	mTransformWater = IDENTITY_MATRIX;
	RotateAboutX(&mTransformWater, -PI / 2);									// Rotate plane
	TranslateMatrix(&mTransformWater, -5.0f, -5.0f, -10.0f);					// Translate plane

	// Check if dynamic reflections are used, and if yes then render to FBO from water point of view
	if (hasDynamicReflectionsPlane)
	{
		mViewer = MultiplyMatrices(&viewMatrix, &mTransformWater);			// Get position of water in world
		DrawToFBO(&mViewer, Vector2(fboWidth, fboHeight), texRRCube);		// Render scene to FBOs
	}
	
	// =================== Draw WATER ============================
	// Bind cube texture (for reflection / refraction) - done after the skybox, as skybox uses its own texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texRRCube);

	// ** Set matrices and draw water
	water.SetMatrices(projectionMatrix, viewMatrix, mTransformWater);		// Set matrices for water
	water.Draw();

	// Release texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	// ================= Draw MIRROR SPHERE ======================
	// Render scene to cube map from mirror sphere point of view
	if (hasDynamicReflectionsSphere)
	{
		mViewer = MultiplyMatrices(&viewMatrix, &mTransformMirrorSphere);	// Get mirror sphere position in world
		DrawToFBO(&mViewer, Vector2(fboWidth, fboHeight), texRRCube2);		// Draw to FBO from mirror sphere POV
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texRRCube);

	sProgReflectRefract.Bind();
	sProgReflectRefract.SendUniformMatrix("u_Projection", projectionMatrix);	// Send projection matrix
	sProgReflectRefract.SendUniformMatrix("u_View", viewMatrix);				// Send view matrix
	sProgReflectRefract.SendUniformMatrix("u_Model", mTransformMirrorSphere);	// Send model matrix
	sProgReflectRefract.SendUniformInt("u_EnvTexture", 0);						// Send environmental cube texture
	sProgReflectRefract.SendUniformBool("u_IsRefractive", true);				// Send info about refractivness

	// ** Set matrices and draw mirror sphere
	vboMirrorSphere.Draw();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	sProgReflectRefract.Unbind();

	// ** =================== Draw Multiple Objects with Shader Program ===========================
	// Bind shader program used for drawing
	sProgPassthrough.Bind();
	sProgPassthrough.SendUniformMatrix("u_View", viewMatrix);
	sProgPassthrough.SendUniformMatrix("u_Projection", projectionMatrix);

	// =================== Draw MONKEY ===========================
	sProgPassthrough.SendUniformMatrix("u_Model", mTransformMonkey);

	glActiveTexture(GL_TEXTURE0);
	texFur.Bind();
	sProgPassthrough.SendUniformInt("u_tex", 0);

	vboMonkey.Draw();													// Draw VBO

	texFur.Unbind();

	// =================== Draw SPHERE (with Earth's texture on it) ===========================
	sProgPassthrough.SendUniformMatrix("u_Model", mTransformEarth);

	// Activate texture handle and bind 'texture' to it
	glActiveTexture(GL_TEXTURE0);
	texEarth.Bind();													// Bind texture
	sProgPassthrough.SendUniformInt("u_tex", 0);						// Send bound texture to fragment shader

	vboEarth.Draw();													// Draw VBO

	// Release handles
	texEarth.Unbind();

	// =================== Draw TORUS ===========================
	sProgPassthrough.SendUniformMatrix("u_Model", mTransformTorus);
	
	glActiveTexture(GL_TEXTURE0);
	texUVMap.Bind();
	sProgPassthrough.SendUniformInt("u_tex", 0);

	vboTorus.Draw();													// Draw VBO

	// Release memory
	texUVMap.Unbind();

	// Unbind reflection cube texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	sProgPassthrough.Unbind();											// No need for this shader program anymore, so unbind it
	
	// ==================================================================================================================
	// ====== Particle Generation ======
	// ==================================================================================================================
	Matrix modelMatrixSparkles = IDENTITY_MATRIX;
	TranslateMatrix(&modelMatrixSparkles, 1.0f, 0.0f, -6.0f);
	particleSparkles.SetMatrices(projectionMatrix, viewMatrix, modelMatrixSparkles);
	particleSparkles.Update(secondsElapsed);
	particleSparkles.Render();

	Matrix modelMatrixSnow = IDENTITY_MATRIX;
	TranslateMatrix(&modelMatrixSnow, -1.0f, -1.0f, -4.0f);
	particleSnow.SetMatrices(projectionMatrix, viewMatrix, modelMatrixSnow);
	particleSnow.Update(secondsElapsed);
	particleSnow.Render();

	Matrix modelMatrixCustomSys = IDENTITY_MATRIX;
	TranslateMatrix(&modelMatrixCustomSys, 0.0f, -2.0f, -2.0f);
	particleCustom.SetMatrices(projectionMatrix, viewMatrix, modelMatrixCustomSys);
	particleCustom.Update(secondsElapsed);
	particleCustom.Render();
	// ====== Particle Generation ======

	glutSwapBuffers();											// Swap buffers
	glutPostRedisplay();										// Display front buffer
}

// FBO rendering for Dynamic Reflections
void Game::DrawToFBO(Matrix* viewerPosition, Vector2 resolution, GLuint texture)
{
	// Rendering directions
	RENDER_DIRECTION directions[6] = { RENDER_FRONT, RENDER_BACK, RENDER_BOTTOM, RENDER_TOP, RENDER_LEFT, RENDER_RIGHT };
	
	glDisable(GL_DEPTH_TEST);

	// Size of arrays for FBOs / transform matrices (based on 6 sides of the cube)
	unsigned orientationsSize = 6;

	// To easier render scene into 6 different FBOs from different positions
	FrameBufferObject* fbos[] = { &fboNegZ, &fboPosZ, &fboNegY, &fboPosY, &fboNegX, &fboPosX };

	// Orient viewer to look from the same position toward different directions (all matrices are originally pointing towards negative Z)
	Matrix lookAtNegZ = IDENTITY_MATRIX;
	Matrix lookAtPosZ = IDENTITY_MATRIX;
	Matrix lookAtNegY = IDENTITY_MATRIX;
	Matrix lookAtPosY = IDENTITY_MATRIX;
	Matrix lookAtNegX = IDENTITY_MATRIX;
	Matrix lookAtPosX = IDENTITY_MATRIX;

	// Rotate matrices to match direction they are supposed to be facing (PI = 180 degrees)
	RotateAboutY(&lookAtNegZ, PI);
	RotateAboutY(&lookAtPosZ, 0);
	RotateAboutX(&lookAtNegY, -PI / 2);
	RotateAboutX(&lookAtPosY, PI / 2);
	RotateAboutY(&lookAtNegX, PI / 2);
	RotateAboutY(&lookAtPosX, -PI / 2);

	// To easier manipulate with matrices when rendering to FBOs - using array
	Matrix* orientations[] = { &lookAtNegZ, &lookAtPosZ, &lookAtNegY, &lookAtPosY, &lookAtNegX, &lookAtPosX };

	// Different orientation for objects
	Matrix objOrientationNegZ = IDENTITY_MATRIX;
	Matrix objOrientationPosZ = IDENTITY_MATRIX;
	Matrix objOrientationNegY = IDENTITY_MATRIX;
	Matrix objOrientationPosY = IDENTITY_MATRIX;
	Matrix objOrientationNegX = IDENTITY_MATRIX;
	Matrix objOrientationPosX = IDENTITY_MATRIX;
	
	RotateAboutY(&objOrientationNegZ, 0);
	RotateAboutY(&objOrientationPosZ, PI);
	RotateAboutX(&objOrientationNegY, -PI / 2);
	RotateAboutX(&objOrientationPosY, PI / 2);
	RotateAboutY(&objOrientationNegX, -PI / 2);
	RotateAboutY(&objOrientationPosX, PI / 2);

	Matrix* objOrientations[] = { &objOrientationNegZ, &objOrientationPosZ, &objOrientationNegY, &objOrientationPosY, &objOrientationNegX, &objOrientationPosX };

	// Set render projection matrix
	renderFBOProjectionMatrix = CreateOrtographicProjectionMatrix(-22.5f, 22.5f, 22.5f, -22.5f, viewerPosition->elements[13], (viewerPosition->elements[13] + 100.0f) * -1);
	//Matrix renderFBOProjectionMatrix2 = CreateOrtographicProjectionMatrix(-22.5f, 22.5f, 22.5f, -22.5f, viewerPosition->elements[13], (viewerPosition->elements[13] + 100.0f) * -1);
	Matrix renderFBOProjectionMatrix2 = CreateOrtographicProjectionMatrix(-22.5f, 22.5f, 22.5f, -22.5f, -100, 100.0f);

	// Invert render projection matrix to get FBO rendered appropriatelly
	ScaleMatrix(&renderFBOProjectionMatrix, 1, -1, -1);	
	ScaleMatrix(&renderFBOProjectionMatrix2, -1, -1, 1);

	// Change position of above matrices to match position of the viewer (eye)
	for (unsigned i = 0; i < orientationsSize; i++)
	{	
		// Change elements of orientation 'lookAt' matrices directly relative to position
		objOrientations[i]->elements[12] = viewerPosition->elements[12];			// x
		objOrientations[i]->elements[13] = viewerPosition->elements[13];			// y
		objOrientations[i]->elements[14] = viewerPosition->elements[14];			// z

		// Check values in lookAt matrices
		//printf("\nOrientation matrix %d elements: ", i);
		//for (int t = 0; t < 16; t++) printf(" %f ", orientations[i]->elements[t]);
		//printf("\n\n");

		// Set viewport for FBO rendering
		glViewport(0, 0, resolution.x, resolution.y);

		// Clear and bind FBO for rendering
		fbos[i]->Clear();
		fbos[i]->Bind();

		// =================== Draw SKYBOX ===========================
		// Need to render skyBox by using renderFBOProjectionMatrix matrix
		skyBox.SetMatrices(renderFBOProjectionMatrix, *orientations[i], IDENTITY_MATRIX);
		skyBox.Draw();
		// Set skyBox matrices back to old values (using 'camera' projection)
		skyBox.SetMatrices(projectionMatrix, skyViewMatrix, IDENTITY_MATRIX);

		// =================== Bind Shader Program used to draw objects in the scene ===========================
		sProgPassthrough.Bind();	
		sProgPassthrough.SendUniformMatrix("u_View", *objOrientations[i]);
		sProgPassthrough.SendUniformMatrix("u_Projection", renderFBOProjectionMatrix2);

		// =================== Draw MONKEY ===========================
		// Check if object is visible from viewer's (reflective object) point of view
		if (IsInLineOfSight(objOrientations[i], &mTransformMonkey, Vector2(45.0f, 45.0f), directions[i]));
		{
			sProgPassthrough.SendUniformMatrix("u_Model", mTransformMonkey);
			glActiveTexture(GL_TEXTURE0);
			texFur.Bind();
			sProgPassthrough.SendUniformInt("u_tex", 0);

			vboMonkey.Draw();											// Draw VBO

			texFur.Unbind();
		}
		// =================== Draw SPHERE (with Earth's texture on it) ===========================
		// Check if object is visible from viewer's (reflective object) point of view
		if (IsInLineOfSight(objOrientations[i] , &mTransformEarth, Vector2(45.0f, 45.0f), directions[i]));
		{
			sProgPassthrough.SendUniformMatrix("u_Model", mTransformEarth);
			glActiveTexture(GL_TEXTURE0);
			texEarth.Bind();											// Bind texture
			sProgPassthrough.SendUniformInt("u_tex", 0);				// Send bound texture to fragment shader

			vboEarth.Draw();											// Draw VBO

			// Release handles
			texEarth.Unbind();
		}
		// =================== Draw TORUS ===========================
		// Check if object is visible from viewer's (reflective object) point of view
		if (IsInLineOfSight(objOrientations[i], &mTransformTorus, Vector2(45.0f, 45.0f), directions[i]));
		{
			sProgPassthrough.SendUniformMatrix("u_Model", mTransformTorus);

			glActiveTexture(GL_TEXTURE0);
			texUVMap.Bind();
			sProgPassthrough.SendUniformInt("u_tex", 0);

			vboTorus.Draw();											// Draw VBO

			// Release memory
			texUVMap.Unbind();
		}

		// Unbind shader program
		sProgPassthrough.Unbind();

		// Unbind FBO
		fbos[i]->UnBind();
	}

	// Update cube sides
	UpdateCubeSide(texture, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, &fboNegZ);
	UpdateCubeSide(texture, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, &fboPosZ);
	UpdateCubeSide(texture, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, &fboPosY);
	UpdateCubeSide(texture, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, &fboNegY);
	UpdateCubeSide(texture, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, &fboNegX);
	UpdateCubeSide(texture, GL_TEXTURE_CUBE_MAP_POSITIVE_X, &fboPosX);

	// Reset viewport
	glViewport(0, 0, windowWidth, windowHeight);

	glEnable(GL_DEPTH_TEST);
}

void Game::KeyboardDown(unsigned char key, int mouseX, int mouseY)
{
	//move position of camera based on WASD keys, and XZ keys for rotation
	const float moveSpeed = 2.0; //units per second
	
	switch (key)
	{
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	case 'a':
		TranslateMatrix(&viewMatrix, moveSpeed * secondsElapsed, 0.0f, 0.0f);			// To move left, translate view matrix in -X direction (objects in the world move to the right)
		break;
	case 'd':
		TranslateMatrix(&viewMatrix, -moveSpeed * secondsElapsed, 0.0f, 0.0f);			// To move right, translate view matrix in +X direction (objects in the world move to the left)
		break;
	case 'w':
		TranslateMatrix(&viewMatrix, 0.0f, 0.0f, moveSpeed * secondsElapsed);			// To move forward, translate view matrix in -Z direction (objects in the world move back)
		break;
	case 's':
		TranslateMatrix(&viewMatrix, 0.0f, 0.0f, -moveSpeed * secondsElapsed);			// To move backwards, translate view matrix in +Z direction (objects in the world move forward)
		break;
	case 'r':
		TranslateMatrix(&viewMatrix, 0.0f, moveSpeed * secondsElapsed, 0.0f);			// Move up (objects in the world move down)
		break;
	case 'f':
		TranslateMatrix(&viewMatrix, 0.0f, -moveSpeed * secondsElapsed, 0.0f);			// Move down (objects in the world move up)
		break;
	case 'z':
		RotateAboutY(&viewMatrix, moveSpeed * secondsElapsed * DEGREES_TO_RADIANS);		// To rotate camera to left, rotate view matrix around Y axis in CCW direction
		break;
	case 'x':
		RotateAboutY(&viewMatrix, -moveSpeed * secondsElapsed * DEGREES_TO_RADIANS);	// To rotate camera to right, rotate view matrix around Y axis in CW direction
		break;
	}
}

void Game::KeyboardUp(unsigned char key, int mouseX, int mouseY)
{
	switch (key)
	{
	case 32: // the space bar
		break;
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::KeyboardSpecial(int key, int x, int y)
{
	//move position of camera based on arrow keys, and Page Up and Page Down keys for rotation
	const float moveSpeed = 2.0; //units per second
	switch (key)
	{
	case GLUT_KEY_LEFT:
		TranslateMatrix(&viewMatrix, moveSpeed * secondsElapsed, 0.0f, 0.0f);				// To move left, translate view matrix in -X direction
		break;
	case GLUT_KEY_RIGHT:
		TranslateMatrix(&viewMatrix, -1 * moveSpeed * secondsElapsed, 0.0f, 0.0f);			// To move right, translate view matrix in +X direction
		break;
	case GLUT_KEY_UP:
		TranslateMatrix(&viewMatrix, 0.0f, 0.0f, moveSpeed * secondsElapsed);				// To move forward, translate view matrix in -Z direction
		break;
	case GLUT_KEY_DOWN:
		TranslateMatrix(&viewMatrix, 0.0f, 0.0f, -1 * moveSpeed * secondsElapsed);			// To move backwards, translate view matrix in -Z direction
		break;
	case GLUT_KEY_PAGE_UP:
		RotateAboutY(&viewMatrix, -1 * moveSpeed * secondsElapsed * DEGREES_TO_RADIANS);	// To rotate camera to left, rotate view matrix around Y axis in CCW direction
		break;
	case GLUT_KEY_PAGE_DOWN:
		RotateAboutY(&viewMatrix, moveSpeed * secondsElapsed * DEGREES_TO_RADIANS);			// To rotate camera to right, rotate view matrix around Y axis in CCW direction
		break;
	}
}

void Game::MouseClicked(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			mouseX = x;
			mouseY = y;
		}
		else
		{
			mouseX = -1;
			mouseY = -1;
		}
	}
	else if (button == 3)
	{
		MouseScrolled(0, 1, x, y);
	}
	else
	{
		MouseScrolled(0, -1, x, y);
	}
}

void Game::MouseMoved(int x, int y)
{
	if (mouseX != -1 && mouseY != -1) {
		
		const float moveSpeed = 0.05;
		int dx = x - mouseX;
		int dy = y - mouseY;
		
		RotateAboutX(&viewMatrix, -dy * secondsElapsed * moveSpeed);
		RotateAboutY(&viewMatrix, -dx * secondsElapsed * moveSpeed);
		
		mouseX = x;
		mouseY = y;
	}

	if (GLUT_DOWN == mouseButtonPressed)
	{
		mouseX = oldMouseX - x;
		mouseY = oldMouseY - y;
	}
}

void Game::MouseScrolled(int button, int direction, int x, int y)
{
	const float moveSpeed = 2.0f;

	if (GLUT_MIDDLE_BUTTON == mouseButtonPressed)
	{
		if (direction > 0) 
			TranslateMatrix(&viewMatrix, 0.0f, 0.0f, moveSpeed * secondsElapsed);
		else
			TranslateMatrix(&viewMatrix, 0.0f, 0.0f, -1 * moveSpeed * secondsElapsed);
	}
}

void Game::Close()
{
	// Clear memory occupied by shader programs, shaders, and textures

	// Release memory occupied by shaders
	vertPassThrough.Destroy();	fragSimpleColor.Destroy();
	
	// Release memory occupied by shader programs
	sProgPassthrough.Unbind();	sProgPassthrough.Destroy();

	// Release memory occupied by textures
	texBlue.Unbind();			texBlue.Destroy();
	texMess.Unbind();			texMess.Destroy();
	texFur.Unbind();			texFur.Destroy();
	texUVMap.Unbind();			texUVMap.Destroy();
	
	// Release memory occupied by environmental cube texture
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	// Release memory occupied by SkyBox
	skyBox.Destroy();

	// Release memory occupied by Water
	water.Destroy();

	// Release memory occupied by the other VBOs (no need for this as VertexBufferObject destructor calls Destroy function)
	//vboMonkey.Destroy();
	//vboEarth.Destroy();
	//vboMirrorSphere.Destroy();
	//vboTorus.Destroy();

	// Release memory occupied by FBOs (FrameBufferObject destructor calls Destroy function)
	//fboNegZ.Destroy();
	//fboPosZ.Destroy();
	//fboNegY.Destroy();
	//fboPosY.Destroy();
	//fboNegX.Destroy();
	//fboPosX.Destroy();

	// Relase memory occupied by Particle Engine instances
	particleSparkles.Destroy();
	particleSnow.Destroy();
	particleCustom.Destroy();
}

void Game::ResizeWindow()
{
	// Create projection matrix to be used in Vertex shader for transformations - matrix changes to adapt when window is resized
	projectionMatrix = CreatePerspectiveProjection(fieldOfView, windowWidth / windowHeight, nearPlane, farPlane);
	// projectionMatrix = CreateOrtographicProjectionMatrix(-25.0f, 25.0f, 25.0f, -25.0f, -60.0f, 60.0f);
}

// Used to initialize cube texture
bool Game::InitCubeSide(GLuint texture, GLenum targetSide, const char* textureFile)
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	unsigned char* image_data = SOIL_load_image(textureFile, &fboWidth, &fboHeight, 0, SOIL_LOAD_RGBA);
	if (!image_data) {
		fprintf(stderr, "ERROR: could not load %s\n", textureFile);
		return false;
	}
	// check if image dimensions are non-power-of-2
	if ((fboWidth & (fboWidth - 1)) != 0 || (fboHeight & (fboHeight - 1)) != 0) {
		fprintf(stderr, "WARNING: image %s is not power-of-2 dimensions\n", textureFile);
	}

	// copy pixel image data into side of cube map - initialize texture
	glTexImage2D(targetSide, 0, GL_RGBA, fboWidth, fboHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
	free(image_data);
	return true;
}

// this function overload only renders FBO to texture used for dynamic relfection / refraction
bool Game::UpdateCubeSide(GLuint texture, GLenum targetSide, FrameBufferObject* fbo)
{
	// Bind cube texture and FBO used to render scene
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	
	// Check if FBO is complete, and if it is then render FBO to texture side
	fbo->Bind();
	if (fbo->Check()) glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, targetSide, texture, 0);
	else fprintf(stderr, "ERROR: FBO is not complete!");
	fbo->UnBind();

	return true;
}

bool Game::IsInLineOfSight(Matrix* originTransform, Matrix* targetTransform, Vector2 viewRectangle, RENDER_DIRECTION direction)
{
	Vector3 worldPosOrigin = Vector3(originTransform->elements[12], originTransform->elements[13], originTransform->elements[14]);
	Vector3 worldPosTarget = Vector3(targetTransform->elements[12], targetTransform->elements[13], targetTransform->elements[14]);

	bool isVisible = false;

	switch (direction)
	{
	case RENDER_FRONT:
		// to be visible from the front of the object Z must be higher negative value and X and Y must be within view rectangle
		if (worldPosTarget.z < worldPosOrigin.z)
		{
			if (std::abs(worldPosTarget.x) < abs(viewRectangle.x / 2) && std::abs(worldPosTarget.y) < std::abs((viewRectangle.y / 2)))
				isVisible = true;
		}
		break;
	case RENDER_BACK:
		// to be visible from the back of the object Z must be higher positive value and X and Y must be within view rectangle
		if (worldPosTarget.z > worldPosOrigin.z)
		{
			if (std::abs(worldPosTarget.x) < abs(viewRectangle.x / 2) && std::abs(worldPosTarget.y) < std::abs((viewRectangle.y / 2))) 
				isVisible = true;
		}
		break;
	case RENDER_BOTTOM:
		// to be visible from the bottom of the object Y must be higher negative value and X and Z must be within view rectangle
		if (worldPosTarget.y < worldPosOrigin.y)
		{
			if (std::abs(worldPosTarget.x) < abs(viewRectangle.x / 2) && std::abs(worldPosTarget.z) < std::abs((viewRectangle.y / 2))) 
				isVisible = true;
		}
		break;
	case RENDER_TOP:
		// to be visible from the top of the object Y must be higher positive value and X and Z must be within view rectangle
		if (worldPosTarget.y > worldPosOrigin.y)
		{
			if (std::abs(worldPosTarget.x) < abs(viewRectangle.x / 2) && std::abs(worldPosTarget.z) < std::abs((viewRectangle.y / 2))) 
				isVisible = true;
		}
		break;
	case RENDER_LEFT:
		// to be visible from the left of the object X must be higher negative value and Y and Z must be within view rectangle
		if (worldPosTarget.x < worldPosOrigin.x)
		{
			if (std::abs(worldPosTarget.z) < abs(viewRectangle.x / 2) && std::abs(worldPosTarget.y) < std::abs((viewRectangle.y / 2))) 
				isVisible = true;
		}
		break;
	case RENDER_RIGHT:
		// to be visible from the right of the object X must be higher positive value and Y and Z must be within view rectangle
		if (worldPosTarget.x > worldPosOrigin.x)
		{
			if (std::abs(worldPosTarget.z) < abs(viewRectangle.x / 2) && std::abs(worldPosTarget.y) < std::abs((viewRectangle.y / 2))) 
				isVisible = true;
		}
		break;
	}

	return isVisible;
}