/*
	 This is the main file for GDW Project.
	 Samir Suljkanovic 2016
**/

#include "Engine.h"

Game* gameObj;

int
windowWidth = 1920,				// Width of created window
windowHeight = 1024,			// Height of created window
windowHandle = 0;				// Handle to created window

unsigned frameCount = 0;		// Frames per second counter

unsigned
timeCurrent = 0,				// Time taken 'now' by update method
timePrevious = 0,				// Last memorized time taken
timeElapsed = 0;				// Difference between current and last time recorded

// -----------------------------------------------------------------------------------------
// *** *** *** Function prototypes *** *** ***
// -----------------------------------------------------------------------------------------
void Initialize(int, char*[]);				// Initialize function prototype
void WindowResize(int, int);				// Window Resize function prototype
void Render(void);							// Render function prototype
void Timer(int);							// Timer function prototype
void Idle(void);							// Idle function prototype
void WindowClose(void);						// WindowClose function prototype
void KeyboardDown(unsigned char, int, int);	// KeyboardDown function prototype
void KeyboardUp(unsigned char, int, int);	// KeyboardUp function prototype
void KeyboardSpecial(int key, int x, int y);// KeyboardSpecial function prototype
void MouseClicked(int, int, int, int);		// MouseClicked function prototype
void MouseMoved(int x, int y);				// MouseMoved function prototype
void MouseScrolled(int button, int direction, int x, int y);	// MouseScrolled function prototype
// -----------------------------------------------------------------------------------------
// *** *** ***  End of func. prototypes  *** *** ***
// -----------------------------------------------------------------------------------------

// Main function
int main(int argc, char* argv[])
{
	Initialize(argc, argv);		// Initialize GL

	gameObj = new Game();		// Create new Game object
	gameObj->Initialize();		// Call Initialize function on Game object to load object data, shaders, textures, etc.

	glutMainLoop();				// Run main GL loop

	exit(EXIT_SUCCESS);
}

// Initialize function - GL and window related initializations
void Initialize(int argc, char* argv[])
{
	glewExperimental = GL_TRUE;
	glutInit(&argc, argv);

	// Set current and previous time to the same value of milliseconds since glut started
	timeCurrent = glutGet(GLUT_ELAPSED_TIME);
	timePrevious = timeCurrent;

	// Set context version, flags and profile
	glutInitContextVersion(4, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	// Set GL options
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Set initial window size and display modes
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

	// Create window
	windowHandle = glutCreateWindow(WINDOW_TITLE_PREFIX);

	// Check if window handle is ok, and if not display error message
	if (windowHandle < 1) {
		fprintf(stderr, "ERROR: A new window was not created.\n");
		exit(EXIT_FAILURE);
	}

	// Set various GL function callbacks
	glutReshapeFunc(WindowResize);			// Set reshape function - triggered when window is resized
	glutDisplayFunc(Render);				// Set display function - function rendering screen
	glutIdleFunc(Idle);						// Set idle function - function when GL is idle
	glutTimerFunc(0, Timer, 0);				// Set timer function - function calling back itself based on timer set value
	glutCloseFunc(WindowClose);				// Set close window function - function to cleanup memory upon window close event
	glutKeyboardFunc(KeyboardDown);			// Set keyboard function - function responding to keyboards key presses
	glutKeyboardUpFunc(KeyboardUp);			// Set keyboard up function - function responding to keyboards key released
	glutSpecialFunc(KeyboardSpecial);		// Set keyboard special function - function handling keyboard arrow keys
	glutMouseFunc(MouseClicked);			// Set mouse function - function responding to mouse clicks
	glutMotionFunc(MouseMoved);				// Set mouse motion function - after button is clicked will receive X, Y coordinates based on mouse position
	glutMouseWheelFunc(MouseScrolled);		// Set mouse scroll wheel function

	GLenum GlewInitResult = glewInit();		// Get GLEW initialization result

	// Check if GLEW result check returned is ok, and if not display error message and exit
	if (GLEW_OK != GlewInitResult) {
		fprintf(stderr, "ERROR: %s\n", glewGetErrorString(GlewInitResult));
		exit(EXIT_FAILURE);
	}

	// Print GL version to the console window
	fprintf(stdout, "INFO: OpenGL Version: %s\n", glGetString(GL_VERSION));

	// Set clear color - GL window repaint color after clear function is called
	glClearColor(0.35f, 0.15f, 0.15f, 0.0f);	// Clear color
	glClearDepth(1.0f);							// Clear depth
	glEnable(GL_DEPTH_TEST);					// Enable depth test
	glDepthFunc(GL_LESS);						// Set depth function
	//glEnable(GL_CULL_FACE);						// Enable face culling
	//glCullFace(GL_BACK);						// Set which faces to cull
	//glFrontFace(GL_CW);							// Specify how faces are drawn (CW = clock wise, CCW = counter clock wise)
}

// Window resize function - changes viewport on resize
void WindowResize(int Width, int Height)
{
	windowWidth = Width;					// Store new window width
	windowHeight = Height;					// Store new window height
	gameObj->ResizeWindow();				// Call game object resize window function
	glViewport(0, 0, windowWidth, windowHeight);	// Resize viewport properly
}

// Render function - displays content
void Render(void)
{
	gameObj->Draw();						// Call Draw function in Game object
	++frameCount;							// Increment frame count (this is just for the frame info in window title)
}

// Idle function - when idling
void Idle(void)
{
	glutPostRedisplay();
}

// Timer function - time updated frame count and title display
void Timer(int Value)
{
	// Take current time and calculate difference between previous and current time to get delta time.
	timeCurrent = glutGet(GLUT_ELAPSED_TIME);
	timeElapsed = timeCurrent - timePrevious;

	timePrevious = timeCurrent;				// Remember last 'current' time as previous time

	gameObj->Update(timeElapsed);			// Call update function in Game object and pass time elapsed between two calls

	unsigned timerFuncRecall = 250;			// Time for callback to this function

	if (0 != Value) {

		// Allocate memory space for window title
		char* TempString = (char*)malloc(512 + strlen(WINDOW_TITLE_PREFIX));
		
		// Add frame per second information to title (as the function calls itself every 250 ms i.e. 4 times per sec (1 sec = 1000 ms) need to multiply frameCount by 1000 / timerFuncRecall)
		sprintf(TempString, "%s: %d Frames Per Second @ %d x %d", WINDOW_TITLE_PREFIX, frameCount * 1000 / timerFuncRecall, windowWidth, windowHeight);
		
		glutSetWindowTitle(TempString);		// Display window title
		free(TempString);					// Free up the allocated memory
	}

	frameCount = 0;							// Reset frame count

	glutTimerFunc(timerFuncRecall, Timer, 1);	// Make callback to this function
}

void KeyboardDown(unsigned char key, int x, int y)
{
	gameObj->KeyboardDown(key, x, y);			// Call KeyboardDown function in Game object
}

void KeyboardUp(unsigned char key, int x, int y)
{
	gameObj->KeyboardUp(key, x, y);				// Call KeyboardUp function in Game object
}

void KeyboardSpecial(int key, int x, int y)
{
	gameObj->KeyboardSpecial(key, x, y);
}

void MouseClicked(int button, int state, int x, int y)
{
	gameObj->MouseClicked(button, state, x, y);	// Call MouseClicked function in Game object
}

void MouseMoved(int x, int y)
{
	gameObj->MouseMoved(x, y);					// Call MouseMoved function in Game object
}

void MouseScrolled(int button, int direction, int x, int y)
{
	gameObj->MouseScrolled(button, direction, x, y);	// Call MouseScrolled function in Game object
}

void WindowClose()
{
	gameObj->Close();
}