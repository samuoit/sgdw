/*
	This is the header for SkyBox class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_SKY_BOX_H
#define SGDW_SKY_BOX_H

class SkyBox
{
public:

	SkyBox();						// Default constructor
	~SkyBox();						// Destructor

	bool Initialize();				// Initialize SkyBox
	void Destroy();					// Cleanup memory occupied by SkyBox
	void Draw();					// Draw SkyBox
	void SetMatrices				// Set projection, view and model matrix for SkyBox
		(
		Matrix projection,				// Projection matrix for clipping
		Matrix view,					// View matrix for world positioning
		Matrix model					// Model matrix for origin positioning
		);

private:

	// Loads individual images to cube sides
	bool InitCubeSide(GLuint texture, GLenum targetSide, const char* textureFile);

	bool isInitialized;				// State of the object - true if initialize, or false if not

	ShaderProgram sProgSkyBox;		// Shader program to render SkyBox
	Shader vertSkyBox, fragSkyBox;	// Shaders used by shader program

	Matrix mProj, mView, mModel;	// Projection, view and model matrices

	GLuint vao;						// Handle to VAO
	GLuint vbo;						// Handle to VBO
	GLuint texCube;					// Pointer to cube texture handle

	int faceSize;					// Size of face (triangle = 3)
	int pointsSize;					// Size of attributes in points array collection (float elements)
	int pointsFaces;				// Number of triangular faces specified by points array data
};

#endif