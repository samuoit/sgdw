/*
	This is the header for Water class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_WATER_H
#define SGDW_WATER_H

class Water
{
public:

	Water();						// Constructor
	~Water();						// Destructor

	bool Initialize();				// Initialize water object
	void Update(float secsElapsed);	// Update water object
	void Draw();					// Render (draw) water object
	void Destroy();					// Destroy water object (clean-up memory)
	float GetWaterHeight();			// Return height of water plane

	void SetMatrices(				// Set matrices for use by shaders
		Matrix projection,			// projection matrix
		Matrix view,				// view matrix
		Matrix model				// model matrix
		);

	float RandomFloat(float min, float max, unsigned seed = 0);
	void Water::SetRandomSeed(unsigned seed = 0);

private:

	bool isInitialized;				// state of the object

	Matrix normalMatrix;			// normal matrix
	Vector3 eyePosition;			// viewer (eye/camera) position
	Vector3 direction[8];			// wave directions
	float amplitude[8];				// wave heights
	float waveLength[8];			// wave lengths
	float speed[8];					// wave speeds
	int numberOfWaves;				// # of waves per cycle
	float waterHeight;				// height (Y coordinate) of the water
	float time;						// time for wave cycle
	float randomAngle;				// random angle between two values

	Texture texWater;				// Water texture
	GLuint envTexture;				// Environmental (cube) texture used for reflection/refraction

	ObjLoader loader;				// Object loader to load water object (plane)
	VertexBufferObject vboWater;	// Water (plane) object

	ShaderProgram sProgWaterRR;		// Shader program handling water shaders reflection/refraction
	ShaderProgram sProgWaterMV;		// Shader program handling water surface movement
	Shader vertWaterRR, fragWaterRR;// Vertex and Fragment shaders for water object reflection/refraction
	Shader vertWaterMV, fragWaterMV;// Vertex and Fragment shaders for water object movement

	Matrix waterProjectionMatrix;	// Projection matrix for water object
	Matrix waterViewMatrix;			// View matrix for water object
	Matrix waterModelMatrix;		// Model (transform) matrix for Water object

};

#endif
