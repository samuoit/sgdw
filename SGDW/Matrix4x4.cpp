///*
//	This is the implementation for Matrix4x4 class.
//	Samir Suljkanovic 2015
//*/
//
//#include "Engine.h"
//
//Matrix4x4::Matrix4x4()
//{
//	m11 = 0; m12 = 0; m13 = 0; m14 = 0;				// First column
//	m21 = 0; m22 = 0; m23 = 0; m24 = 0;				// Second column
//	m31 = 0; m32 = 0; m33 = 0; m34 = 0;				// Third column
//	m41 = 0; m42 = 0; m43 = 0; m44 = 0;				// Fourth column
//}
//
//Matrix4x4::Matrix4x4(bool isIdentity)
//{
//	if (!isIdentity)								// Creates 'zero' matrix
//		Matrix4x4();
//	else											// Creates 'identity' matrix
//		Matrix4x4(
//		1.0f, 0.0f, 0.0f, 0.0f,
//		0.0f, 1.0f, 0.0f, 0.0f, 
//		0.0f, 0.0f, 1.0f, 0.0f,
//		0.0f, 0.0f, 0.0f, 1.0f);
//}
//
//Matrix4x4::Matrix4x4(
//	float M11, float M12, float M13, float M14,		// First column
//	float M21, float M22, float M23, float M24,		// Second column
//	float M31, float M32, float M33, float M34,		// Third column
//	float M41, float M42, float M43, float M44)		// Fourth column
//{
//	m11 = M11; m12 = M12; m13 = M13; m14 = M14;
//	m21 = M21; m22 = M22; m23 = M23; m24 = M24;
//	m31 = M31; m32 = M32; m33 = M33; m34 = M34;
//	m41 = M41; m42 = M42; m43 = M43; m44 = M44;
//}
//
//Matrix4x4::Matrix4x4(const Matrix4x4 &mat)
//{
//	m11 = mat.m11; m12 = mat.m12; m13 = mat.m13; m14 = mat.m14;
//	m21 = mat.m21; m22 = mat.m22; m23 = mat.m23; m24 = mat.m24;
//	m31 = mat.m31; m32 = mat.m32; m33 = mat.m33; m34 = mat.m34;
//	m41 = mat.m41; m42 = mat.m42; m43 = mat.m43; m44 = mat.m44;
//}
//
//Matrix4x4::Matrix4x4(glm::mat4 &mat)
//{
//	m11 = mat[0][0]; m12 = mat[1][0]; m13 = mat[2][0]; m14 = mat[3][0];
//	m21 = mat[0][1]; m22 = mat[1][1]; m23 = mat[2][1]; m24 = mat[3][1];
//	m31 = mat[0][2]; m32 = mat[1][2]; m33 = mat[2][2]; m34 = mat[3][2];
//	m41 = mat[0][3]; m42 = mat[1][3]; m43 = mat[2][3]; m44 = mat[3][3];
//}
//
//Matrix4x4::~Matrix4x4() { /* DESTRUCTOR */ }
//
//bool Matrix4x4::operator==(const Matrix4x4 &mat)
//{
//	if (m11 == mat.m11 && m12 == mat.m12 && m13 == mat.m13 && m14 == mat.m14 &&
//		m21 == mat.m21 && m22 == mat.m22 && m23 == mat.m23 && m24 == mat.m24 &&
//		m31 == mat.m31 && m32 == mat.m32 && m33 == mat.m33 && m34 == mat.m34 &&
//		m41 == mat.m41 && m42 == mat.m42 && m43 == mat.m43 && m44 == mat.m44)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}
//
//void Matrix4x4::operator=(const Matrix4x4 &mat)
//{
//	m11 = mat.m11; m12 = mat.m12; m13 = mat.m13; m14 = mat.m14;
//	m21 = mat.m21; m22 = mat.m22; m23 = mat.m23; m24 = mat.m24;
//	m31 = mat.m31; m32 = mat.m32; m33 = mat.m33; m34 = mat.m34;
//	m41 = mat.m41; m42 = mat.m42; m43 = mat.m43; m44 = mat.m44;
//}
//
//void Matrix4x4::operator+=(const Matrix4x4 &mat)
//{
//	m11 += mat.m11; m12 += mat.m12; m13 += mat.m13; m14 += mat.m14;
//	m21 += mat.m21; m22 += mat.m22; m23 += mat.m23; m24 += mat.m24;
//	m31 += mat.m31; m32 += mat.m32; m33 += mat.m33; m34 += mat.m34;
//	m41 += mat.m41; m42 += mat.m42; m43 += mat.m43; m44 += mat.m44;
//}
//
//void Matrix4x4::operator-=(const Matrix4x4 &mat)
//{
//	m11 -= mat.m11; m12 -= mat.m12; m13 -= mat.m13; m14 -= mat.m14;
//	m21 -= mat.m21; m22 -= mat.m22; m23 -= mat.m23; m24 -= mat.m24;
//	m31 -= mat.m31; m32 -= mat.m32; m33 -= mat.m33; m34 -= mat.m34;
//	m41 -= mat.m41; m42 -= mat.m42; m43 -= mat.m43; m44 -= mat.m44;
//}
//
//void Matrix4x4::operator*=(const Matrix4x4 &mat)
//{
//	Matrix4x4 temp = this;
//
//	m11 = temp.m11 * mat.m11 + temp.m12 * mat.m21 + temp.m13 * mat.m31 + temp.m14 * mat.m41;
//	m12 = temp.m11 * mat.m12 + temp.m12 * mat.m22 + temp.m13 * mat.m32 + temp.m14 * mat.m42;
//	m13 = temp.m11 * mat.m13 + temp.m12 * mat.m23 + temp.m13 * mat.m33 + temp.m14 * mat.m43;
//	m14 = temp.m11 * mat.m14 + temp.m12 * mat.m24 + temp.m13 * mat.m34 + temp.m14 * mat.m44;
//	m21 = temp.m21 * mat.m11 + temp.m22 * mat.m21 + temp.m23 * mat.m31 + temp.m24 * mat.m41;
//	m22 = temp.m21 * mat.m12 + temp.m22 * mat.m22 + temp.m23 * mat.m32 + temp.m24 * mat.m42;
//	m23 = temp.m21 * mat.m13 + temp.m22 * mat.m23 + temp.m23 * mat.m33 + temp.m24 * mat.m43;
//	m24 = temp.m21 * mat.m14 + temp.m22 * mat.m24 + temp.m23 * mat.m34 + temp.m24 * mat.m44;
//	m31 = temp.m31 * mat.m11 + temp.m32 * mat.m21 + temp.m33 * mat.m31 + temp.m34 * mat.m41;
//	m32 = temp.m31 * mat.m12 + temp.m32 * mat.m22 + temp.m33 * mat.m32 + temp.m34 * mat.m42;
//	m33 = temp.m31 * mat.m13 + temp.m32 * mat.m23 + temp.m33 * mat.m33 + temp.m34 * mat.m43;
//	m34 = temp.m31 * mat.m14 + temp.m32 * mat.m24 + temp.m33 * mat.m34 + temp.m34 * mat.m44;
//	m41 = temp.m41 * mat.m11 + temp.m42 * mat.m21 + temp.m43 * mat.m31 + temp.m44 * mat.m41;
//	m42 = temp.m41 * mat.m12 + temp.m42 * mat.m22 + temp.m43 * mat.m32 + temp.m44 * mat.m42;
//	m43 = temp.m41 * mat.m13 + temp.m42 * mat.m23 + temp.m43 * mat.m33 + temp.m44 * mat.m43;
//	m44 = temp.m41 * mat.m14 + temp.m42 * mat.m24 + temp.m43 * mat.m34 + temp.m44 * mat.m44;
//}
//
//Matrix4x4 Matrix4x4::operator+(const Matrix4x4 &mat)
//{
//	return Matrix4x4(
//		m11 + mat.m11, m12 + mat.m12, m13 + mat.m13, m14 + mat.m14,
//		m21 + mat.m21, m22 + mat.m22, m23 + mat.m23, m24 + mat.m24,
//		m31 + mat.m31, m32 + mat.m32, m33 + mat.m33, m34 + mat.m34,
//		m41 + mat.m41, m42 + mat.m42, m43 + mat.m43, m44 + mat.m44
//		);
//}
//
//Matrix4x4 Matrix4x4::operator-(const Matrix4x4 &mat)
//{
//	return Matrix4x4(
//		m11 - mat.m11, m12 - mat.m12, m13 - mat.m13, m14 - mat.m14,
//		m21 - mat.m21, m22 - mat.m22, m23 - mat.m23, m24 - mat.m24,
//		m31 - mat.m31, m32 - mat.m32, m33 - mat.m33, m34 - mat.m34,
//		m41 - mat.m41, m42 - mat.m42, m43 - mat.m43, m44 - mat.m44
//		);
//}
//
//Matrix4x4 Matrix4x4::operator*(const Matrix4x4 &mat)
//{
//	Matrix4x4 temp(true);
//
//	unsigned int row, column, row_offset;
//
//	for (row = 0, row_offset = row * 4; row < 4; ++row, row_offset = row * 4)
//	{
//		for (column = 0; column < 4; ++column)
//		{
//			temp.elements[row_offset + column] =
//				(elements[row_offset + 0] * mat.elements[column + 0]) +
//				(elements[row_offset + 1] * mat.elements[column + 4]) +
//				(elements[row_offset + 2] * mat.elements[column + 8]) +
//				(elements[row_offset + 3] * mat.elements[column + 12]);
//		}
//	}
//
//	return temp;
//}
//
//Matrix4x4 Matrix4x4::operator*(float f)
//{
//	return Matrix4x4
//	(
//		m11 * f, m12 * f, m13 * f, m14 * f,
//		m21 * f, m22 * f, m23 * f, m24 * f,
//		m31 * f, m32 * f, m33 * f, m34 * f,
//		m41 * f, m42 * f, m43 * f, m44 * f
//	);
//}
//
//Matrix4x4 Matrix4x4::operator/(float f)
//{
//	return Matrix4x4
//	(
//		m11 / f, m12 / f, m13 / f, m14 / f,
//		m21 / f, m22 / f, m23 / f, m24 / f,
//		m31 / f, m32 / f, m33 / f, m34 / f,
//		m41 / f, m42 / f, m43 / f, m44 / f
//	);
//}
//
//// TODO: Check if this is proper column major implementation
//float Matrix4x4::Determinant()
//{
//	return 
//		m14 * m23 * m32 * m41 - m13 * m24 * m32 * m41 - m14 * m22 * m33 * m41 + m12 * m24 * m33 * m41 +
//		m13 * m22 * m34 * m41 - m12 * m23 * m34 * m41 - m14 * m23 * m31 * m42 + m13 * m24 * m31 * m42 +
//		m14 * m21 * m33 * m42 - m11 * m24 * m33 * m42 - m13 * m21 * m34 * m42 + m11 * m23 * m34 * m42 +
//		m14 * m22 * m31 * m43 - m12 * m24 * m31 * m43 - m14 * m21 * m32 * m43 + m11 * m24 * m32 * m43 +
//		m12 * m21 * m34 * m43 - m11 * m22 * m34 * m43 - m13 * m22 * m31 * m44 + m12 * m23 * m31 * m44 +
//		m13 * m21 * m32 * m44 - m11 * m23 * m32 * m44 - m12 * m21 * m33 * m44 + m11 * m22 * m33 * m44;
//}
//
//Matrix4x4 Matrix4x4::Transpose()
//{
//	float temp;
//
//	temp = m12; m12 = m21; m21 = temp;
//	temp = m13; m13 = m31; m31 = temp;
//	temp = m14; m14 = m41; m41 = temp;
//	temp = m23; m23 = m32; m32 = temp;
//	temp = m24; m24 = m42; m42 = temp;
//	temp = m34; m34 = m43; m43 = temp;
//
//	return this;
//}
//
//// TODO: Revise for column major implementation
//Matrix4x4 Matrix4x4::Inverse()
//{
//	// Check if matrix has inverse (determinant must not equal 0)
//	if (Determinant() != 0)
//	{
//		Matrix4x4 mInverse;		// Inverse matrix
//		Matrix4x4 mAdjoint;		// Adjoint matrix
//
//		// Adjugated / Adjoint matrix (after minor's cofactoring and transpose)
//		mAdjoint.m11 = m22 * m33 * m44 - m22 * m34 * m43 - m32 * m23 * m44 + m32 * m24 * m43 + m42 * m23 * m34 - m42 * m24 * m33;
//		mAdjoint.m12 = -m11 * m33 * m44 + m12 * m34 * m43 + m32 * m13 * m44 - m32 * m14 * m43 - m42 * m13 * m34 + m42 * m14 * m33;
//		mAdjoint.m13 = m12 * m23 * m44 - m12 * m24 * m43 - m22 * m13 * m44 + m22 * m14 * m43 + m42 * m13 * m24 - m42 * m14 * m23;
//		mAdjoint.m14 = -m12 * m23 * m34 + m12 * m24 * m33 + m22 * m13 * m34 - m22 * m14 * m33 - m32 * m13 * m24 + m32 * m14 * m23;
//		mAdjoint.m21 = -m21 * m33 * m44 + m21 * m34 * m43 + m31 * m23 * m44 - m31 * m24 * m43 - m41 * m23 * m34 + m41 * m24 * m33;
//		mAdjoint.m22 = m11 * m33 * m44 - m11 * m34 * m43 - m31 * m13 * m44 + m31 * m14 * m43 + m41 * m13 * m34 - m41 * m14 * m33;
//		mAdjoint.m23 = -m11 * m23 * m44 + m11 * m24 * m43 + m21 * m13 * m44 - m21 * m14 * m43 - m41 * m13 * m24 + m41 * m14 * m23;
//		mAdjoint.m24 = m11 * m23 * m34 - m11 * m24 * m33 - m21 * m13 * m34 + m21 * m14 * m33 + m31 * m13 * m24 - m31 * m14 * m23;
//		mAdjoint.m31 = m21 * m32 * m44 - m21 * m34 * m42 - m31 * m22 * m44 + m31 * m24 * m42 + m41 * m22 * m34 - m41 * m24 * m32;
//		mAdjoint.m32 = -m11 * m32 * m44 + m11 * m34 * m42 + m31 * m12 * m44 - m31 * m14 * m42 - m41 * m12 * m34 + m41 * m14 * m32;
//		mAdjoint.m33 = m11 * m22 * m44 - m11 * m24 * m42 - m21 * m12 * m44 + m21 * m14 * m42 + m41 * m12 * m24 - m41 * m14 * m22;
//		mAdjoint.m34 = -m11 * m22 * m34 + m11 * m24 * m32 + m21 * m12 * m34 - m21 * m14 * m32 - m31 * m12 * m24 + m31 * m14 * m22;
//		mAdjoint.m41 = -m21 * m32 * m43 + m21 * m33 * m42 + m31 * m22 * m43 - m31 * m23 * m42 - m41 * m22 * m33 + m41 * m23 * m32;
//		mAdjoint.m42 = m11 * m32 * m43 - m11 * m33 * m42 - m31 * m12 * m43 + m31 * m13 * m42 + m41 * m12 * m33 - m41 * m13 * m32;
//		mAdjoint.m43 = -m11 * m22 * m43 + m11 * m23 * m42 + m21 * m12 * m43 - m21 * m13 * m42 - m41 * m12 * m23 + m41 * m13 * m22;
//		mAdjoint.m44 = m11 * m22 * m33 - m11 * m23 * m32 - m21 * m12 * m33 + m21 * m13 * m32 + m31 * m12 * m23 - m31 * m13 * m22;
//
//		mInverse = mAdjoint / Determinant();
//
//		m11 = mInverse.m11;
//		m12 = mInverse.m12;
//		m13 = mInverse.m13;
//		m14 = mInverse.m14;
//		m21 = mInverse.m21;
//		m22 = mInverse.m22;
//		m23 = mInverse.m23;
//		m24 = mInverse.m24;		
//		m31 = mInverse.m31;
//		m32 = mInverse.m32;
//		m33 = mInverse.m33;
//		m34 = mInverse.m34;
//		m41 = mInverse.m41;
//		m42 = mInverse.m42;
//		m43 = mInverse.m43;
//		m44 = mInverse.m44;		
//	}
//
//	return this;
//}
//
//Vector4 Matrix4x4::GetRow(int row)
//{
//	if (row == 1) return Vector4(m11, m21, m31, m41);
//	else if (row == 2) return Vector4(m12, m22, m32, m42);
//	else if (row == 3) return Vector4(m13, m23, m33, m43);
//	else if (row == 4) return Vector4(m14, m24, m34, m44);
//	else return Vector4(); 
//}
//
//Vector4 Matrix4x4::GetColumn(int column)
//{
//	if (column == 1) return Vector4(m11, m12, m13, m14);
//	else if (column == 2) return Vector4(m21, m22, m23, m24);
//	else if (column == 3) return Vector4(m31, m32, m33, m34);
//	else if (column == 4) return Vector4(m41, m42, m43, m44);
//	else return Vector4();
//}
//
//void Matrix4x4::SetRow(int row, Vector4 rowVector)
//{
//	if (row == 1) { m11 = rowVector.x; m21 = rowVector.y; m31 = rowVector.z; m41 = rowVector.w; }
//	else if (row == 2) { m12 = rowVector.x; m22 = rowVector.y; m32 = rowVector.z; m42 = rowVector.w; }
//	else if (row == 3) { m13 = rowVector.x; m23 = rowVector.y; m33 = rowVector.z; m43 = rowVector.w; }
//	else if (row == 4) { m14 = rowVector.x; m24 = rowVector.y; m34 = rowVector.z; m44 = rowVector.w; }
//}
//
//void Matrix4x4::SetColumn(int column, Vector4 columnVector)
//{
//	if (column == 1) { m11 = columnVector.x; m12 = columnVector.y; m13 = columnVector.z; m14 = columnVector.w; }
//	else if (column == 2) { m21 = columnVector.x; m22 = columnVector.y; m23 = columnVector.z; m24 = columnVector.w; }
//	else if (column == 3) { m31 = columnVector.x; m32 = columnVector.y; m33 = columnVector.z; m34 = columnVector.w; }
//	else if (column == 3) { m41 = columnVector.x; m42 = columnVector.y; m43 = columnVector.z; m44 = columnVector.w; }
//}
//
//Matrix4x4 Matrix4x4::Zero()
//{
//	return &Matrix4x4();
//}
//
//Matrix4x4 Matrix4x4::Identity()
//{
//	return &Matrix4x4(true);
//}
//
//// Generic rotate method where axis is specified as Vector3
//Matrix4x4 Matrix4x4::Rotate(Matrix4x4 mat, float angle, Vector3 vec)
//{
//	float aRad = DEGREES_TO_RADIANS * angle;				// Convert degrees to radians
//	float cosA = cos(aRad);									// Cosinus of the angle
//	float sinA = sin(aRad);									// Sinus of the angle
//
//	Vector3 axis = vec.Normalize();							// Rotation axis (normalized vector)
//	Vector3 temp(axis * (1.0f - cosA));
//
//	Matrix4x4 rotationMatrix;
//	rotationMatrix.m11 = cosA + temp.x * axis.x;
//	rotationMatrix.m21 = 0 + temp.x * axis.y + sinA * axis.z;
//	rotationMatrix.m31 = 0 + temp.x * axis.z - sinA * axis.y;
//	rotationMatrix.m41 = 0;
//	rotationMatrix.m12 = 0 + temp.y * axis.x - sinA * axis.z;
//	rotationMatrix.m22 = cosA + temp.y * axis.y;
//	rotationMatrix.m32 = 0 + temp.y * axis.z + sinA * axis.x;
//	rotationMatrix.m42 = 0;
//	rotationMatrix.m13 = 0 + temp.z * axis.x + sinA * axis.y;
//	rotationMatrix.m23 = 0 + temp.z * axis.y - sinA * axis.x;
//	rotationMatrix.m33 = cosA + temp.z * axis.z;
//	rotationMatrix.m43 = 0;
//	rotationMatrix.m14 = 0;
//	rotationMatrix.m24 = 0;
//	rotationMatrix.m34 = 0;
//	rotationMatrix.m44 = 1;
//
//	Matrix4x4 result;
//	result.m11 = mat.m11 * rotationMatrix.m11 + mat.m12 * rotationMatrix.m21 + mat.m13 * rotationMatrix.m31 + mat.m14 * rotationMatrix.m41;
//	result.m12 = mat.m11 * rotationMatrix.m12 + mat.m12 * rotationMatrix.m22 + mat.m13 * rotationMatrix.m32 + mat.m14 * rotationMatrix.m42;
//	result.m13 = mat.m11 * rotationMatrix.m13 + mat.m12 * rotationMatrix.m23 + mat.m13 * rotationMatrix.m33 + mat.m14 * rotationMatrix.m43;
//	result.m14 = mat.m11 * rotationMatrix.m14 + mat.m12 * rotationMatrix.m24 + mat.m13 * rotationMatrix.m34 + mat.m14 * rotationMatrix.m44;
//	result.m21 = mat.m21 * rotationMatrix.m11 + mat.m22 * rotationMatrix.m21 + mat.m23 * rotationMatrix.m31 + mat.m24 * rotationMatrix.m41;
//	result.m22 = mat.m21 * rotationMatrix.m12 + mat.m22 * rotationMatrix.m22 + mat.m23 * rotationMatrix.m32 + mat.m24 * rotationMatrix.m42;
//	result.m23 = mat.m21 * rotationMatrix.m13 + mat.m22 * rotationMatrix.m23 + mat.m23 * rotationMatrix.m33 + mat.m24 * rotationMatrix.m43;
//	result.m24 = mat.m21 * rotationMatrix.m14 + mat.m22 * rotationMatrix.m24 + mat.m23 * rotationMatrix.m34 + mat.m24 * rotationMatrix.m44;
//	result.m31 = mat.m31 * rotationMatrix.m11 + mat.m32 * rotationMatrix.m21 + mat.m33 * rotationMatrix.m31 + mat.m34 * rotationMatrix.m41;
//	result.m32 = mat.m31 * rotationMatrix.m12 + mat.m32 * rotationMatrix.m22 + mat.m33 * rotationMatrix.m32 + mat.m34 * rotationMatrix.m42;
//	result.m33 = mat.m31 * rotationMatrix.m13 + mat.m32 * rotationMatrix.m23 + mat.m33 * rotationMatrix.m33 + mat.m34 * rotationMatrix.m43;
//	result.m34 = mat.m31 * rotationMatrix.m14 + mat.m32 * rotationMatrix.m24 + mat.m33 * rotationMatrix.m34 + mat.m34 * rotationMatrix.m44;
//	result.m41 = mat.m41 * rotationMatrix.m11 + mat.m42 * rotationMatrix.m21 + mat.m43 * rotationMatrix.m31 + mat.m44 * rotationMatrix.m41;
//	result.m42 = mat.m41 * rotationMatrix.m12 + mat.m42 * rotationMatrix.m22 + mat.m43 * rotationMatrix.m32 + mat.m44 * rotationMatrix.m42;
//	result.m43 = mat.m41 * rotationMatrix.m13 + mat.m42 * rotationMatrix.m23 + mat.m43 * rotationMatrix.m33 + mat.m44 * rotationMatrix.m43;
//	result.m43 = mat.m41 * rotationMatrix.m13 + mat.m42 * rotationMatrix.m24 + mat.m43 * rotationMatrix.m33 + mat.m44 * rotationMatrix.m44;
//
//	return result;
//}
//
//glm::mat4 ToGlmMat4(Matrix4x4 &mat)
//{
//	glm::mat4 temp;
//
//	temp[0][0] = mat.m11; temp[1][0] = mat.m12; temp[2][0] = mat.m13; temp[3][0] = mat.m14;
//	temp[0][1] = mat.m21; temp[1][1] = mat.m22; temp[2][1] = mat.m23; temp[3][1] = mat.m24;
//	temp[0][2] = mat.m31; temp[1][2] = mat.m32; temp[2][2] = mat.m33; temp[3][2] = mat.m34;
//	temp[0][3] = mat.m41; temp[1][3] = mat.m42; temp[2][3] = mat.m43; temp[3][3] = mat.m44;
//
//	return glm::mat4();
//}
//
//glm::mat4 Matrix4x4::ToGlmMat4(Matrix4x4 &mat)
//{
//	glm::mat4 temp;
//
//	temp[0][0] = mat.m11; temp[1][0] = mat.m12; temp[2][0] = mat.m13; temp[3][0] = mat.m14;
//	temp[0][1] = mat.m21; temp[1][1] = mat.m22; temp[2][1] = mat.m23; temp[3][1] = mat.m24;
//	temp[0][2] = mat.m31; temp[1][2] = mat.m32; temp[2][2] = mat.m33; temp[3][2] = mat.m34;
//	temp[0][3] = mat.m41; temp[1][3] = mat.m42; temp[2][3] = mat.m43; temp[3][3] = mat.m44;
//
//	return temp;
//}
//
//glm::mat4 Matrix4x4::ToGlmMat4()
//{
//	glm::mat4 temp;
//
//	temp[0][0] = m11; temp[1][0] = m12; temp[2][0] = m13; temp[3][0] = m14;
//	temp[0][1] = m21; temp[1][1] = m22; temp[2][1] = m23; temp[3][1] = m24;
//	temp[0][2] = m31; temp[1][2] = m32; temp[2][2] = m33; temp[3][2] = m34;
//	temp[0][3] = m41; temp[1][3] = m42; temp[2][3] = m43; temp[3][3] = m44;
//
//	return temp;
//}
//
//
//
//
////// STATIC METHODS ////
//
//float Matrix4x4::Cotangent(float angle)
//{
//	return (float)(1.0 / tan(angle));
//}
//
//float Matrix4x4::DegreesToRadians(float degrees)
//{
//	return degrees * (float)(PI / 180);
//}
//
//float Matrix4x4::RadiansToDegrees(float radians)
//{
//	return radians * (float)(180 / PI);
//}
//
//Matrix4x4 Matrix4x4::MultiplyMatrices(const Matrix4x4* m1, const Matrix4x4* m2)
//{
//	Matrix4x4 out(true);	// Identity matrix
//	unsigned int row, column, row_offset;
//
//	for (row = 0, row_offset = row * 4; row < 4; ++row, row_offset = row * 4)
//	for (column = 0; column < 4; ++column)
//		out.elements[row_offset + column] =
//		(m1->elements[row_offset + 0] * m2->elements[column + 0]) +
//		(m1->elements[row_offset + 1] * m2->elements[column + 4]) +
//		(m1->elements[row_offset + 2] * m2->elements[column + 8]) +
//		(m1->elements[row_offset + 3] * m2->elements[column + 12]);
//
//	return out;
//}
//
//void Matrix4x4::ScaleMatrix(Matrix4x4* m, float x, float y, float z)
//{
//	Matrix4x4 scale(true);
//
//	scale.elements[0] = x;
//	scale.elements[5] = y;
//	scale.elements[10] = z;
//
//	memcpy(m->elements, MultiplyMatrices(m, &scale).elements, sizeof(m->elements));
//}
//
//void Matrix4x4::TranslateMatrix(Matrix4x4* m, float x, float y, float z)
//{
//	Matrix4x4 translation(true);
//
//	translation.elements[12] = x;
//	translation.elements[13] = y;
//	translation.elements[14] = z;
//
//	memcpy(m->elements, MultiplyMatrices(m, &translation).elements, sizeof(m->elements));
//}
//
//void Matrix4x4::RotateAboutX(Matrix4x4* m, float angle)
//{
//	Matrix4x4 rotation (true);
//	float sine = (float)sin(angle);
//	float cosine = (float)cos(angle);
//
//	rotation.elements[5] = cosine;
//	rotation.elements[6] = -sine;
//	rotation.elements[9] = sine;
//	rotation.elements[10] = cosine;
//
//	memcpy(m->elements, MultiplyMatrices(m, &rotation).elements, sizeof(m->elements));
//}
//
//void Matrix4x4::RotateAboutY(Matrix4x4* m, float angle)
//{
//	Matrix4x4 rotation(true);
//	float sine = (float)sin(angle);
//	float cosine = (float)cos(angle);
//
//	rotation.elements[0] = cosine;
//	rotation.elements[8] = sine;
//	rotation.elements[2] = -sine;
//	rotation.elements[10] = cosine;
//
//	memcpy(m->elements, MultiplyMatrices(m, &rotation).elements, sizeof(m->elements));
//}
//
//void Matrix4x4::RotateAboutZ(Matrix4x4* m, float angle)
//{
//	Matrix4x4 rotation (true);
//	float sine = (float)sin(angle);
//	float cosine = (float)cos(angle);
//
//	rotation.elements[0] = cosine;
//	rotation.elements[1] = -sine;
//	rotation.elements[4] = sine;
//	rotation.elements[5] = cosine;
//
//	memcpy(m->elements, MultiplyMatrices(m, &rotation).elements, sizeof(m->elements));
//}
//
//Matrix4x4 Matrix4x4::CreateProjectionMatrix(
//	float fovy,
//	float aspect_ratio,
//	float near_plane,
//	float far_plane
//	)
//{
//	Matrix4x4 out = { { 0 } };
//
//	const float
//		y_scale = Cotangent(DegreesToRadians(fovy / 2)),
//		x_scale = y_scale / aspect_ratio,
//		frustum_length = far_plane - near_plane;
//
//	out.elements[0] = x_scale;
//	out.elements[5] = y_scale;
//	out.elements[10] = -((far_plane + near_plane) / frustum_length);
//	out.elements[11] = -1;
//	out.elements[14] = -((2 * near_plane * far_plane) / frustum_length);
//
//	return out;
//}