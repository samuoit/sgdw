/*
	This is the implementation for Util class.
	Samir Suljkanovic 2016
*/
#include "Engine.h"

#define BUFFER_OFFSET(i) ((char *)0 + (i))

GLuint screenQuadVAO = GL_NONE;												// Screen quad VAO
GLuint screenQuadVBO = GL_NONE;												// Screen quad VBO

// ==================== Bellow function prototypes are defined in Util.h ===================
const char* Util::LoadFile(const char* fileName, const char* fileMode)
{
	// Check if valid file path is provided
	if (fileName)
	{
		FILE* fileData = fopen(fileName, fileMode);	// Open file in requested mode - rb = read binary

		// Check if data was loaded and if it was then return contents
		if (fileData)
		{
			// Get characters count
			fseek(fileData, 0, SEEK_END);
			unsigned count = (unsigned)ftell(fileData);

			// Check if count is greater than 0
			if (count)
			{
				char* fileContents = (char*)malloc(count + 1);				// Allocate memory for size of the file
				rewind(fileData);											// Go to start of the file
				count = (unsigned)fread(fileContents, 1, count, fileData);	// Read file content to allocated memory
				*(fileContents + count) = 0;								// Null terminate content
				return fileContents;										// Return file content
			}
		}
	}

	// If error occurred return 0
	return 0;
}

bool InitFullScreenQuad()
{
	float quadData[] = 
	{
		// vertices positions
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,

		// UVs
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f

	};

	int vertexSize = 6 * 3 * sizeof(float);			// Size in bytes: 6 vertices [positions] (with 3 coordinates => x,y,z) to make 2 triangles making up the quad
	int texCoordSize = 6 * 2 * sizeof(float);		// Size in bytes: 6 texture coordinates (with 2 coordinates u,v)

	glGenVertexArrays(1, &screenQuadVAO);			// Generate VAO
	glBindVertexArray(screenQuadVAO);				// Bind VAO

	glEnableVertexAttribArray(0);					// Enable attribute array for vertices positions
	glEnableVertexAttribArray(1);					// Enable attribute array for UV coordinates

	glGenBuffers(1, &screenQuadVBO);				// Generate VBO
	glBindBuffer(GL_ARRAY_BUFFER, screenQuadVBO);	// Bind VBO
	glBufferData(GL_ARRAY_BUFFER, vertexSize + texCoordSize, quadData, GL_STATIC_DRAW);		// Load buffer data

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));			// Pointer to vertices positions data
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertexSize));	// Pointer to vertices texture coordinates data

	// Clear video memory
	glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	glBindVertexArray(GL_NONE);

	// Return true for successful initialization
	return true;
}

void DrawFullScreenQuad()
{
	glBindVertexArray(screenQuadVAO);				// Bind VAO
	glDrawArrays(GL_TRIANGLES, 0, 6);				// Draw two defined triangles to form a quad
	glBindVertexArray(GL_NONE);						// Release memory
}

// ==================== Above function prototypes are defined in Util.h ===================

// ================== Bellow function prototypes are defined in Engine.h ==================
const Matrix BIAS_MATRIX = 
{
	0.5, 0.0, 0.0, 0.5,
	0.0, 0.5, 0.0, 0.5,
	0.0, 0.0, 0.5, 0.5,
	0.0, 0.0, 0.0, 1.0
};

const Matrix IDENTITY_MATRIX = 
{
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1
};

float Cotangent(float angle)
{
	return (float)(1.0 / tan(angle));
}

float DegreesToRadians(float degrees)
{
	return degrees * (float)(PI / 180);
}

float RadiansToDegrees(float radians)
{
	return radians * (float)(180 / PI);
}

Matrix MultiplyMatrices(const Matrix* m1, const Matrix* m2)
{
	Matrix out = IDENTITY_MATRIX;
	unsigned int row, column, row_offset;

	for (row = 0, row_offset = row * 4; row < 4; ++row, row_offset = row * 4)
	for (column = 0; column < 4; ++column)
		out.elements[row_offset + column] =
		(m1->elements[row_offset + 0] * m2->elements[column + 0]) +
		(m1->elements[row_offset + 1] * m2->elements[column + 4]) +
		(m1->elements[row_offset + 2] * m2->elements[column + 8]) +
		(m1->elements[row_offset + 3] * m2->elements[column + 12]);

	return out;
}

void ScaleMatrix(Matrix* m, float x, float y, float z)
{
	Matrix scale = IDENTITY_MATRIX;

	scale.elements[0] = x;
	scale.elements[5] = y;
	scale.elements[10] = z;

	memcpy(m->elements, MultiplyMatrices(m, &scale).elements, sizeof(m->elements));
}

void TranslateMatrix(Matrix* m, float x, float y, float z)
{
	Matrix translation = IDENTITY_MATRIX;

	translation.elements[12] = x;
	translation.elements[13] = y;
	translation.elements[14] = z;

	memcpy(m->elements, MultiplyMatrices(m, &translation).elements, sizeof(m->elements));
}

void RotateAboutX(Matrix* m, float angle)
{
	Matrix rotation = IDENTITY_MATRIX;
	
	rotation.elements[5] = cosf(angle);
	rotation.elements[6] = sinf(angle);
	rotation.elements[9] = -sinf(angle);
	rotation.elements[10] = cosf(angle);

	memcpy(m->elements, MultiplyMatrices(m, &rotation).elements, sizeof(m->elements));
}

void RotateAboutY(Matrix* m, float angle)
{
	Matrix rotation = IDENTITY_MATRIX;
	
	rotation.elements[0] = cosf(angle);
	rotation.elements[2] = -sinf(angle);
	rotation.elements[8] = sinf(angle);
	rotation.elements[10] = cosf(angle);

	memcpy(m->elements, MultiplyMatrices(m, &rotation).elements, sizeof(m->elements));
}

void RotateAboutZ(Matrix* m, float angle)
{
	Matrix rotation = IDENTITY_MATRIX;
	float sine = sin(angle);
	float cosine = cos(angle);

	rotation.elements[0] = cosf(angle);
	rotation.elements[1] = sinf(angle);
	rotation.elements[4] = -sinf(angle);
	rotation.elements[5] = cosf(angle);

	memcpy(m->elements, MultiplyMatrices(m, &rotation).elements, sizeof(m->elements));
}

Matrix CreateProjectionMatrix(float fovy, float aspect_ratio, float near_plane, float far_plane)
{
	Matrix out = { { 0 } };

	const float
		y_scale = Cotangent(DegreesToRadians(fovy / 2.0f)),
		x_scale = y_scale / aspect_ratio,
		frustum_length = far_plane - near_plane;

	out.elements[0] = x_scale;
	out.elements[5] = y_scale;
	out.elements[10] = -((far_plane + near_plane) / frustum_length);
	out.elements[11] = -1.0f;
	out.elements[14] = -((2.0f * near_plane * far_plane) / frustum_length);

	return out;
}

Matrix CreatePerspectiveProjection(float fovy, float aspect_ratio, float near_plane, float far_plane)
{
	Matrix out = { { 0 } };

	float yF = Cotangent(DegreesToRadians(fovy / 2.0f));
	float xF = yF / aspect_ratio;

	out.elements[0] = xF;
	out.elements[5] = yF;
	out.elements[10] = (near_plane + far_plane) / (near_plane - far_plane);
	out.elements[11] = -1.0f;
	out.elements[14] = (2 * near_plane * far_plane) / (near_plane - far_plane);

	return out;
}

Matrix CreateOrtographicProjectionMatrix(float left, float right, float top, float bottom, float zNear, float zFar)
{
	Matrix out{ { 0 } };

	out.elements[0] = 2.0f / (right - left);
	out.elements[5] = 2.0f / (top - bottom);
	out.elements[10] = -2.0f / (zFar - zNear);
	out.elements[12] = -(right + left) / (right - left);
	out.elements[13] = -(top + bottom) / (top - bottom);
	out.elements[14] = -(zFar + zNear) / (zFar - zNear);
	out.elements[15] = 1.0f;

	//out.elements[0] = 2.0f / (right - left);
	//out.elements[5] = 2.0f / (top - bottom);
	//out.elements[10] = -2.0f / (zFar - zNear);
	//out.elements[3] = -(right + left) / (right - left);
	//out.elements[7] = -(top + bottom) / (top - bottom);
	//out.elements[11] = -(zFar + zNear) / (zFar - zNear);
	//out.elements[15] = 1.0f;

	return out;
}

Matrix LookAt(Vector3 eye, Vector3 target, Vector3 up)
{
	Vector3 f((target - eye).Normalize());
	Vector3 s((f.Cross(up)).Normalize());
	Vector3 u(s.Cross(f));

	Matrix out = IDENTITY_MATRIX;
	
	out.elements[0] = s.x;
	out.elements[4] = s.y;
	out.elements[8] = s.z;

	out.elements[1] = u.x;
	out.elements[5] = u.y;
	out.elements[9] = u.z;

	out.elements[2] = -f.x;
	out.elements[6] = -f.y;
	out.elements[10] = -f.z;

	out.elements[12] = -(s.Dot(eye));
	out.elements[13] = -(u.Dot(eye));
	out.elements[14] = f.Dot(eye);

	return out;
}

//Matrix LookAt(Vector3 right, Vector3 up, Vector3 direction)
//{
//	Matrix out = IDENTITY_MATRIX;/
//
//	out.elements[0] = right.x;1
//	out.elements[1] = right.y;
//	out.elements[2] = right.z;
//	out.elements[4] = up.x;
//	out.elements[5] = up.y;
//	out.elements[6] = up.z;
//	out.elements[8] = direction.x;
//	out.elements[9] = direction.y;
//0	out.elements[10] = direction.z;
//}

float Determinant(Matrix* m)
{
	return 
	m->elements[3] * m->elements[6] * m->elements[9] * m->elements[12] - 
	m->elements[2] * m->elements[7] * m->elements[9] * m->elements[12] - 
	m->elements[3] * m->elements[5] * m->elements[10] * m->elements[12] + 
	m->elements[1] * m->elements[7] * m->elements[10] * m->elements[12] +
	m->elements[2] * m->elements[5] * m->elements[11] * m->elements[12] - 
	m->elements[1] * m->elements[6] * m->elements[11] * m->elements[12] - 
	m->elements[3] * m->elements[6] * m->elements[8] * m->elements[13] + 
	m->elements[2] * m->elements[7] * m->elements[8] * m->elements[13] +
	m->elements[3] * m->elements[4] * m->elements[10] * m->elements[13] - 
	m->elements[0] * m->elements[7] * m->elements[10] * m->elements[13] - 
	m->elements[2] * m->elements[4] * m->elements[11] * m->elements[13] + 
	m->elements[0] * m->elements[6] * m->elements[11] * m->elements[13] +
	m->elements[3] * m->elements[5] * m->elements[8] * m->elements[14] - 
	m->elements[1] * m->elements[7] * m->elements[8] * m->elements[14] - 
	m->elements[3] * m->elements[4] * m->elements[9] * m->elements[14] + 
	m->elements[0] * m->elements[7] * m->elements[9] * m->elements[14] +
	m->elements[1] * m->elements[4] * m->elements[11] * m->elements[14] - 
	m->elements[0] * m->elements[5] * m->elements[11] * m->elements[14] - 
	m->elements[2] * m->elements[5] * m->elements[8] * m->elements[15] + 
	m->elements[1] * m->elements[6] * m->elements[8] * m->elements[15] +
	m->elements[2] * m->elements[4] * m->elements[9] * m->elements[15] - 
	m->elements[0] * m->elements[6] * m->elements[9] * m->elements[15] - 
	m->elements[1] * m->elements[4] * m->elements[10] * m->elements[15] + 
	m->elements[0] * m->elements[5] * m->elements[10] * m->elements[15];
}

Matrix TransposeMatrix(Matrix* m)
{
	Matrix transpose = IDENTITY_MATRIX;

	transpose.elements[0] = m->elements[0];
	transpose.elements[1] = m->elements[4];
	transpose.elements[2] = m->elements[8];
	transpose.elements[3] = m->elements[12];
	transpose.elements[4] = m->elements[1];
	transpose.elements[5] = m->elements[5];
	transpose.elements[6] = m->elements[9];
	transpose.elements[7] = m->elements[13];
	transpose.elements[8] = m->elements[2];
	transpose.elements[9] = m->elements[6];
	transpose.elements[10] = m->elements[10];
	transpose.elements[11] = m->elements[14];
	transpose.elements[12] = m->elements[3];
	transpose.elements[13] = m->elements[7];
	transpose.elements[14] = m->elements[11];
	transpose.elements[15] = m->elements[15];

	return transpose;
}

Matrix InverseMatrix(Matrix* m)
{
	Matrix inverse = IDENTITY_MATRIX;
	float determinant = 0;

	// Calculate cofactor minors and adjoint
	inverse.elements[0] = m->elements[5] * m->elements[10] * m->elements[15] - m->elements[5] * m->elements[11] * m->elements[14] - m->elements[9] * m->elements[6] * m->elements[15] + m->elements[9] * m->elements[7] * m->elements[14] + m->elements[13] * m->elements[6] * m->elements[11] - m->elements[13] * m->elements[7] * m->elements[10];
	inverse.elements[4] = -m->elements[4] * m->elements[10] * m->elements[15] + m->elements[4] * m->elements[11] * m->elements[14] + m->elements[8] * m->elements[6] * m->elements[15] - m->elements[8] * m->elements[7] * m->elements[14] - m->elements[12] * m->elements[6] * m->elements[11] + m->elements[12] * m->elements[7] * m->elements[10];
	inverse.elements[8] = m->elements[4] * m->elements[9] * m->elements[15] - m->elements[4] * m->elements[11] * m->elements[13] - m->elements[8] * m->elements[5] * m->elements[15] + m->elements[8] * m->elements[7] * m->elements[13] + m->elements[12] * m->elements[5] * m->elements[11] - m->elements[12] * m->elements[7] * m->elements[9];
	inverse.elements[12] = -m->elements[4] * m->elements[9] * m->elements[14] + m->elements[4] * m->elements[10] * m->elements[13] + m->elements[8] * m->elements[5] * m->elements[14] - m->elements[8] * m->elements[6] * m->elements[13] - m->elements[12] * m->elements[5] * m->elements[10] + m->elements[12] * m->elements[6] * m->elements[9];
	inverse.elements[1] = -m->elements[1] * m->elements[10] * m->elements[15] + m->elements[1] * m->elements[11] * m->elements[14] + m->elements[9] * m->elements[2] * m->elements[15] - m->elements[9] * m->elements[3] * m->elements[14] - m->elements[13] * m->elements[2] * m->elements[11] + m->elements[13] * m->elements[3] * m->elements[10];
	inverse.elements[5] = m->elements[0] * m->elements[10] * m->elements[15] - m->elements[0] * m->elements[11] * m->elements[14] - m->elements[8] * m->elements[2] * m->elements[15] + m->elements[8] * m->elements[3] * m->elements[14] + m->elements[12] * m->elements[2] * m->elements[11] - m->elements[12] * m->elements[3] * m->elements[10];
	inverse.elements[9] = -m->elements[0] * m->elements[9] * m->elements[15] + m->elements[0] * m->elements[11] * m->elements[13] + m->elements[8] * m->elements[1] * m->elements[15] - m->elements[8] * m->elements[3] * m->elements[13] - m->elements[12] * m->elements[1] * m->elements[11] + m->elements[12] * m->elements[3] * m->elements[9];
	inverse.elements[13] = m->elements[0] * m->elements[9] * m->elements[14] - m->elements[0] * m->elements[10] * m->elements[13] - m->elements[8] * m->elements[1] * m->elements[14] + m->elements[8] * m->elements[2] * m->elements[13] + m->elements[12] * m->elements[1] * m->elements[10] - m->elements[12] * m->elements[2] * m->elements[9];
	inverse.elements[2] = m->elements[1] * m->elements[6] * m->elements[15] - m->elements[1] * m->elements[7] * m->elements[14] - m->elements[5] * m->elements[2] * m->elements[15] + m->elements[5] * m->elements[3] * m->elements[14] + m->elements[13] * m->elements[2] * m->elements[7] - m->elements[13] * m->elements[3] * m->elements[6];
	inverse.elements[6] = -m->elements[0] * m->elements[6] * m->elements[15] + m->elements[0] * m->elements[7] * m->elements[14] + m->elements[4] * m->elements[2] * m->elements[15] - m->elements[4] * m->elements[3] * m->elements[14] - m->elements[12] * m->elements[2] * m->elements[7] + m->elements[12] * m->elements[3] * m->elements[6];
	inverse.elements[10] = m->elements[0] * m->elements[5] * m->elements[15] - m->elements[0] * m->elements[7] * m->elements[13] - m->elements[4] * m->elements[1] * m->elements[15] + m->elements[4] * m->elements[3] * m->elements[13] + m->elements[12] * m->elements[1] * m->elements[7] - m->elements[12] * m->elements[3] * m->elements[5];
	inverse.elements[14] = -m->elements[0] * m->elements[5] * m->elements[14] + m->elements[0] * m->elements[6] * m->elements[13] + m->elements[4] * m->elements[1] * m->elements[14] - m->elements[4] * m->elements[2] * m->elements[13] - m->elements[12] * m->elements[1] * m->elements[6] + m->elements[12] * m->elements[2] * m->elements[5];
	inverse.elements[3] = -m->elements[1] * m->elements[6] * m->elements[11] + m->elements[1] * m->elements[7] * m->elements[10] + m->elements[5] * m->elements[2] * m->elements[11] - m->elements[5] * m->elements[3] * m->elements[10] - m->elements[9] * m->elements[2] * m->elements[7] + m->elements[9] * m->elements[3] * m->elements[6];
	inverse.elements[7] = m->elements[0] * m->elements[6] * m->elements[11] - m->elements[0] * m->elements[7] * m->elements[10] - m->elements[4] * m->elements[2] * m->elements[11] + m->elements[4] * m->elements[3] * m->elements[10] + m->elements[8] * m->elements[2] * m->elements[7] - m->elements[8] * m->elements[3] * m->elements[6];
	inverse.elements[11] = -m->elements[0] * m->elements[5] * m->elements[11] + m->elements[0] * m->elements[7] * m->elements[9] + m->elements[4] * m->elements[1] * m->elements[11] - m->elements[4] * m->elements[3] * m->elements[9] - m->elements[8] * m->elements[1] * m->elements[7] + m->elements[8] * m->elements[3] * m->elements[5];
	inverse.elements[15] = m->elements[0] * m->elements[5] * m->elements[10] - m->elements[0] * m->elements[6] * m->elements[9] - m->elements[4] * m->elements[1] * m->elements[10] + m->elements[4] * m->elements[2] * m->elements[9] + m->elements[8] * m->elements[1] * m->elements[6] - m->elements[8] * m->elements[2] * m->elements[5];

	// Find determinant
	determinant = m->elements[0] * inverse.elements[0] + m->elements[1] * inverse.elements[4] + m->elements[2] * inverse.elements[8] + m->elements[3] * inverse.elements[12];

	// If determinant is 0 return zero matrix
	if (determinant == 0) return Matrix{ { 0 } };

	// Otherwise get 1 / determinant
	determinant = 1.0f / determinant;
	
	// Transpose and multiply elements of adjoint by 1 / determinant
	for (unsigned i = 0; i < 16; i++)
	{
		inverse.elements[i] = inverse.elements[i] * determinant;
	}

	return inverse;
}

void PrintMatrixElements(Matrix* matrix, char* message)
{
	if ("DEFAULT" == message)
	{
		printf("\n\nMatrix Elements: \n%.2f %.2f %.2f %.2f \n%.2f %.2f %.2f %.2f \n%.2f %.2f %.2f %.2f \n%.2f %.2f %.2f %.2f\n\n",
			matrix->elements[0], matrix->elements[4], matrix->elements[8], matrix->elements[12],
			matrix->elements[1], matrix->elements[5], matrix->elements[9], matrix->elements[13],
			matrix->elements[2], matrix->elements[6], matrix->elements[10], matrix->elements[14],
			matrix->elements[3], matrix->elements[7], matrix->elements[11], matrix->elements[15]
			);
	}
	else
	{
		char out[200];
		std::strcpy(out, message);
		std::strcat(out, "\n%.2f %.2f %.2f %.2f \n%.2f %.2f %.2f %.2f \n%.2f %.2f %.2f %.2f \n%.2f %.2f %.2f %.2f\n\n");

		printf(out, 
			matrix->elements[0], matrix->elements[4], matrix->elements[8], matrix->elements[12],
			matrix->elements[1], matrix->elements[5], matrix->elements[9], matrix->elements[13],
			matrix->elements[2], matrix->elements[6], matrix->elements[10], matrix->elements[14],
			matrix->elements[3], matrix->elements[7], matrix->elements[11], matrix->elements[15]
			);
	}
}

// ================== Above function prototypes are defined in Engine.h ==================

// Right handed
/*
Matrix LookAt(Vector3 eye, Vector3 target, Vector3 up)
{
Vector3 zaxis = (eye - target).Normalize();		// The "forward" vector.
Vector3 xaxis = (up.Cross(zaxis)).Normalize();	// The "right" vector.
Vector3 yaxis = zaxis.Cross(xaxis);				// The "up" vector.

// Create a 4x4 orientation matrix from the right, up, and forward vectors.
Matrix orientation = {
xaxis.x, yaxis.x, zaxis.x, 0,
xaxis.y, yaxis.y, zaxis.y, 0,
xaxis.z, yaxis.z, zaxis.z, 0,
0, 0, 0, 1
};

// Create a 4x4 translation matrix. The eye position is negated which is equivalent to the inverse of the translation matrix. T(v)^-1 == T(-v)
Matrix translation = {
1, 0, 0, 0,
0, 1, 0, 0,
0, 0, 1, 0,
-eye.x, -eye.y, -eye.z, 1
};

// Combine the orientation and translation to compute the final view matrix
Matrix out = IDENTITY_MATRIX;
unsigned int row, column, row_offset;

for (row = 0, row_offset = row * 4; row < 4; ++row, row_offset = row * 4)
for (column = 0; column < 4; ++column)
out.elements[row_offset + column] =
(orientation.elements[row_offset + 0] * translation.elements[column + 0]) +
(orientation.elements[row_offset + 1] * translation.elements[column + 4]) +
(orientation.elements[row_offset + 2] * translation.elements[column + 8]) +
(orientation.elements[row_offset + 3] * translation.elements[column + 12]);

return out;
}
*/

//void ExitOnGLError(const char* error_message)
//{
//	const GLenum ErrorValue = glGetError();
//
//	if (ErrorValue != GL_NO_ERROR)
//	{
//		fprintf(stderr, "%s: %s\n", error_message, gluErrorString(ErrorValue));
//		exit(EXIT_FAILURE);
//	}
//}

//GLuint LoadShader(const char* filename, GLenum shader_type)
//{
//	GLuint shader_id = 0;
//	FILE* file;
//	long file_size = -1;
//	char* glsl_source;
//
//	if (NULL != (file = fopen(filename, "rb")) &&
//		0 == fseek(file, 0, SEEK_END) &&
//		-1 != (file_size = ftell(file)))
//	{
//		rewind(file);
//
//		if (NULL != (glsl_source = (char*)malloc(file_size + 1)))
//		{
//			if (file_size == (long)fread(glsl_source, sizeof(char), file_size, file))
//			{
//				glsl_source[file_size] = '\0';
//
//				if (0 != (shader_id = glCreateShader(shader_type)))
//				{
//					glShaderSource(shader_id, 1, &glsl_source, NULL);
//					glCompileShader(shader_id);
//					ExitOnGLError("Could not compile a shader");
//				}
//				else
//					fprintf(stderr, "ERROR: Could not create a shader.\n");
//			}
//			else
//				fprintf(stderr, "ERROR: Could not read file %s\n", filename);
//
//			free(glsl_source);
//		}
//		else
//			fprintf(stderr, "ERROR: Could not allocate %i bytes.\n", file_size);
//
//		fclose(file);
//	}
//	else
//	{
//		if (NULL != file)
//			fclose(file);
//		fprintf(stderr, "ERROR: Could not open file %s\n", filename);
//	}
//
//	return shader_id;
//}