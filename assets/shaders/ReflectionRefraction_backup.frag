#version 430

in VertexData
{
	vec3 eyePosition;
	vec3 eyeNormal;
	vec3 texCoord;
} vIn;

layout (binding = 0) uniform samplerCube u_EnvTexture;					// Environmental cube texture
layout (binding = 1) uniform sampler2D u_tex;							// Texture of object for base color

uniform float u_BaseColorStrength = 0.2;								// Strength of base color vs. reflection/refraction color

uniform mat4 u_View;													// View matrix

uniform float u_RefractionRatio = 1.0 / 1.3333;							// Refraction ratio between air and material (water, glass, etc. water = 1.33333)
uniform bool u_IsRefractive = false;									// Object is both refractive and reflective if this is true, otherwise only reflective

layout (location = 0) out vec4 FragColor;								// Fragment color output

void main () 
{
	vec3 incidence = normalize (vIn.eyePosition);							// Normalized eye position (plane of incidence)
	vec3 normal = normalize (vIn.eyeNormal);								// Normalized eye normal

	// Reflection (rays reflected of the object)
	vec3 reflected = reflect (incidence, normal);
	reflected = vec3 (inverse (u_View) * vec4 (reflected, 0.0));		// Convert from eye to world space

	// Refraction (rays refracted by the object)
	vec3 refracted = refract (incidence, normal, u_RefractionRatio);
	refracted = vec3 (inverse (u_View) * vec4 (refracted, 0.0));		// Convert from eye to world space

	// Output fragment color
	vec4 colorReflected = texture(u_EnvTexture, reflected);				// Reflected color
	vec4 colorRefracted = texture(u_EnvTexture, refracted);				// Refracted color

	// Get base color from texture
	vec3 baseColor = texture(u_tex, vIn.texCoord.xy).xyz;

	// If object is both reflective and refractive ouput mix of reflected and refracted colors
	if (u_IsRefractive) 
		FragColor = mix(colorRefracted, colorReflected, u_RefractionRatio);
	else	// or output reflected color with base color
		FragColor = mix(colorReflected, vec4(baseColor, 1), u_BaseColorStrength);
}