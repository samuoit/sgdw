#version 430

layout (location = 0) in vec3 vIn_position;	// Vertex position
layout (location = 1) in vec3 vIn_normal;	// Vertex normal
layout (location = 2) in vec3 vIn_texCoord;	// Vertex texture coordinates

uniform mat4 u_Projection;					// Projection matrix
uniform mat4 u_View;						// View matrix
uniform mat4 u_Model;						// Model matrix

out VertexData
{
	vec3 eyePosition;
	vec3 eyeNormal;
	vec3 texCoord;
} vOut;

void main ()
{
	vOut.eyePosition = vec3 (u_View * u_Model * vec4 (vIn_position, 1.0));			// Ouput eye position in world
	vOut.eyeNormal = vec3 (u_View * u_Model * vec4 (vIn_normal, 0.0));				// Output eye normal in world
	vOut.texCoord = vIn_texCoord;													// Output texture coordinates

	gl_Position = u_Projection * u_View * u_Model * vec4 (vIn_position, 1.0);		// Output vertex position
}