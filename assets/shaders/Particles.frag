#version 430

uniform sampler2D uTex;

in vec2 texcoord;
in float frag_alpha;

out vec4 outColor;

void main()
{
	vec4 color = texture(uTex, texcoord).rgba;
	color.a *= frag_alpha;
	outColor = color;
}