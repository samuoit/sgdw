#version 430

uniform sampler2D u_tex, u_tex2;
uniform float u_blendRatio;

// Fragment Shader Inputs
in VertexData
{
	vec3 position;
	vec3 normal;
	vec3 textureUV;
	vec4 color;
} vIn;

layout(location = 0) out vec4 FragColor;

void main()
{
	vec2 uv = (vIn.textureUV).xy;
	FragColor = vec4(texture(u_tex2, uv) * vec4(1.0, 1.0, 1.0, 1.0));
}