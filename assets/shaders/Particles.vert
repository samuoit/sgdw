#version 430

// VBO data received from C++
layout(location = 0) in vec3 vboIn_position;
layout(location = 1) in float vboIn_size;
layout(location = 2) in float vboIn_alpha;

// Uniform constants
uniform mat4 uModel;
uniform mat4 uView;

// Vertex shader output
out float size;
out float alpha;

void main()
{
	gl_Position = uView * uModel * vec4(vboIn_position, 1.0);	// Calculate vertex position
	size = vboIn_size;											// Pass vertex size
	alpha = vboIn_alpha;										// Pass alpha
}