#version 430

// Vertex shader inputs - sent as VBO attributes and layouts correspond to specified order of attributes in VBO
layout(location = 0) in vec3 vboIn_position;
layout(location = 1) in vec3 vboIn_normal;
layout(location = 2) in vec3 vboIn_textureUV;
layout(location = 3) in vec4 vboIn_color;

// Uniforms - Constants for the entire pipeline sent by C++ functions (glSendUniform*)
uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;

out VertexData
{
	vec3 position;
	vec3 normal;
	vec3 textureUV;
	vec4 color;
} vboOut;

void main()
{
	vboOut.textureUV = vboIn_textureUV;
	vboOut.normal = vboIn_normal;
	vboOut.color = vboIn_color;

	vec4 inPos = vec4(vboIn_position, 1);
	
	gl_Position = (u_Projection * u_View * u_Model) * inPos;
}