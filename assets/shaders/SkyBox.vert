#version 430

layout(location = 0) in vec3 vboIn_position;
uniform mat4 u_Projection, u_View;

out vec3 texCoords;

void main () {
	texCoords = vboIn_position;
	gl_Position = u_Projection * u_View * vec4 (vboIn_position, 1.0);
}