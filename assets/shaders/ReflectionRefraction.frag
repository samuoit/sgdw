#version 430

in VertexData
{
	vec3 eyePosition;
	vec3 eyeNormal;
	vec3 texCoord;
} vIn;

uniform samplerCube u_EnvTexture;										// Environmental cube texture

uniform mat4 u_View;													// View matrix

uniform float u_RefractionRatio = 1.0 / 1.3333;							// Refraction ratio between air and material (water, glass, etc. water = 1.33333)
uniform bool u_IsRefractive = false;									// Object is both refractive and reflective if this is true, otherwise only reflective

layout (location = 0) out vec4 FragColor;								// Fragment color output

void main () 
{
	vec3 incidence = normalize (vIn.eyePosition);						// Normalized eye position (plane of incidence)
	vec3 normal = normalize (vIn.eyeNormal);							// Normalized eye normal

	// Reflection (rays reflected of the object)
	vec3 reflected = reflect (incidence, normal);						// Get reflection vector
	reflected = vec3 (inverse (u_View) * vec4 (reflected, 0.0));		// Convert from eye to world space

	// Refraction (rays refracted by the object)
	vec3 refracted = refract (incidence, normal, u_RefractionRatio);	// Get refraction vector
	refracted = vec3 (inverse (u_View) * vec4 (refracted, 0.0));		// Convert from eye to world space

	// Output fragment color
	vec4 colorReflected = texture(u_EnvTexture, reflected);				// Sample reflected color based on where reflected vector points on texture
	vec4 colorRefracted = texture(u_EnvTexture, refracted);				// Sample refracted color based on where refracted vector points on texture

	// If object is both reflective and refractive ouput mix of reflected and refracted colors
	if (u_IsRefractive) 
		FragColor = mix(colorRefracted, colorReflected, u_RefractionRatio);
	else	// or output reflected color
		FragColor = colorReflected;
}