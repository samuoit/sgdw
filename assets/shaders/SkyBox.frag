#version 430

in vec3 texCoords;

uniform samplerCube u_TexCube;

out vec4 FragColour;

void main () 
{
	FragColour = texture(u_TexCube, texCoords);
}