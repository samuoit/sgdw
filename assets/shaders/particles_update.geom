#version 430

// Geometry comming in and out (points)
layout(points) in;
layout(points) out;
layout(max_vertices = 40) out;

// Inputs passed from vertex shader
in vec3 vPositionPass[];
in vec3 vVelocityPass[];
in vec3 vColorPass[];
in float fLifeTimePass[];
in float fSizePass[];
in int iTypePass[];

// Outputs used by transform feedback for further processing
out vec3 vPositionOut;
out vec3 vVelocityOut;
out vec3 vColorOut;
out float fLifeTimeOut;
out float fSizeOut;
out int iTypeOut;

// Uniform constants
uniform vec3 generatorPosition;					// Initial position where all new particles are spawned
uniform vec3 genGravityVector;					// Gravity force affecting particles
uniform vec3 genVelocityMin;					// Velocity minimum of a new particle
uniform vec3 genVelocityRange;					// Velocity range of a new particle
uniform vec3 genColor;							// Color of a new particle
uniform float genSize;							// Size of a new particle
uniform float genLifeMinimum;					// Minimum life time of a new particle
uniform float genLifeRange;						// Range of life of a new particle
uniform float timeElapsed;						// Time passed since last frame
uniform int countOfParticlesToGenerate;			// How many particles to generate
uniform vec3 randomSeed;						// Seed number for our random number function

vec3 localSeed;									// Vertex copy of random variable to randomize position of vertex on all axis

// Returns random number from zero to one
float randZeroOne()
{
	uint n = floatBitsToUint(localSeed.x * 2531011.0 + localSeed.y * 214013.0 + localSeed.z * 141251.0);
	n = n * (n * n * 15731u + 789221u);
	n = (n >> 9u) | 0x3F800000u;
 
	float fRes =  2.0 - uintBitsToFloat(n);
	localSeed = vec3(localSeed.x + 147158.0 * fRes, localSeed.y * fRes  + 415161.0 * fRes, localSeed.z + 324154.0 * fRes);
	return fRes;
}

void main()
{
	localSeed = randomSeed;
  
	vPositionOut = vPositionPass[0];
	vVelocityOut = vVelocityPass[0];
	
	if(iTypePass[0] != 0) 
	{
		vPositionOut += vVelocityOut * timeElapsed;
		vVelocityOut += genGravityVector * timeElapsed;
	}

	vColorOut = vColorPass[0];
	fLifeTimeOut = fLifeTimePass[0] - timeElapsed;
	fSizeOut = fSizePass[0];
	iTypeOut = iTypePass[0];
    
	if(iTypePass[0] == 0)
	{
		EmitVertex();
		EndPrimitive();
    
		for(int i = 0; i < countOfParticlesToGenerate; i++)
		{
			vPositionOut = generatorPosition;
			vVelocityOut = genVelocityMin + vec3(genVelocityRange.x * randZeroOne(), genVelocityRange.y * randZeroOne(), genVelocityRange.z * randZeroOne());
			vColorOut = genColor;
			fLifeTimeOut = genLifeMinimum + genLifeRange * randZeroOne();
			fSizeOut = genSize;
			iTypeOut = 1;
			EmitVertex();
			EndPrimitive();
		}
	}
	else if(fLifeTimePass[0] > 0.0)
	{
		EmitVertex();
		EndPrimitive(); 
	}
}