/*
	This is the collection of header files to be included in implementation (.cpp) files.
	This file also specifies enumerations and global variables used by various classes of SGDW project.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_ENGINE_H
#define SGDW_ENGINE_H

// -----------------------------------------------------------------------------------------
// *** *** *** Global constants / macros *** *** ***
// -----------------------------------------------------------------------------------------
#define WINDOW_TITLE_PREFIX "Samir Suljkanovic - GDW Substitute - "

#define CHAR_BUFFER_SIZE 128
#define BUFFER_OFFSET(i) ((char *)0 + (i))
#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }
#define FOR(q,n) for(int q=0;q<n;q++)
#define SFOR(q,s,e) for(int q=s;q<=e;q++)
#define RFOR(q,n) for(int q=n;q>=0;q--)
#define RSFOR(q,s,e) for(int q=s;q>=e;q--)
#define ESZ(elem) (int)elem.size()

#define PI					3.14159265358979323846f	// PI
#define DEGREES_TO_RADIANS	0.01745329f				// Degrees to Radians (RAD = DEG * PI / 180)
#define RADIANS_TO_DEGREES	57.2957795f				// Radians to Degrees (DEG = RAD * 180 / PI)

// Placed in this heather file in order to allow for any class easy access to window width / height and window handle. Variables are deckared and initialized in main.cpp file
extern int windowWidth;		// Width of created window
extern int windowHeight;	// Height of created window
extern int windowHandle;	// Handle to created window

// Matrix 4x4 alternative
typedef struct Matrix
{
	float elements[16];
} Matrix;

// Identity matrix
extern const Matrix IDENTITY_MATRIX;

// The other Matrix methods related to world, camera, and projection spaces are defined at the bottom of this file

// -----------------------------------------------------------------------------------------
// *** *** *** Global enumerations *** *** ***
// -----------------------------------------------------------------------------------------
enum COORDINATE_AXIS // Used by Vector classes and Quaternion
{
	X_AXIS,
	Y_AXIS,
	Z_AXIS,
	W_AXIS
};

enum SHADER_SOURCE	// Used by Shader and ShaderProgram classes
{
	SHADER_FILE,
	SHADER_STRING
};

enum VBO_ATTRIBUTE_TYPE	// Used by the VBOAttribute class for attribute type
{
	VBO_POSITION,
	VBO_NORMAL,
	VBO_TEXTURE_UV,
	VBO_COLOR,
	VBO_INTERLEAVED = 8
};

// Particle Definitions
#define NUM_PARTICLE_ATTRIBUTES 6
#define MAX_PARTICLES_ON_SCENE 100000
#define PARTICLE_TYPE_GENERATOR 0
#define PARTICLE_TYPE_NORMAL 1
#define PARTICLE_TYPE_COUNT 13	// Update this when new type of particle is added

enum PARTICLE_TYPE		// Used by particle generator for particle type
{
	PARTICLE_DEFAULT = 0,
	PARTICLE_CUSTOM = 1,
	PARTICLE_RED = 2,
	PARTICLE_GREEN = 3,
	PARTICLE_BLUE = 4,
	PARTICLE_YELLOW = 5,
	PARTICLE_ASH = 6,
	PARTICLE_SNOW = 7,
	PARTICLE_RAIN = 8,
	PARTICLE_SMOKE = 9,
	PARTICLE_DUST = 10,
	PARTICLE_FIRE = 11,
	PARTICLE_CLOUD = 12
};

enum CAMERA_TYPE		// Used when creating projection view
{
	CAMERA_ORTOGRAPHIC,
	CAMERA_PERSPECTIVE,
	CAMERA_OBLIQUE,
	CAMERA_STEREOSCOPIC
};

// -----------------------------------------------------------------------------------------
// *** *** *** Various includes *** *** ***
// -----------------------------------------------------------------------------------------
// === Standard libraries ===
#include <ctime>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <time.h>

// Collections
#include <map>
#include <vector>

// GLM library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// === Glew, Freeglut, SOIL === 
#include <GL/glew.h>
#include <gl/wglew.h>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include <SOIL/SOIL.h>

// === Other includes ===
#include "Util.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Quaternion.h"

#include "Shader.h"
#include "ShaderProgram.h"

#include "Texture.h"
#include "FrameBufferObject.h"
#include "VBOAttribute.h"
#include "ObjLoader.h"
#include "VertexBufferObject.h"
#include "Timer.h"
#include "Game.h"

#include "ParticleSystem.h"
#include "ParticleGenerator.h"
#include "Particle.h"
#include "SkyBox.h"
#include "Water.h"

// -----------------------------------------------------------------------------------------

float Cotangent(float angle);
float DegreesToRadians(float degrees);		// Convert degress to radians
float RadiansToDegrees(float radians);		// Convert radians to degrees

Matrix MultiplyMatrices(const Matrix* m1, const Matrix* m2);	// Multiply two Matrix objects
void RotateAboutX(Matrix* m, float angle);						// Create rotation matrix about X axis
void RotateAboutY(Matrix* m, float angle);						// Create rotation matrix about Y axis
void RotateAboutZ(Matrix* m, float angle);						// Create rotation matrix about Z axis
void ScaleMatrix(Matrix* m, float x, float y, float z);			// Create scale matrix for x, y, z axis
void TranslateMatrix(Matrix* m, float x, float y, float z);		// Create translate matrix for x, y, z axis

Matrix InverseMatrix(Matrix* m);								// Create inverse matrix (via adjoin and minor cofactor matrices)
Matrix TransposeMatrix(Matrix* m);								// Create transpose matrix (shifting elements arround diagonal)
float Determinant(Matrix* m);									// Returns value of determinant for specified matrix

Matrix CreateProjectionMatrix(									// Create perspective projection matrix for field of view, aspect ratio, and near and far frustum planes
	float fovy, 
	float aspect_ratio, 
	float near_plane, 
	float far_plane
	);

Matrix CreatePerspectiveProjection(								// Same as above with slight variation in implementation
	float fovy, 
	float aspect_ratio, 
	float near_plane, 
	float far_plane
	);

Matrix CreateOrtographicProjectionMatrix(						// Create ortographic projection matrix for window specified with left, right, top, bottom, and near and far frustum planes
	float left,
	float right,
	float top,
	float bottom,
	float near_plane,
	float far_plane
	);

void PrintMatrixElements(Matrix* m, char* message = "DEFAULT");	// Print elements of the matrix preceeded with the message (if no message is specieid i.e. DEFAULT then default message is displayed)

Matrix LookAt(Vector3 eye, Vector3 target, Vector3 up);			// Create 'view' i.e. LookAt matrix based on observer position (eye), viewing target (target), and up vector.


bool InitFullScreenQuad();													// Initialize VBO for full screen quad drawing (for example, used by FBO's textures)
void DrawFullScreenQuad();													// Draw previously initialized VBO representing full screen quad

#endif