/*
	This is the header for Shader class.
	Samir Suljkanovic 2016
*/
#ifndef SGDW_SHADER_H
#define SGDW_SHADER_H

class Shader
{
public :
	// Constructor(s) for Shader initialization
	Shader();														// Default constructor
	~Shader();														// Destructor
	
	unsigned int LoadShader(SHADER_SOURCE, const char*, GLenum);	// Load shader from code or file
	unsigned int GetHandle();										// Return handle for this shader
	void Destroy();													// Release memory occupied by this shader and reset handle

private:
	unsigned int handle;											// Shader's handle
	GLenum type;													// Shader's type
};

#endif