/*
	This is the header for ParticleGenerator class.
	Samir Suljkanovic 2016
*/
#ifndef SGDW_PARTICLE_GENERATOR_H
#define SGDW_PARTICLE_GENERATOR_H

class ParticleGenerator
{
public:
	
	ParticleGenerator();											// Default constructor
	~ParticleGenerator();											// Destructor

	bool Init														// Initializing particle engine shaders, shader program, particle arrays, properties, varyings, etc.
		(
		Vector3 newGeneratorPosition,									// Position of particle spawn point
		Vector3 newGeneratorMinimumVelocity,							// Minimum velocity of particles on all axis
		Vector3 newGeneratorMaximumVelocity,							// Maximum velocity of particles on all axis
		Vector3 newGeneratorGravityVelocity,							// Gravity force affecting generated particles
		Vector3 newGeneratorColor,										// Color of generated particles
		float newGeneratorMinimumLife,									// Minimum life time for particle
		float newGeneratorMaximumLife,									// Maximum life time for particle
		float newGeneratorSize,											// Size of generated particle
		float repeatGenerateTime,										// Time between particle spawns
		int countOfParticlesToGenerate,									// Number of particles to generate when spawn time is reached
		PARTICLE_TYPE typeOfParticle,									// Type of particle (ash, smoke, fire, etc.)
		const char* textureFilePath = ""								// Optional file path to custom particle texture for custom particle type (PARTICLE_CUSTOM)
		);
	void Render();													// Drawing particles on the screen
	void Update(float timeElapsed);									// Updating particle properties
	void Destroy();													// Release taken resources - cleanup occupied memory

	void SetParticleTypeAndTexture									// Sets texture for particle, if type is PARTICLE_CUSTOM then path of custom file can be given
		(
			PARTICLE_TYPE particleType,									// Type of particle used
			const char* textureFilePath = ""							// File path for custom texture (if type is PARTICLE_CUSTOM)
		);

	void SetMatrices												// Sets projection and view matrices used by shaders to project particles on screen (calculates geometry quads for particle from view matrix)
		(
			Matrix* mNewProjection,										// Projection matrix
			Matrix* mNewView											// View matrix
		);

	void SetMatrices												// Sets projection matrix, and builds view matrix from eye, view and up vectors  (calculates geometry quads for particle from view matrix)
		(
			Matrix* mNewProjection,										// Projection matrix
			Vector3 vNewEye,											// Eye position (camera)
			Vector3 vNewView,											// Viewing target (camera)
			Vector3 vNewUpVector										// Up vector (camera)
		);
	
	int GetParticlesCount();										// Returns number of current particles being output

private:
	
	bool isInitialized;												// Particle engine initialized flag

	unsigned transformFeedbackBuffer;								// Transfor feedback buffer from geometry shader
	unsigned particleBuffer[2];										// Particle buffers
	unsigned vaoParticles[2];										// Particle VAOs
	unsigned uiQuery;												// Query buffer

	const char* particleTextureName;								// Name of the texture for the particle
	Texture particleTexture;										// Texture used for the particle

	int currentReadBuffer;											// Currently read buffer
	int particlesCount;												// Count of particles being rendered

	Matrix mProjection, mView;										// Projection and view matrices
	Vector3 vQuad1, vQuad2;											// Quads being rendered as particles

	float elapsedTime, nextSpawnTime;								// Elapsed time between particle spawns, and particle spawn time

	Vector3 generatorPosition;										// Position of particle engine spawn point
	Vector3 genVelocityMin, genVelocityRange;						// Minimum velocity and range (active forces)
	Vector3 genGravityVector;										// Gravity affecting particle (reactive force)
	Vector3 genColor;												// Particle color
	float genLifeMinimum, genLifeRange;								// Minimum lifetime of a particle and life range
	float genSize;													// Size of particle
	int countOfParticlesToGenerate;									// Number of particles to spawn at a time

	// Shaders and shader programs used by particle engine
	Shader vertUpdateParticles, geomUpdateParticles, fragUpdateParticles, vertRenderParticles, geomRenderParticles, fragRenderParticles;
	ShaderProgram progRenderParticles, progUpdateParticles;
};

#endif