/*
	This is the header for Matrix3x3 class.
	Samir Suljkanovic 2015
*/
#include "Vector3.h"

#ifndef SGDW_MATRIX3X3_H
#define SGDW_MATRIX3X3_H

class Matrix3x3
{
public:
	// Constructor(s) for Matrix3x3 initialization
	Matrix3x3();								// Default Constructor - creates zero matrix
	Matrix3x3(bool isIdentity);					// Creates identity matrix if argument is true, or defaults to zero matrix if argument is false
	Matrix3x3(
		float M11, float M12, float M13,
		float M21, float M22, float M23,
		float M31, float M32, float M33);		// Each Matrix3x3 value set individually by the argument
	Matrix3x3(const Matrix3x3 &mat);			// Matrix3x3 initialized to values from another Matrix3x3 as argument

	~Matrix3x3();								// Destructor

	// Overloaded operators for operations on Matrix3x3s
	bool operator==(const Matrix3x3 &mat);		// Comparison
	void operator=(const Matrix3x3 &mat);		// Assignment
	void operator+=(const Matrix3x3 &mat);		// Addition
	void operator-=(const Matrix3x3 &mat);		// Subtraction
	void operator*=(const Matrix3x3 &mat);		// Multiplication

	Matrix3x3 operator+(const Matrix3x3 &mat);	// Addition with the other Matrix3x3
	Matrix3x3 operator-(const Matrix3x3 &mat);	// Subtraction with the other Matrix3x3
	Matrix3x3 operator*(const Matrix3x3 &mat);	// Multiplication with the other Matrix3x3
	Matrix3x3 operator*(float f);				// Multiplication with scalar
	Matrix3x3 operator/(float f);				// Division by scalar

	float Determinant();						// Returns determinant of  This is the implementation for  matrix
	void Transpose();							// Transposes  This is the implementation for  matrix
	void Inverse();								// Inverses  This is the implementation for  matrix
	void OrthoNormalize();						// Ortho normalize matrix

	Vector3 GetRow(int row);					// Get specified row of the matrix (acceptable arguments 1, 2, 3)
	Vector3 GetColumn(int column);				// Get specified column of the matrix - acceptable arguments 1, 2, 3
	void SetRow(int row, Vector3 rowVector);	// Set specified row of the matrix to vector values
	void SetColumn(int column, Vector3 colVector);	// Set specified column of the matrix to vector values
	
	Matrix3x3 RotationMatrixX(float angle);		// Creates Matrix3x3 to rotate vectors around the X axis by the given angle
	Matrix3x3 RotationMatrixY(float angle);		// Creates Matrix3x3 to rotate vectors around the Y axis by the given angle
	Matrix3x3 RotationMatrixZ(float angle);		// Creates Matrix3x3 to rotate vectors around the Z axis by the given angle
	
	static Vector3 ScaleVector(Matrix3x3 scaleMat, Vector3 vecToScale);	// Returns scaled vector vecToScale based on the scale values from scaleVec parameter vector
	static Matrix3x3 Zero();											// Returns Zero matrix
	static Matrix3x3 Identity();										// Returns Identity matrix
	
	// Allow access to independent elements, or as an array e.g. for a Matrix3x3 named mat, mat.element[0] or m11, mat.element[8] or m33, etc.
	union
	{
		struct
		{
			float m11, m12, m13, m21, m22, m23, m31, m32, m33;
		};

		float elements[9];
	};
};

#endif