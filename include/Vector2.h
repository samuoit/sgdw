/*
	This is the header for Vector2 class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_VECTOR2_H
#define SGDW_VECTOR2_H

class Vector2
{
public:
	// Constructor(s) for Vector3 initialization
	Vector2();								// Default Constructor
	Vector2(float X, float Y);				// Each Vector3 value set individually by the argument
	Vector2(float XY);						// All values initialized to specified value of the argument
	Vector2(const Vector2 &vec);			// Vector initialized to values from another Vector3 as argument

	~Vector2();	// Destructor

	// Overloaded operators for operations on vectors
	bool operator==(const Vector2 &vec);	// Comparison - equal
	bool operator!=(const Vector2 &vec);	// Comparison - not equal
	void operator=(const Vector2 &vec);		// Assignment
	void operator+=(const Vector2 &vec);	// Addition
	void operator-=(const Vector2 &vec);	// Subtraction
	
	Vector2 operator+(const Vector2 &vec);	// Addition with the other vector
	Vector2 operator-(const Vector2 &vec);	// Subtraction with the other vector
	
	// Vector-Scalar operations
	void operator*=(float f);				// Self multiplication by scalar
	void operator/=(float f);				// Self division by scalar
	Vector2 operator*(float f);				// Multiplication with scalar
	Vector2 operator/(float f);				// Division by scalar	
	
	void Negated();							// Negation
	float Dot(const Vector2 &vec);			// Dot product
	float Magnitude();						// Magnitude i.e. length of the Vector3
	float MagnitudeSquared();				// Square of the vector's magnitude
	Vector2 Normalize();					// Normalize vector
	Vector2 Perpendicular();				// Perpendicular vector turned by 90 degrees in relation to original vector
	void Rotate(float angle);				// Rotation arround particular axis
	Vector2 AngleToVector(float angle);		// Angle to vector
	float VectorToAngle(const Vector2 &vec);// Vector to angle
	void Reflect(Vector2& normalizedVec);	// Reflect the vector (useful for rays of light reflecting of the surfaces) 
	
	static Vector2 Zero();					// All Vector2 values as 0 i.e. { 0, 0 }
	static Vector2 One();					// All Vector2 values as 1 i.e. { 1, 1 }

	// Allow access to independent elements, or as an array e.g. for a Vector3 named vec3, vec3.coord[0] or vec3.x
	union
	{
		struct
		{
			float x, y;
		};

		float coords[2];
	};
};

#endif