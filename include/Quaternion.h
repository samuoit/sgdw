/*
	This is the header for Quaternion class.
	Samir Suljkanovic 2016
*/
#include "Vector3.h"

#ifndef SGDW_Quaternion_H
#define SGDW_Quaternion_H

class Quaternion
{
public:
	// Constructor(s) for Quaternion initialization
	Quaternion();									// Default Constructor
	Quaternion(float X, float Y, float Z, float W);	// Each Quaternion value set individually by the argument
	Quaternion(float XYZW)	;						// All values initialized to specified value of the argument
	Quaternion(const Quaternion &quat);				// quaternion initialized to values from another Quaternion as argument

	~Quaternion();	// Destructor

	// Overloaded operators for operations on quaternions
	bool operator==(const Quaternion &quat);		// Comparison =
	bool operator!=(const Quaternion &quat);		// Comparison !=
	void operator=(const Quaternion &quat);			// Assignment
	void operator+=(const Quaternion &quat);		// Addition
	void operator-=(const Quaternion &quat);		// Subtraction
	void operator/=(const Quaternion &quat);		// Division
	void operator*=(const Quaternion &quat);		// Multiplication

	Quaternion operator+(const Quaternion &quat);	// Addition with the other quaternion
	Quaternion operator-(const Quaternion &quat);	// Subtraction with the other quaternion
	Quaternion operator/(const Quaternion &quat);	// Division with the other quaternion
	Quaternion operator*(const Quaternion &quat);	// Multiplication with the other quaternion
	Quaternion operator*(float f);					// Multiplication with scalar
	Quaternion operator/(float f);					// Division by scalar	

	void Negated();									// Negation
	Quaternion Conjugate() const;					// Quaternion conjugation
	float Dot(const Quaternion &quat);				// Dot product
	Quaternion Cross(const Quaternion &quat);		// Cross product
	float Magnitude();								// Magnitude i.e. length of the Quaternion
	float MagnitudeSquared();						// Square of the quaternion's magnitude
	Quaternion Zero();								// All Quaternion values as 0 i.e. { 0, 0, 0, 0 }
	Quaternion Normalize();							// Normalize quaternion
	Vector3 RotateVector(Quaternion quat, const Vector3 vec3) const;				// Rotation of Vector3 by using quaternion
	Quaternion GetRotationQuaternion(const Vector3 &fromVec, const Vector3 &toVec);	// Gets rotation quaternion based on two vectors

	// Allow access to independent elements, or as an array e.g. for a Quaternion named quat, quat.coord[3] or quat.w
	union
	{
		struct
		{
			float x, y, z, w;
		};

		float coords[4];
	};
};

#endif