/*
	This is the header for FrameBufferObject class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_FrameBufferObjectOBJECT_H
#define SGDW_FrameBufferObjectOBJECT_H

class FrameBufferObject
{
public:

	FrameBufferObject() = delete;								// Using "= delete" in constructor assignment will not allow use of default constructor when creating object of the FBO type
	FrameBufferObject(unsigned colorAttachmentsNumber);			// Constructor taking number of color attachments		
	~FrameBufferObject();										// Destructor

	void InitDepthTexture(unsigned width, unsigned height);		// Initializes depth texture
	
	void InitColorTexture(										// Initializes color texture
		unsigned index,
		unsigned width,
		unsigned height, 
		GLint internalFormat,
		GLint filter, 
		GLint wrap);

	bool Check();												// Checks if FBO is valid
	void Destroy();												// Clears OpenGL memory
	void Clear();												// Clears all attached textures
	void Bind();												// Binds FBO
	void UnBind();												// Unbinds FBO

	void MoveToBackBuffer(int windowWidth, int windowHeight);	// Move FBO to back buffer

	GLuint GetColorHandle(unsigned index) const;				// Returns color handle for specified index
	GLuint GetDepthHandle() const;								// Returns depth handle for specified index

	void RenderToCubeMap(GLuint* texHandle, float width, float height);		// Render FBO to cube map

private:
	GLuint frameBuffer = GL_NONE;								// Frame buffer
	GLuint depthAttachment = GL_NONE;							// Depth attachment
	GLuint *colorAttachments = nullptr;							// Color attachment
	GLenum *buffers = nullptr;									// 

	unsigned colorAttachmentsNumber = 0;

};

#endif