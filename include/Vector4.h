/*
	This is the header for Vector4 class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_VECTOR4_H
#define SGDW_VECTOR4_H

class Vector4
{
public:
	// Constructor(s) for Vector4 initialization
	Vector4();										// Default Constructor
	Vector4(float X, float Y, float Z, float W);	// Each Vector4 value set individually by the argument
	Vector4(float XYZW);							// All values initialized to specified value of the argument
	Vector4(const Vector4 &vec);					// Vector initialized to values from another Vector4 as argument

	~Vector4();	// Destructor

	// Overloaded operators for operations on vectors
	bool operator==(const Vector4 &vec);	// Comparison - equal
	bool operator!=(const Vector4 &vec);	// Comparison - not equal
	void operator=(const Vector4 &vec);		// Assignment
	void operator+=(const Vector4 &vec);	// Addition
	void operator-=(const Vector4 &vec);	// Subtraction
	
	Vector4 operator+(const Vector4 &vec);	// Addition with the other vector
	Vector4 operator-(const Vector4 &vec);	// Subtraction with the other vector
	
	// Vector-Scalar operations
	void operator/=(float f);				// Self-division by scalar
	void operator*=(float f);				// Self-multiplication by scalar
	Vector4 operator*(float f);				// Multiplication with scalar
	Vector4 operator/(float f);				// Division by scalar	
	
	void Negated();							// Negation
	float Dot(const Vector4 &vec);			// Dot product
	float Magnitude();						// Magnitude i.e. length of the Vector4
	float MagnitudeSquared();				// Square of the vector's magnitude
	Vector4 Normalize();					// Normalize vector
	Vector4 Cross(const Vector4 &vec);		// Cross product with another vector
	void Rotate(COORDINATE_AXIS, float angle);	// Rotation arround particular axis

	static Vector4 Zero();					// All Vector4 values as 0 i.e. { 0, 0, 0, 0 }
	static Vector4 One();					// All Vector4 values as 1 i.e. { 1, 1, 1, 1 }

	// Allow access to independent elements, or as an array e.g. for a Vector4 named vec, vec.coord[0] or vec.x
	union
	{
		struct
		{
			float x, y, z, w;
		};

		float coords[4];
	};
};

#endif