/*
	This is the header for Matrix4x4 class.
	Samir Suljkanovic 2015
*/
//#include "Vector4.h"

#ifndef SGDW_MATRIX4x4_H
#define SGDW_MATRIX4x4_H

class Matrix4x4
{
public:
	// Constructor(s) for Matrix4x4 initialization
	Matrix4x4();								// Default Constructor - creates zero matrix
	Matrix4x4(bool isIdentity);					// Creates identity matrix if argument is true, or defaults to zero matrix if argument is false
	Matrix4x4(
		float M11, float M12, float M13, float M14,
		float M21, float M22, float M23, float M24,
		float M31, float M32, float M33, float M34,
		float M41, float M42, float M43, float M44
		);										// Each Matrix4x4 value set individually by the argument
	Matrix4x4(const Matrix4x4 &mat);			// Matrix4x4 initialized to values from another Matrix4x4 as argument
	Matrix4x4(glm::mat4 &mat);					// Matrix4x4 initialized from glm::mat4 matrix

	~Matrix4x4();								// Destructor

	// Overloaded operators for operations on Matrix4x4s
	bool operator==(const Matrix4x4 &mat);		// Comparison
	void operator=(const Matrix4x4 &mat);		// Assignment
	void operator+=(const Matrix4x4 &mat);		// Addition
	void operator-=(const Matrix4x4 &mat);		// Subtraction
	void operator*=(const Matrix4x4 &mat);		// Multiplication

	Matrix4x4 operator+(const Matrix4x4 &mat);	// Addition with the other Matrix4x4
	Matrix4x4 operator-(const Matrix4x4 &mat);	// Subtraction with the other Matrix4x4
	Matrix4x4 operator*(const Matrix4x4 &mat);	// Multiplication with the other Matrix4x4
	Matrix4x4 operator*(float f);				// Multiplication with scalar
	Matrix4x4 operator/(float f);				// Division by scalar

	float Determinant();						// Returns determinant of matrix
	Matrix4x4 Transpose();						// Transposes matrix
	Matrix4x4 Inverse();						// Inverses matrix

	Matrix4x4 Rotate(Matrix4x4 mat, float angle, Vector3 vec);	// Creates rotation matrix
	Matrix4x4 Translate();						// Translate

	Vector4 GetRow(int row);					// Get specified row of the matrix (acceptable arguments 1, 2, 3)
	Vector4 GetColumn(int column);				// Get specified column of the matrix - acceptable arguments 1, 2, 3
	void SetRow(int row, Vector4 rowVector);	// Set specified row of the matrix to vector values
	void SetColumn(int column, Vector4 colVector);	// Set specified column of the matrix to vector values

	Matrix4x4 Homogeneous(
		Matrix3x3 rotation, 
		Vector3 translation, 
		float scale);							// Creates homogeneous transformation matrix

	Matrix4x4 Expand3x3To4x4(Matrix3x3 &mat);	// Expands 3 x 3 matrix to create 4 x 4 matrix padded with 0 values

	Matrix4x4 Zero();							// Zero matrix
	Matrix4x4 Identity();						// Identity matrix

	glm::mat4 ToGlmMat4();						// Converts 'this' matrix to glm::mat4
	static glm::mat4 ToGlmMat4(Matrix4x4 &mat);	// Converts given Matrix4x4 to glm::mat4


	// Angles
	static float Cotangent(float angle);
	static float DegreesToRadians(float degrees);
	static float RadiansToDegrees(float radians);

	static Matrix4x4 MultiplyMatrices(const Matrix4x4* mat1, const Matrix4x4* mat2);

	// Transformations
	static void RotateAboutX(Matrix4x4* m, float angle);
	static void RotateAboutY(Matrix4x4* m, float angle);
	static void RotateAboutZ(Matrix4x4* m, float angle);
	static void ScaleMatrix(Matrix4x4* m, float x, float y, float z);
	static void TranslateMatrix(Matrix4x4* m, float x, float y, float z);
	
	// Projection - camera
	static Matrix4x4 CreateProjectionMatrix(float fovy, float aspect_ratio, float near_plane, float far_plane);



	// Allow access to independent elements, or as an array e.g. for a Matrix4x4 named mat, mat.element[0] or m11, mat.element[8] or m33, etc.
	union
	{
		struct
		{
			float m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44;
		};

		float elements[16];
		float colsAndRows[4][4];
	};
};

#endif