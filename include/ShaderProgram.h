/*
	This is the header for ShaderProgram class.
	Samir Suljkanovic 2016
*/
#ifndef SGDW_SHADER_PROGRAM_H
#define SGDW_SHADER_PROGRAM_H

class ShaderProgram
{
public:
	ShaderProgram();												// Default constructor
	~ShaderProgram();												// Destructor

	void AttachShader(Shader shader);								// Attach shader to shader program
	int LinkProgram();												// Link shader program
	void Bind();													// Bind shader program
	void Unbind();													// Unbind shader program
	void Destroy();													// Release memory occupied by this shader and reset handle

	unsigned GetHandle();											// Return handle for shader program

	// Functions used to send uniforms (shader variables) to different shaders
	int GetUniformLocation(char* uniformName);						// Get uniform location from stored map value, or GL if not in map [returns -1 if uniform name is not found]
	void SendUniformMatrix(char* uniformName, Matrix matrix);		// Send Matrix (alternative 4x4 matrix) to shader
	void SendUniformVec4(char* uniformName, Vector4 vec4Value);		// Send Vector4 - vec4 to shader
	void SendUniformVec3(char* uniformName, Vector3 vec3Value);		// Send Vector3 - vec3 to shader
	void SendUniformFloat(char* uniformName, float floatValue);		// Send float to shader
	void SendUniformInt(char* uniformName, int intValue);			// Send int to shader
	void SendUniformBool(char* uniformName, bool boolValue);		// Send boolean to shader

	void SendUniformFloatArray(char* uniformName, float* arr, int arrSize);		// Send float array to shader
	void SendUniformVec3Array(char* uniformName, Vector3* arr, int arrSize);	// Send Vector3 array to shader

	
private:
	std::map<std::string, int> uniformLocations;					// Optimization map for faster access to uniform locations
	unsigned int handle;											// Handle to shader program
	bool isLinked;													// Keeps state of the link for the shader program
};

#endif