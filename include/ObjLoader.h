/*
	This is the header for ObjLoader class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_OBJ_LOADER_H
#define SGDW_OBJ_LOADER_H

struct ObjectFace
{
	ObjectFace()
	{
		vertices[0] = 0;
		vertices[1] = 0;
		vertices[2] = 0;
		normals[0] = 0;
		normals[1] = 0;
		normals[2] = 0;
		textures[0] = 0;
		textures[1] = 0;
		textures[2] = 0;
	}

	unsigned vertices[3];
	unsigned normals[3];
	unsigned textures[3];
};

class ObjLoader
{
public:
	ObjLoader();															// Default constructor
	~ObjLoader();															// Destructor

	bool LoadFromFile(const char* fileName);								// Load .obj file with model data	
	VBOAttribute GetAttributeData(VBO_ATTRIBUTE_TYPE attributeDataType);	// Export requested type of data (VBO_POSITION, VBO_NORMAL, VBO_TEXTURE_UV, VBO_COLOR)
	VBOAttribute ObjLoader::GetInterleavedAttributeData();					// Export all data as interleaved (vertex-normal-textureUVs)
	
private:

	// Collections of sorted vertex data
	std::vector<float> readyInterleavedData;								// Combination of all data for interleaved VBO's (vertex-normal-textureUVs)
	std::vector<float> readyVertexData;										// Vertex positions data ready for use
	std::vector<float> readyTextureData;									// Vertex textures data ready for use
	std::vector<float> readyNormalData;										// Vertex normals data ready for use
	std::vector<float> readyColorData;										// Vertex color data ready for use

	VBOAttribute vboAttribute;												// Temporary collections of prepared vertex data attributes

	// Float arrays to be returned
	float* outInterleavedData;
	float* outVertexData;
	float* outTextureData;
	float* outNormalData;
	float* outColorData;

};

#endif