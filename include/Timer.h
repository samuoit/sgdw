/*
	This is the header for Timer class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_TIMER_H
#define SGDW_TIMER_H

class Timer
{
public:
	Timer();										// Default constructor
	~Timer();										// Destructor

	float TickTock();								// Updates the timer clock
	float GetElapsedTimeMS();						// Delta time in milliseconds 
	float GetElapsedTimeSeconds();					// Delta time in seconds
	float GetCurrentTime();							// Current time

private:
	float 
		currentTime,								// Currently recorded time
		previousTime,								// Previously recorded time
		elapsedTime;								// Difference between currently recorded time and previosly recorded time
};

#endif