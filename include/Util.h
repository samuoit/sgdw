/*
	This is the header for Util class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_Util_H
#define SGDW_Util_H

class Util
{
public:
	static const char* LoadFile(const char* fileName, const char* fileMode);	// Load specified file in specified file mode (e.g. "rb" = read binary)
	
	void SeedRandomNumberGenerator();											// Seed random number generator
	void SeedRandomNumberGenerator(unsigned int seed);							// Seed random number generator based on seed provided

	float RandomRangef(float min, float max);									// Get random float range number between min and max specified
	int RandomRangei(int min, int max);											// Get random int range number between min and max specified
};

#endif