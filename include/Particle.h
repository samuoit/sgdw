/*
	
*/
#ifndef SGDW_PARTICLE_H
#define SGDW_PARTICLE_H

class Particle
{
public:
	Particle();
	~Particle();

	unsigned GetType();
	Vector3 GetPosition();
	Vector3 GetVelocity();
	Vector3 GetColor();
	float GetLifeTimeInMs();
	float GetSize();

	void SetType(unsigned newType);
	void SetPosition(Vector3 newPosition);
	void SetVelocity(Vector3 newVelocity);
	void SetColor(Vector3 newColor);
	void SetLifeTimeInMs(float lifetimeInMilisecs);
	void SetSize(float newSize);

private:
	unsigned type;
	Vector3 position;
	Vector3 velocity;
	Vector3 color;
	float lifetimeInMs;
	float size;
};

#endif