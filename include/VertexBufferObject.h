/*
	This is the header for VertexBufferObject class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_VERTEX_BUFFER_OBJECT_H
#define SGDW_VERTEX_BUFFER_OBJECT_H

struct VBODataType	// Used for offset information
{
	Vector3 positions;
	Vector3 normals;
	Vector3 textureUVs;
};

class VertexBufferObject
{
public:
	VertexBufferObject();									// Default constructor
	~VertexBufferObject();									// Default destructor

	void AddAttributesData(VBOAttribute &atr);				// Adds array of attribute elements to VBOAttribute collection
	void CreateVBOs(bool interleave = true);				// Creates VBOs from collection of all VBOAttribute objects for each VBO
	void Draw();											// Instruct GL to draw VBO
	void Destroy();											// Frees memory by deleting references to VBO
	int GetVBOSize();										// Return size of vector containing all VBO elements with their attributes
	void SetInterleaved(bool interleave = true);			// Set attribute as true = interleaved (default) / false = non-interleaved

private:
	bool isInterleaved;										// Is this VBO interleaved
	unsigned int vaoHandle;									// Handle for VAO (Vertex Array Object)
	std::vector<unsigned> vboHandles;						// Vector (collection) with handles for all VBOs
	std::vector<VBOAttribute> vboAttributes;				// Vector (collection) of VBO Attributes
};

#endif