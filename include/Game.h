/*
	This is the header for Game class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_GAME_H
#define SGDW_GAME_H

// Direction of scene when rendered to FBO
enum RENDER_DIRECTION
{
	RENDER_FRONT = 0,												// Rendered front (neg Z)
	RENDER_BACK = 1,												// Rendered back (pos Z)
	RENDER_BOTTOM = 2,												// Rendered bottom (neg Y)
	RENDER_TOP = 3,													// Rendered top (pos Y)
	RENDER_RIGHT = 4,												// Rendered right (neg X)
	RENDER_LEFT = 5,												// Rendered left (pos X)
};

//Matrix* orientations[] = { &lookAtNegZ, &lookAtPosZ, &lookAtNegY, &lookAtPosY, &lookAtNegX, &lookAtPosX };
class Game
{
public:

	Game();															// Default constructor
	~Game();														// Destructor

	void Initialize();												// Initializes objects, textures, shader program and links shaders to shader program
	void Update(float timeElapsed);									// Updates physics, movements, etc.
	void Draw();													// Binds shader program, textures, objects, and invokes drawing of objects
	void Close();													// Ends execution of the game
	
	void ResizeWindow();											// Handles window resize

	void SetCameraProperties(float fov, float zNear, float zFar);	// Sets field of view, near and far planes values

	void KeyboardDown(unsigned char key, int mouseX, int mouseY);	// Handles key presses on keyboard - when pressed
	void KeyboardUp(unsigned char key, int mouseX, int mouseY);		// Handles key presses on keyboard - when released
	void KeyboardSpecial(int key, int x, int y);					// Handles special key presses on keyboard (arrow keys)
	void MouseClicked(int button, int state, int x, int y);			// Handles mouse clicks
	void MouseMoved(int x, int y);									// Handles mouse movement
	void MouseScrolled(int button, int direction, int x, int y);	// Handles wheel scrolling on middle mouse button

	// Screen rendering to FBOs
	void DrawToFBO													// Draws entire scene to specified FBO
		(
			Matrix* viewer,												// Point of view (position from which the scene is viewewed / rendered to FBO)
			Vector2 resolution,											// Size of FBO (x - width, y - height)
			GLuint textureId											// Handle for the texture cube object
		);
	
	bool IsInLineOfSight(											// Checking if object is in the line of sight of the other object
		Matrix* originTransform, 										// Transform matrix for the viewer object (eye)
		Matrix* targetTransform,										// Transform matrix for the target object (object being viewed)
		Vector2 viewRectangle,											// Width and Height of view rectangle (orthographic projection)
		RENDER_DIRECTION direction										// Direction in which rendering is done
		);

	// Initializes cube texture by loading image files to each cube side
	bool InitCubeSide(GLuint texture, GLenum targetSide, const char* textureFile);

	// Updates individual sides of cube texture by data from FBOs
	bool UpdateCubeSide(GLuint texture, GLenum targetSide, FrameBufferObject* fbo);
	
	// Camera frustum properties
	float fieldOfView, nearPlane, farPlane;

	// Shader programs
	ShaderProgram sProgPassthrough;
	ShaderProgram sProgReflectRefract;

	// Shaders
	Shader vertPassThrough;
	Shader fragSimpleColor;
	Shader vertReflectRefract;
	Shader fragReflectRefract;

	// Object loading
	ObjLoader objLoader;									// ObjLoader - used to load .obj files into VBOAttributes used by VBOs

	// Object rotation
	const GLfloat degreesPerSecond = 15.0f;					// Degrees to rotate per second
	GLfloat degreesRotated = 0.0f;							// Current rotation angle in degrees
	GLfloat radiansAngle = 0.0f;							// Degrees rotation and angle of rotation in radians
	float secondsElapsed = 0;								// Seconds elapsed (used for camera / mouse movement)

	// Mouse control
	const float mouseSensitivity = 0.1f;					// Mouse sensitivity
	int mouseButtonPressed = GLUT_UP,						// Mouse button pressed (GLUT_DOWN = any mouse button pressed, GLUT_UP = no button pressed)
		mouseX = 0,												// Current mouse X position
		mouseY = 0,												// Current mouse Y position
		oldMouseX = 0,											// Old mouse X position
		oldMouseY = 0;											// Old mouse Y position
	float scrollY = 0.0f;									// Scroll increment on Y axis

	// Transformation matrices (world, view, clip space)
	Matrix projectionMatrix;								// Projection matrix
	Matrix renderFBOProjectionMatrix;						// Projection matrix used to render scene to FBOs from water point of view
	Matrix mirrorSphereProjectionMatrix;					// Projection matrix used to render scene to FBOs from mirror sphere point of view
	Matrix viewMatrix;										// Camera (viewer or 'eye') transform
	Matrix skyViewMatrix;									// Special version of view matrix not taking view matrix translation into account

	// VBOs (monkey and torus object, sphere with Earth's texture
	VertexBufferObject vboMonkey;
	VertexBufferObject vboTorus;
	VertexBufferObject vboEarth;
	VertexBufferObject vboMirrorSphere;

	// To make FBO rendering easier each object has its own transform matrix
	Matrix mTransformWater;
	Matrix mTransformMonkey;								// Model matrix for Monkey head object
	Matrix mTransformTorus;									// Model matrix for Torus
	Matrix mTransformEarth;									// Model matrix for Sphere (Earth) object
	Matrix mTransformMirrorSphere;							// Model matrix for Mirror Sphere

	Matrix mViewer;											// Viewer position when rendering FBOs for dynamic reflections

	// Textures
	Texture texEarth;
	Texture texFur;
	Texture texUVMap;
	Texture texBlue;
	Texture texMess;

	float blendRatio = 0.5;									// Percent of blend ratio for two textures (used as % for texture and (1 - %) for texture2, 0.5 = 50%)

	// Cube texture for FBO rendering
	GLuint texRRCube;										// Texture cube for reflection/refraction (water render)
	GLuint texRRCube2;										// Texture cube for reflection/refraction (mirror sphere render)

	// FBOs to render cube texture sides (reflection textures)
	FrameBufferObject fboNegZ;								// Negative Z 
	FrameBufferObject fboPosZ;								// Positive Z
	FrameBufferObject fboNegY;								// Negative Y
	FrameBufferObject fboPosY;								// Positive Y
	FrameBufferObject fboNegX;								// Negative X
	FrameBufferObject fboPosX;								// Positive X
	
	int fboWidth, fboHeight;								// Width and height of FBOs

};

#endif