/*
	This is the header for Texture2D class. This class is based on SOIL library by Jonathan Dummer.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_TEXTURE2D_H
#define SGDW_TEXTURE2D_H

class Texture
{
public:

	Texture();							// Default constructor
	~Texture();							// Destructor

	bool Load(const char* filePath);	// Load a image file to a texture
	void Bind();						// Bind texture
	void Unbind();						// Unbind texture
	void Destroy();						// Delete references to texture
	GLuint GetHandle();					// Return handle for the texture
	
private:
	GLuint txHandle = 0;				// Handle for texture
};

#endif