/*
	This is the header for Vector3 class.
	Samir Suljkanovic 2016
*/

#ifndef SGDW_VECTOR3_H
#define SGDW_VECTOR3_H

class Vector3
{
public:
	// Constructor(s) for Vector3 initialization
	Vector3();								// Default Constructor
	Vector3(float X, float Y, float Z);		// Each Vector3 value set individually by the argument
	Vector3(float XYZ);						// All values initialized to specified value of the argument
	Vector3(const Vector3 &vec);			// Vector3 initialized to values from another Vector3 as argument
	Vector3(glm::vec3 &vec);				// Vector3 initialized to values from glm::vec3

	~Vector3();	// Destructor

	// Overloaded operators for operations on vectors
	bool operator==(const Vector3 &vec);	// Comparison - equal
	bool operator!=(const Vector3 &vec);	// Comparison - not equal
	void operator=(const Vector3 &vec);		// Assignment
	void operator+=(const Vector3 &vec);	// Addition
	void operator-=(const Vector3 &vec);	// Subtraction

	Vector3 operator+(const Vector3 &vec);	// Addition with the other vector
	Vector3 operator-(const Vector3 &vec);	// Subtraction with the other vector

	// Vector-Scalar operations
	void operator/=(float f);				// Self-division by float
	void operator*=(float f);				// Self-multiplication by float
	Vector3 operator*(float f) const;		// Multiplication with float
	Vector3 operator/(float f) const;		// Division by float
	
	void Negated();							// Negation
	float Dot(const Vector3 &vec);			// Dot product
	float Magnitude();						// Magnitude i.e. length of the Vector3
	float MagnitudeSquared();				// Square of the vector's magnitude
	Vector3 Normalize();					// Normalize vector
	Vector3 Cross(const Vector3 &vec);		// Cross product with another vector
	void Rotate(COORDINATE_AXIS, float angle);	// Rotation arround particular axis

	static Vector3 Zero();					// All Vector3 values as 0 i.e. { 0, 0, 0 }
	static Vector3 One();					// All Vector3 values as 1 i.e. { 1, 1, 1 }
	static Vector3 ScaleVector(Vector3 scaleVec, Vector3 vecToScale);	// Returns scaled vector vecToScale based on the scale values from scaleVec parameter vector
	//void PolarToVector(float azemuth, float elevation, float distance);
	//Vector3 VectorToPolar(Vector3 target);

	glm::vec3 ToGlmVec3();					// Converts 'this' Vector3 to glm::vec3
	static glm::vec3 ToGlmVec3(Vector3 &vec);// Converts given Vector3 to glm::vec3

	// Allow access to independent elements, or as an array e.g. for a Vector3 named vec, vec.coord[0] or vec.x
	union
	{
		struct
		{
			float x, y, z;
		};

		float coords[3];
	};

//private:
//
//	static float sinAngle;
//	static float cosAngle;
//	static Vector3 tempV;
};

#endif