/*
This is the header for VBO Attribute class.
Samir Suljkanovic 2016
*/

#ifndef SGDW_VBO_ATTRIBUTE_H
#define SGDW_VBO_ATTRIBUTE_H

class VBOAttribute
{
public:

	VBOAttribute();
	~VBOAttribute();

	// Getter methods to return attribute values
	VBO_ATTRIBUTE_TYPE GetAttributeType();											// Returns attribute type
	GLenum GetElementType();														// Returns element type
	unsigned GetElementsSize();														// Returns size of elements
	unsigned GetElementsPerAttribute();												// Returns element number per attribute
	unsigned GetElementsTotalCount();												// Returns total of all elements for all of the attributes
	void* GetDataPointer();															// Returns pointer, pointing to location where attributes data is allocated
	unsigned* GetInterleavedElementsPerAttribute();									// Returns array containing element sizes for each interleaved attribute

	// Setter methods to set attribute values
	void SetAttributeType(VBO_ATTRIBUTE_TYPE attributeType);						// Sets attribute type
	void SetElementType(GLenum elementType);										// Sets element type
	void SetElementsSize(unsigned elementSizeInBytes);								// Sets element size
	void SetElementsPerAttribute(unsigned elementsPerAttribute);					// Sets number of expected elements per attribute
	void SetElementsTotalCount(unsigned elementsTotalCount);						// Sets total number of elements inside attribute array
	void SetDataPointer(void* dataPointer);											// Sets data pointer to memory where array of attributes and their elements is stored
	void SetInterleavedElementsPerAttribute(unsigned elementSizes[4]);				// Returns array containing element sizes for each interleaved attribute

private:

	VBO_ATTRIBUTE_TYPE attributeType;												// Type of the attribute (e.g. VBO position, color, texture coordinates, normals)
	GLenum elementType;																// Type of the elements in array (e.g. GL_FLOAT for positions, colors, normals, or textures)
	unsigned elementSize;															// Size (in bytes) of each element in array
	unsigned elementsPerAttribute;													// Elements per attribute (e.g. a vertex position has 3)
	unsigned elementsTotalCount;													// Elements count in the entire array
	void* data;																		// Pointer to data

	unsigned interleavedElementsPerAttribute[4];									// Elements per attribute in a interleaved VBO for each attribute
};

#endif